--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.0
-- Dumped by pg_dump version 9.5.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: _abi_conf_parametro; Type: TABLE; Schema: public; Owner: jabi
--

CREATE TABLE _abi_conf_parametro (
    chiave character varying(50) NOT NULL,
    valore character varying(100),
    note character varying(100),
    crypt boolean DEFAULT false
);


ALTER TABLE _abi_conf_parametro OWNER TO jabi;

--
-- Name: _abi_conf_token_id_seq; Type: SEQUENCE; Schema: public; Owner: jabi
--

CREATE SEQUENCE _abi_conf_token_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE _abi_conf_token_id_seq OWNER TO jabi;

--
-- Name: _abi_conf_token; Type: TABLE; Schema: public; Owner: jabi
--

CREATE TABLE _abi_conf_token (
    id integer DEFAULT nextval('_abi_conf_token_id_seq'::regclass) NOT NULL,
    token character varying(50) NOT NULL,
    insert_date date DEFAULT now() NOT NULL,
    update_date date,
    fl_attivo boolean NOT NULL
);


ALTER TABLE _abi_conf_token OWNER TO jabi;

--
-- Name: _abi_istat_comune; Type: TABLE; Schema: public; Owner: jabi
--

CREATE TABLE _abi_istat_comune (
    cod_provincia character varying(3),
    cod_comune character varying(10),
    istat_comune character varying(10) NOT NULL,
    denominazione character varying(100),
    cod_catastale character varying(10)
);


ALTER TABLE _abi_istat_comune OWNER TO jabi;

--
-- Name: _abi_istat_provincia; Type: TABLE; Schema: public; Owner: jabi
--

CREATE TABLE _abi_istat_provincia (
    cod_provincia character varying(3) NOT NULL,
    cod_nuts character varying(5),
    cod_regione character varying(3),
    denominazione character varying(50),
    sigla character varying(2)
);


ALTER TABLE _abi_istat_provincia OWNER TO jabi;

--
-- Name: _abi_istat_regione; Type: TABLE; Schema: public; Owner: jabi
--

CREATE TABLE _abi_istat_regione (
    cod_regione character varying(3) NOT NULL,
    cod_nuts2 character varying(5),
    denominazione character varying(50),
    ripartizione_geografica character varying(50)
);


ALTER TABLE _abi_istat_regione OWNER TO jabi;

--
-- Name: _abi_main_account_id_seq; Type: SEQUENCE; Schema: public; Owner: jabi
--

CREATE SEQUENCE _abi_main_account_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE _abi_main_account_id_seq OWNER TO jabi;

--
-- Name: _abi_main_account; Type: TABLE; Schema: public; Owner: jabi
--

CREATE TABLE _abi_main_account (
    id integer DEFAULT nextval('_abi_main_account_id_seq'::regclass) NOT NULL,
    descr character varying(100) NOT NULL
);


ALTER TABLE _abi_main_account OWNER TO jabi;

--
-- Name: _abi_main_lk_users_class; Type: TABLE; Schema: public; Owner: jabi
--

CREATE TABLE _abi_main_lk_users_class (
    descrizione character varying(20),
    id character varying(3) NOT NULL
);


ALTER TABLE _abi_main_lk_users_class OWNER TO jabi;

--
-- Name: _abi_main_lk_users_status; Type: TABLE; Schema: public; Owner: jabi
--

CREATE TABLE _abi_main_lk_users_status (
    descrizione character varying(20),
    id integer NOT NULL
);


ALTER TABLE _abi_main_lk_users_status OWNER TO jabi;

--
-- Name: _abi_main_users_id_seq; Type: SEQUENCE; Schema: public; Owner: jabi
--

CREATE SEQUENCE _abi_main_users_id_seq
    START WITH 27
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE _abi_main_users_id_seq OWNER TO jabi;

--
-- Name: _abi_main_users; Type: TABLE; Schema: public; Owner: jabi
--

CREATE TABLE _abi_main_users (
    id integer DEFAULT nextval('_abi_main_users_id_seq'::regclass) NOT NULL,
    login character varying(30) NOT NULL,
    pwd character varying(32) NOT NULL,
    name character varying(40),
    surname character varying(40),
    email character varying(50),
    insert_date timestamp without time zone DEFAULT now() NOT NULL,
    update_date timestamp without time zone DEFAULT now() NOT NULL,
    last_access timestamp without time zone,
    statusid smallint NOT NULL,
    classid character varying(3),
    audit boolean DEFAULT false NOT NULL,
    account_id integer,
    codice_attivazione character varying(20),
    codice_fiscale character varying(16),
    numero_iscrizione_albo character varying(20),
    data_nascita date
);


ALTER TABLE _abi_main_users OWNER TO jabi;

--
-- Name: _abi_main_users_session_id_seq; Type: SEQUENCE; Schema: public; Owner: jabi
--

CREATE SEQUENCE _abi_main_users_session_id_seq
    START WITH 274
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE _abi_main_users_session_id_seq OWNER TO jabi;

--
-- Name: _abi_main_users_session; Type: TABLE; Schema: public; Owner: jabi
--

CREATE TABLE _abi_main_users_session (
    userid integer,
    prev_access timestamp without time zone DEFAULT now() NOT NULL,
    sessionid character(32),
    id integer DEFAULT nextval('_abi_main_users_session_id_seq'::regclass) NOT NULL,
    ip character varying(15),
    fl_attiva boolean,
    data_fine_sessione timestamp without time zone
);


ALTER TABLE _abi_main_users_session OWNER TO jabi;

--
-- Data for Name: _abi_conf_parametro; Type: TABLE DATA; Schema: public; Owner: jabi
--

COPY _abi_conf_parametro (chiave, valore, note, crypt) FROM stdin;
\.


--
-- Data for Name: _abi_conf_token; Type: TABLE DATA; Schema: public; Owner: jabi
--

COPY _abi_conf_token (id, token, insert_date, update_date, fl_attivo) FROM stdin;
\.


--
-- Name: _abi_conf_token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jabi
--

SELECT pg_catalog.setval('_abi_conf_token_id_seq', 1, false);


--
-- Data for Name: _abi_istat_comune; Type: TABLE DATA; Schema: public; Owner: jabi
--

COPY _abi_istat_comune (cod_provincia, cod_comune, istat_comune, denominazione, cod_catastale) FROM stdin;
001	 002 	001002	Airasca	A109
001	 003 	001003	Ala di Stura	A117
001	 004 	001004	Albiano d'Ivrea	A157
001	 005 	001005	Alice Superiore	A199
001	 006 	001006	Almese	A218
001	 007 	001007	Alpette	A221
001	 008 	001008	Alpignano	A222
001	 009 	001009	Andezeno	A275
001	 010 	001010	Andrate	A282
001	 011 	001011	Angrogna	A295
001	 012 	001012	Arignano	A405
001	 013 	001013	Avigliana	A518
001	 014 	001014	Azeglio	A525
001	 015 	001015	Bairo	A584
001	 016 	001016	Balangero	A587
001	 017 	001017	Baldissero Canavese	A590
001	 018 	001018	Baldissero Torinese	A591
001	 019 	001019	Balme	A599
001	 020 	001020	Banchette	A607
001	 021 	001021	Barbania	A625
001	 022 	001022	Bardonecchia	A651
001	 023 	001023	Barone Canavese	A673
001	 024 	001024	Beinasco	A734
001	 025 	001025	Bibiana	A853
001	 026 	001026	Bobbio Pellice	A910
001	 027 	001027	Bollengo	A941
001	 028 	001028	Borgaro Torinese	A990
001	 029 	001029	Borgiallo	B003
001	 030 	001030	Borgofranco dIvrea	B015
001	 031 	001031	Borgomasino	B021
001	 032 	001032	Borgone Susa	B024
001	 033 	001033	Bosconero	B075
001	 034 	001034	Brandizzo	B121
001	 035 	001035	Bricherasio	B171
001	 036 	001036	Brosso	B205
001	 037 	001037	Brozolo	B209
001	 038 	001038	Bruino	B216
001	 039 	001039	Brusasco	B225
001	 040 	001040	Bruzolo	B232
001	 041 	001041	Buriasco	B278
001	 042 	001042	Burolo	B279
001	 043 	001043	Busano	B284
001	 044 	001044	Bussoleno	B297
001	 045 	001045	Buttigliera Alta	B305
001	 046 	001046	Cafasse	B350
001	 047 	001047	Caluso	B435
001	 048 	001048	Cambiano	B462
001	 049 	001049	Campiglione Fenile	B512
001	 050 	001050	Candia Canavese	B588
001	 051 	001051	Candiolo	B592
001	 052 	001052	Canischio	B605
001	 053 	001053	Cantalupa	B628
001	 054 	001054	Cantoira	B637
001	 055 	001055	Caprie	B705
001	 056 	001056	Caravino	B733
001	 057 	001057	Carema	B762
001	 058 	001058	Carignano	B777
001	 059 	001059	Carmagnola	B791
001	 060 	001060	Casalborgone	B867
001	 061 	001061	Cascinette dIvrea	B953
001	 062 	001062	Caselette	B955
001	 063 	001063	Caselle Torinese	B960
001	 064 	001064	Castagneto Po	C045
001	 065 	001065	Castagnole Piemonte	C048
001	 066 	001066	Castellamonte	C133
001	 067 	001067	Castelnuovo Nigra	C241
001	 068 	001068	Castiglione Torinese	C307
001	 069 	001069	Cavagnolo	C369
001	 070 	001070	Cavour	C404
001	 071 	001071	Cercenasco	C487
001	 072 	001072	Ceres	C497
001	 073 	001073	Ceresole Reale	C505
001	 074 	001074	Cesana Torinese	C564
001	 075 	001075	Chialamberto	C604
001	 076 	001076	Chianocco	C610
001	 077 	001077	Chiaverano	C624
001	 078 	001078	Chieri	C627
001	 079 	001079	Chiesanuova	C629
001	 080 	001080	Chiomonte	C639
001	 081 	001081	Chiusa di San Michele	C655
001	 082 	001082	Chivasso	C665
001	 083 	001083	Ciconio	C679
001	 084 	001084	Cintano	C711
001	 085 	001085	Cinzano	C715
001	 086 	001086	Ciri√®	C722
001	 087 	001087	Claviere	C793
001	 088 	001088	Coassolo Torinese	C801
001	 089 	001089	Coazze	C803
001	 090 	001090	Collegno	C860
001	 091 	001091	Colleretto Castelnuovo	C867
001	 092 	001092	Colleretto Giacosa	C868
001	 093 	001093	Condove	C955
001	 094 	001094	Corio	D008
001	 095 	001095	Cossano Canavese	D092
001	 096 	001096	Cuceglio	D197
001	 097 	001097	Cumiana	D202
001	 098 	001098	Cuorgn√®	D208
001	 099 	001099	Druento	D373
001	 100 	001100	Exilles	D433
001	 101 	001101	Favria	D520
001	 102 	001102	Feletto	D524
001	 103 	001103	Fenestrelle	D532
001	 104 	001104	Fiano	D562
001	 105 	001105	Fiorano Canavese	D608
001	 106 	001106	Foglizzo	D646
001	 107 	001107	Forno Canavese	D725
001	 108 	001108	Frassinetto	D781
001	 109 	001109	Front	D805
001	 110 	001110	Frossasco	D812
001	 111 	001111	Garzigliana	D931
001	 112 	001112	Gassino Torinese	D933
001	 113 	001113	Germagnano	D983
001	 114 	001114	Giaglione	E009
001	 115 	001115	Giaveno	E020
001	 116 	001116	Givoletto	E067
001	 117 	001117	Gravere	E154
001	 118 	001118	Groscavallo	E199
001	 119 	001119	Grosso	E203
001	 120 	001120	Grugliasco	E216
001	 121 	001121	Ingria	E301
001	 122 	001122	Inverso Pinasca	E311
001	 123 	001123	Isolabella	E345
001	 124 	001124	Issiglio	E368
001	 125 	001125	Ivrea	E379
001	 126 	001126	La Cassa	E394
001	 127 	001127	La Loggia	E423
001	 128 	001128	Lanzo Torinese	E445
001	 129 	001129	Lauriano	E484
001	 130 	001130	Leini	E518
001	 131 	001131	Lemie	E520
001	 132 	001132	Lessolo	E551
001	 133 	001133	Levone	E566
001	 134 	001134	Locana	E635
001	 135 	001135	Lombardore	E660
001	 136 	001136	Lombriasco	E661
001	 137 	001137	Loranz√®	E683
001	 138 	001138	Lugnacco	E727
001	 139 	001139	Luserna San Giovanni	E758
001	 140 	001140	Lusernetta	E759
001	 141 	001141	Lusigli√®	E763
001	 142 	001142	Macello	E782
001	 143 	001143	Maglione	E817
001	 144 	001144	Marentino	E941
001	 145 	001145	Massello	F041
001	 146 	001146	Mathi	F053
001	 147 	001147	Mattie	F058
001	 148 	001148	Mazz√®	F067
001	 149 	001149	Meana di Susa	F074
001	 150 	001150	Mercenasco	F140
001	 151 	001151	Meugliano	F164
001	 152 	001152	Mezzenile	F182
001	 153 	001153	Mombello di Torino	F315
001	 154 	001154	Mompantero	F318
001	 155 	001155	Monastero di Lanzo	F327
001	 156 	001156	Moncalieri	F335
001	 157 	001157	Moncenisio	D553
001	 158 	001158	Montaldo Torinese	F407
001	 159 	001159	Montalenghe	F411
001	 160 	001160	Montalto Dora	F420
001	 161 	001161	Montanaro	F422
001	 162 	001162	Monteu da Po	F651
001	 163 	001163	Moriondo Torinese	F733
001	 164 	001164	Nichelino	F889
001	 165 	001165	Noasca	F906
001	 166 	001166	Nole	F925
001	 167 	001167	Nomaglio	F927
001	 168 	001168	None	F931
001	 169 	001169	Novalesa	F948
001	 170 	001170	Oglianico	G010
001	 171 	001171	Orbassano	G087
001	 172 	001172	Orio Canavese	G109
001	 173 	001173	Osasco	G151
001	 174 	001174	Osasio	G152
001	 175 	001175	Oulx	G196
001	 176 	001176	Ozegna	G202
001	 177 	001177	Palazzo Canavese	G262
001	 178 	001178	Pancalieri	G303
001	 179 	001179	Parella	G330
001	 180 	001180	Pavarolo	G387
001	 181 	001181	Pavone Canavese	G392
001	 182 	001182	Pecco	G396
001	 183 	001183	Pecetto Torinese	G398
001	 184 	001184	Perosa Argentina	G463
001	 185 	001185	Perosa Canavese	G462
001	 186 	001186	Perrero	G465
001	 187 	001187	Pertusio	G477
001	 188 	001188	Pessinetto	G505
001	 189 	001189	Pianezza	G559
001	 190 	001190	Pinasca	G672
001	 191 	001191	Pinerolo	G674
001	 192 	001192	Pino Torinese	G678
001	 193 	001193	Piobesi Torinese	G684
001	 194 	001194	Piossasco	G691
001	 195 	001195	Piscina	G705
001	 196 	001196	Piverone	G719
001	 197 	001197	Poirino	G777
001	 198 	001198	Pomaretto	G805
001	 199 	001199	Pont-Canavese	G826
001	 200 	001200	Porte	G900
001	 201 	001201	Pragelato	G973
001	 202 	001202	Prali	G978
001	 203 	001203	Pralormo	G979
001	 204 	001204	Pramollo	G982
001	 205 	001205	Prarostino	G986
001	 206 	001206	Prascorsano	G988
001	 207 	001207	Pratiglione	G997
001	 208 	001208	Quagliuzzo	H100
001	 209 	001209	Quassolo	H120
001	 210 	001210	Quincinetto	H127
001	 211 	001211	Reano	H207
001	 212 	001212	Ribordone	H270
001	 213 	001213	Rivalba	H333
001	 214 	001214	Rivalta di Torino	H335
001	 215 	001215	Riva presso Chieri	H337
001	 216 	001216	Rivara	H338
001	 217 	001217	Rivarolo Canavese	H340
001	 218 	001218	Rivarossa	H344
001	 219 	001219	Rivoli	H355
001	 220 	001220	Robassomero	H367
001	 221 	001221	Rocca Canavese	H386
001	 222 	001222	Roletto	H498
001	 223 	001223	Romano Canavese	H511
001	 224 	001224	Ronco Canavese	H539
001	 225 	001225	Rondissone	H547
001	 226 	001226	Ror√®	H554
001	 227 	001227	Roure	H555
001	 228 	001228	Rosta	H583
001	 229 	001229	Rubiana	H627
001	 230 	001230	Rueglio	H631
001	 231 	001231	Salassa	H691
001	 232 	001232	Salbertrand	H684
001	 233 	001233	Salerano Canavese	H702
001	 234 	001234	Salza di Pinerolo	H734
001	 235 	001235	Samone	H753
001	 236 	001236	San Benigno Canavese	H775
001	 237 	001237	San Carlo Canavese	H789
001	 238 	001238	San Colombano Belmonte	H804
001	 239 	001239	San Didero	H820
001	 240 	001240	San Francesco al Campo	H847
001	 241 	001241	Sangano	H855
001	 242 	001242	San Germano Chisone	H862
001	 243 	001243	San Gillio	H873
001	 244 	001244	San Giorgio Canavese	H890
001	 245 	001245	San Giorio di Susa	H900
001	 246 	001246	San Giusto Canavese	H936
001	 247 	001247	San Martino Canavese	H997
001	 248 	001248	San Maurizio Canavese	I024
001	 249 	001249	San Mauro Torinese	I030
001	 250 	001250	San Pietro Val Lemina	I090
001	 251 	001251	San Ponso	I126
001	 252 	001252	San Raffaele Cimena	I137
001	 253 	001253	San Sebastiano da Po	I152
001	 254 	001254	San Secondo di Pinerolo	I154
001	 255 	001255	SantAmbrogio di Torino	I258
001	 256 	001256	SantAntonino di Susa	I296
001	 257 	001257	Santena	I327
001	 258 	001258	Sauze di Cesana	I465
001	 259 	001259	Sauze dOulx	I466
001	 260 	001260	Scalenghe	I490
001	 261 	001261	Scarmagno	I511
001	 262 	001262	Sciolze	I539
001	 263 	001263	Sestriere	I692
001	 264 	001264	Settimo Rottaro	I701
001	 265 	001265	Settimo Torinese	I703
001	 266 	001266	Settimo Vittone	I702
001	 267 	001267	Sparone	I886
001	 268 	001268	Strambinello	I969
001	 269 	001269	Strambino	I970
001	 270 	001270	Susa	L013
001	 271 	001271	Tavagnasco	L066
001	 272 	001272	Torino	L219
001	 273 	001273	Torrazza Piemonte	L238
001	 274 	001274	Torre Canavese	L247
001	 275 	001275	Torre Pellice	L277
001	 276 	001276	Trana	L327
001	 277 	001277	Trausella	L338
001	 278 	001278	Traversella	L345
001	 279 	001279	Traves	L340
001	 280 	001280	Trofarello	L445
001	 281 	001281	Usseaux	L515
001	 282 	001282	Usseglio	L516
001	 283 	001283	Vaie	L538
001	 284 	001284	Val della Torre	L555
001	 285 	001285	Valgioie	L578
001	 286 	001286	Vallo Torinese	L629
001	 287 	001287	Valperga	L644
001	 288 	001288	Valprato Soana	B510
001	 289 	001289	Varisella	L685
001	 290 	001290	Vauda Canavese	L698
001	 291 	001291	Venaus	L726
001	 292 	001292	Venaria Reale	L727
001	 293 	001293	Verolengo	L779
001	 294 	001294	Verrua Savoia	L787
001	 295 	001295	VestignÔøΩ	L811
001	 296 	001296	VialfrÔøΩ	L830
001	 297 	001297	Vico Canavese	L548
001	 298 	001298	Vidracco	L857
001	 299 	001299	Vigone	L898
001	 300 	001300	Villafranca Piemonte	L948
001	 301 	001301	Villanova Canavese	L982
001	 302 	001302	Villarbasse	M002
001	 303 	001303	Villar Dora	L999
001	 304 	001304	Villareggia	M004
001	 305 	001305	Villar Focchiardo	M007
001	 306 	001306	Villar Pellice	M013
001	 307 	001307	Villar Perosa	M014
001	 308 	001308	Villastellone	M027
001	 309 	001309	Vinovo	M060
001	 310 	001310	Virle Piemonte	M069
001	 311 	001311	Vische	M071
001	 312 	001312	Vistrorio	M080
001	 313 	001313	ViÔøΩ	M094
001	 314 	001314	Volpiano	M122
001	 315 	001315	Volvera	M133
001	 316 	001316	Mappano	M316
002	 002 	002002	Alagna Valsesia	A119
002	 003 	002003	Albano Vercellese	A130
002	 004 	002004	Alice Castello	A198
002	 006 	002006	Arborio	A358
002	 007 	002007	Asigliano Vercellese	A466
002	 008 	002008	Balmuccia	A600
002	 009 	002009	Balocco	A601
002	 011 	002011	BianzÔøΩ	A847
002	 014 	002014	Boccioleto	A914
002	 015 	002015	Borgo dAle	B009
002	 016 	002016	Borgosesia	B041
002	 017 	002017	Borgo Vercelli	B046
002	 019 	002019	Breia	B136
002	 021 	002021	Buronzo	B280
002	 025 	002025	Campertogno	B505
002	 029 	002029	Carcoforo	B752
002	 030 	002030	Caresana	B767
002	 031 	002031	Caresanablot	B768
002	 032 	002032	Carisio	B782
002	 033 	002033	Casanova Elvo	B928
002	 035 	002035	San Giacomo Vercellese	B952
002	 038 	002038	Cellio	C450
002	 041 	002041	Cervatto	C548
002	 042 	002042	Cigliano	C680
002	 043 	002043	Civiasco	C757
002	 045 	002045	Collobiano	C884
002	 047 	002047	Costanzana	D113
002	 048 	002048	Cravagliana	D132
002	 049 	002049	Crescentino	D154
002	 052 	002052	Crova	D187
002	 054 	002054	Desana	D281
002	 057 	002057	Fobello	D641
002	 058 	002058	Fontanetto Po	D676
002	 059 	002059	Formigliana	D712
002	 061 	002061	Gattinara	D938
002	 062 	002062	Ghislarengo	E007
002	 065 	002065	Greggio	E163
002	 066 	002066	Guardabosone	E237
002	 067 	002067	Lamporo	E433
002	 068 	002068	Lenta	E528
002	 070 	002070	Lignana	E583
002	 071 	002071	Livorno Ferraris	E626
002	 072 	002072	Lozzolo	E711
002	 078 	002078	Mollia	F297
002	 079 	002079	Moncrivello	F342
002	 082 	002082	Motta de Conti	F774
002	 088 	002088	Olcenengo	G016
002	 089 	002089	Oldenico	G018
002	 090 	002090	Palazzolo Vercellese	G266
002	 091 	002091	Pertengo	G471
002	 093 	002093	Pezzana	G528
002	 096 	002096	Pila	G666
002	 097 	002097	Piode	G685
002	 102 	002102	Postua	G940
002	 104 	002104	Prarolo	G985
002	 107 	002107	Quarona	H108
002	 108 	002108	Quinto Vercellese	H132
002	 110 	002110	Rassa	H188
002	 111 	002111	Rima San Giuseppe	H291
002	 112 	002112	Rimasco	H292
002	 113 	002113	Rimella	H293
002	 114 	002114	Riva Valdobbia	H329
002	 115 	002115	Rive	H346
002	 116 	002116	Roasio	H365
002	 118 	002118	Ronsecco	H549
002	 121 	002121	Rossa	H577
002	 122 	002122	Rovasenda	H364
002	 123 	002123	Sabbia	H648
002	 126 	002126	Salasco	H690
002	 127 	002127	Sali Vercellese	H707
002	 128 	002128	Saluggia	H725
002	 131 	002131	San Germano Vercellese	H861
002	 133 	002133	Santhi√†	I337
002	 134 	002134	Scopa	I544
002	 135 	002135	Scopello	I545
002	 137 	002137	Serravalle Sesia	I663
002	 142 	002142	Stroppiana	I984
002	 147 	002147	Tricerro	L420
002	 148 	002148	Trino	L429
002	 150 	002150	Tronzano Vercellese	L451
002	 152 	002152	Valduggia	L566
002	 156 	002156	Varallo	L669
002	 158 	002158	Vercelli	L750
002	 163 	002163	Villarboit	M003
002	 164 	002164	Villata	M028
002	 166 	002166	Vocca	M106
003	 001 	003001	Agrate Conturbia	A088
003	 002 	003002	Ameno	A264
003	 006 	003006	Armeno	A414
003	 008 	003008	Arona	A429
003	 012 	003012	Barengo	A653
003	 016 	003016	Bellinzago Novarese	A752
003	 018 	003018	Biandrate	A844
003	 019 	003019	Boca	A911
003	 021 	003021	Bogogno	A929
003	 022 	003022	Bolzano Novarese	A953
003	 023 	003023	Borgolavezzaro	B016
003	 024 	003024	Borgomanero	B019
003	 025 	003025	Borgo Ticino	B043
003	 026 	003026	Briga Novarese	B176
003	 027 	003027	Briona	B183
003	 030 	003030	Caltignaga	B431
003	 032 	003032	Cameri	B473
003	 036 	003036	Carpignano Sesia	B823
003	 037 	003037	Casalbeltrame	B864
003	 039 	003039	Casaleggio Novara	B883
003	 040 	003040	Casalino	B897
003	 041 	003041	Casalvolone	B920
003	 042 	003042	Castellazzo Novarese	C149
003	 043 	003043	Castelletto sopra Ticino	C166
003	 044 	003044	Cavaglietto	C364
003	 045 	003045	Cavaglio dAgogna	C365
003	 047 	003047	Cavallirio	C378
003	 049 	003049	Cerano	C483
003	 051 	003051	Colazza	C829
003	 052 	003052	Comignago	C926
003	 055 	003055	Cressa	D162
003	 058 	003058	Cureggio	D216
003	 060 	003060	Divignano	D309
003	 062 	003062	Dormelletto	D347
003	 065 	003065	Fara Novarese	D492
003	 066 	003066	Fontaneto dAgogna	D675
003	 068 	003068	Galliate	D872
003	 069 	003069	Garbagna Novarese	D911
003	 070 	003070	Gargallo	D921
003	 071 	003071	Gattico	D937
003	 073 	003073	Ghemme	E001
003	 076 	003076	Gozzano	E120
003	 077 	003077	Granozzo con Monticello	E143
003	 079 	003079	Grignasco	E177
003	 082 	003082	Invorio	E314
003	 083 	003083	Landiona	E436
003	 084 	003084	Lesa	E544
003	 088 	003088	Maggiora	E803
003	 090 	003090	Mandello Vitta	E880
003	 091 	003091	Marano Ticino	E907
003	 093 	003093	Massino Visconti	F047
003	 095 	003095	Meina	F093
003	 097 	003097	Mezzomerico	F188
003	 098 	003098	Miasino	F191
003	 100 	003100	Momo	F317
003	 103 	003103	Nebbiuno	F859
003	 104 	003104	Nibbiola	F886
003	 106 	003106	Novara	F952
003	 108 	003108	Oleggio	G019
003	 109 	003109	Oleggio Castello	G020
003	 112 	003112	Orta San Giulio	G134
003	 114 	003114	Paruzzaro	G349
003	 115 	003115	Pella	G421
003	 116 	003116	Pettenasco	G520
003	 119 	003119	Pisano	G703
003	 120 	003120	Pogno	G775
003	 121 	003121	Pombia	G809
003	 122 	003122	Prato Sesia	H001
003	 129 	003129	Recetto	H213
003	 130 	003130	Romagnano Sesia	H502
003	 131 	003131	Romentino	H518
003	 133 	003133	San Maurizio dOpaglio	I025
003	 134 	003134	San Nazzaro Sesia	I052
003	 135 	003135	San Pietro Mosezzo	I116
003	 138 	003138	Sillavengo	I736
003	 139 	003139	Sizzano	I767
003	 140 	003140	Soriso	I857
003	 141 	003141	Sozzago	I880
003	 143 	003143	Suno	L007
003	 144 	003144	Terdobbiate	L104
003	 146 	003146	Tornaco	L223
003	 149 	003149	Trecate	L356
003	 153 	003153	Vaprio dAgogna	L668
003	 154 	003154	Varallo Pombia	L670
003	 157 	003157	Veruno	L798
003	 158 	003158	Vespolate	L808
003	 159 	003159	Vicolungo	L847
003	 164 	003164	Vinzaglio	M062
004	 001 	004001	Acceglio	A016
004	 002 	004002	Aisone	A113
004	 003 	004003	Alba	A124
004	 004 	004004	Albaretto della Torre	A139
004	 005 	004005	Alto	A238
004	 006 	004006	Argentera	A394
004	 007 	004007	Arguello	A396
004	 008 	004008	Bagnasco	A555
004	 009 	004009	Bagnolo Piemonte	A571
004	 010 	004010	Baldissero dAlba	A589
004	 011 	004011	Barbaresco	A629
004	 012 	004012	Barge	A660
004	 013 	004013	Barolo	A671
004	 014 	004014	Bastia MondovÔøΩ	A709
004	 015 	004015	Battifollo	A716
004	 016 	004016	Beinette	A735
004	 017 	004017	Bellino	A750
004	 018 	004018	Belvedere Langhe	A774
004	 019 	004019	Bene Vagienna	A779
004	 020 	004020	Benevello	A782
004	 021 	004021	Bergolo	A798
004	 022 	004022	Bernezzo	A805
004	 023 	004023	Bonvicino	A979
004	 024 	004024	Borgomale	B018
004	 025 	004025	Borgo San Dalmazzo	B033
004	 026 	004026	Bosia	B079
004	 027 	004027	Bossolasco	B084
004	 028 	004028	Boves	B101
004	 029 	004029	Bra	B111
004	 030 	004030	Briaglia	B167
004	 031 	004031	Briga Alta	B175
004	 032 	004032	Brondello	B200
004	 033 	004033	Brossasco	B204
004	 034 	004034	Busca	B285
004	 035 	004035	Camerana	B467
004	 036 	004036	Camo	B489
004	 037 	004037	Canale	B573
004	 038 	004038	Canosio	B621
004	 039 	004039	Caprauna	B692
004	 040 	004040	Caraglio	B719
004	 041 	004041	Caramagna Piemonte	B720
004	 042 	004042	CardÔøΩ	B755
004	 043 	004043	CarrÔøΩ	B841
004	 044 	004044	Cartignano	B845
004	 045 	004045	Casalgrasso	B894
004	 046 	004046	Castagnito	C046
004	 047 	004047	Casteldelfino	C081
004	 048 	004048	Castellar	C140
004	 049 	004049	Castelletto Stura	C165
004	 050 	004050	Castelletto Uzzone	C167
004	 051 	004051	Castellinaldo	C173
004	 052 	004052	Castellino Tanaro	C176
004	 053 	004053	Castelmagno	C205
004	 054 	004054	Castelnuovo di Ceva	C214
004	 055 	004055	Castiglione Falletto	C314
004	 056 	004056	Castiglione Tinella	C317
004	 057 	004057	Castino	C323
004	 058 	004058	Cavallerleone	C375
004	 059 	004059	Cavallermaggiore	C376
004	 060 	004060	Celle di Macra	C441
004	 061 	004061	Centallo	C466
004	 062 	004062	Ceresole Alba	C504
004	 063 	004063	Cerretto Langhe	C530
004	 064 	004064	Cervasca	C547
004	 065 	004065	Cervere	C550
004	 066 	004066	Ceva	C589
004	 067 	004067	Cherasco	C599
004	 068 	004068	Chiusa di Pesio	C653
004	 069 	004069	CigliÔøΩ	C681
004	 070 	004070	Cissone	C738
004	 071 	004071	Clavesana	C792
004	 072 	004072	Corneliano dAlba	D022
004	 073 	004073	Cortemilia	D062
004	 074 	004074	Cossano Belbo	D093
004	 075 	004075	Costigliole Saluzzo	D120
004	 076 	004076	Cravanzana	D133
004	 077 	004077	Crissolo	D172
004	 078 	004078	Cuneo	D205
004	 079 	004079	Demonte	D271
004	 080 	004080	Diano dAlba	D291
004	 081 	004081	Dogliani	D314
004	 082 	004082	Dronero	D372
004	 083 	004083	Elva	D401
004	 084 	004084	Entracque	D410
004	 085 	004085	Envie	D412
004	 086 	004086	Farigliano	D499
004	 087 	004087	Faule	D511
004	 088 	004088	Feisoglio	D523
004	 089 	004089	Fossano	D742
004	 090 	004090	Frabosa Soprana	D751
004	 091 	004091	Frabosa Sottana	D752
004	 092 	004092	Frassino	D782
004	 093 	004093	Gaiola	D856
004	 094 	004094	Gambasca	D894
004	 095 	004095	Garessio	D920
004	 096 	004096	Genola	D967
004	 097 	004097	Gorzegno	E111
004	 098 	004098	Gottasecca	E115
004	 099 	004099	Govone	E118
004	 100 	004100	Grinzane Cavour	E182
004	 101 	004101	Guarene	E251
004	 102 	004102	Igliano	E282
004	 103 	004103	Isasca	E327
004	 104 	004104	Lagnasco	E406
004	 105 	004105	La Morra	E430
004	 106 	004106	Lequio Berria	E540
004	 107 	004107	Lequio Tanaro	E539
004	 108 	004108	Lesegno	E546
004	 109 	004109	Levice	E564
004	 110 	004110	Limone Piemonte	E597
004	 111 	004111	Lisio	E615
004	 112 	004112	Macra	E789
004	 113 	004113	Magliano Alfieri	E809
004	 114 	004114	Magliano Alpi	E808
004	 115 	004115	Mango	E887
004	 116 	004116	Manta	E894
004	 117 	004117	Marene	E939
004	 118 	004118	Margarita	E945
004	 119 	004119	Marmora	E963
004	 120 	004120	Marsaglia	E973
004	 121 	004121	Martiniana Po	E988
004	 122 	004122	Melle	F114
004	 123 	004123	Moiola	F279
004	 124 	004124	Mombarcaro	F309
004	 125 	004125	Mombasiglio	F312
004	 126 	004126	Monastero di Vasco	F326
004	 127 	004127	Monasterolo Casotto	F329
004	 128 	004128	Monasterolo di Savigliano	F330
004	 129 	004129	Monchiero	F338
004	 130 	004130	MondovÔøΩ	F351
004	 131 	004131	Monesiglio	F355
004	 132 	004132	Monforte dAlba	F358
004	 133 	004133	MontÔøΩ	F385
004	 134 	004134	Montaldo di MondovÔøΩ	F405
004	 135 	004135	Montaldo Roero	F408
004	 136 	004136	Montanera	F424
004	 137 	004137	Montelupo Albese	F550
004	 138 	004138	Montemale di Cuneo	F558
004	 139 	004139	Monterosso Grana	F608
004	 140 	004140	Monteu Roero	F654
004	 141 	004141	Montezemolo	F666
004	 142 	004142	Monticello dAlba	F669
004	 143 	004143	Moretta	F723
004	 144 	004144	Morozzo	F743
004	 145 	004145	Murazzano	F809
004	 146 	004146	Murello	F811
004	 147 	004147	Narzole	F846
004	 148 	004148	Neive	F863
004	 149 	004149	Neviglie	F883
004	 150 	004150	Niella Belbo	F894
004	 151 	004151	Niella Tanaro	F895
004	 152 	004152	Novello	F961
004	 153 	004153	Nucetto	F972
004	 154 	004154	Oncino	G066
004	 155 	004155	Ormea	G114
004	 156 	004156	Ostana	G183
004	 157 	004157	Paesana	G228
004	 158 	004158	Pagno	G240
004	 159 	004159	Pamparato	G302
004	 160 	004160	Paroldo	G339
004	 161 	004161	Perletto	G457
004	 162 	004162	Perlo	G458
004	 163 	004163	Peveragno	G526
004	 164 	004164	Pezzolo Valle Uzzone	G532
004	 165 	004165	Pianfei	G561
004	 166 	004166	Piasco	G575
004	 167 	004167	Pietraporzio	G625
004	 168 	004168	Piobesi dAlba	G683
004	 169 	004169	Piozzo	G697
004	 170 	004170	Pocapaglia	G742
004	 171 	004171	Polonghera	G800
004	 172 	004172	Pontechianale	G837
004	 173 	004173	Pradleves	G970
004	 174 	004174	Prazzo	H011
004	 175 	004175	Priero	H059
004	 176 	004176	Priocca	H068
004	 177 	004177	Priola	H069
004	 178 	004178	Prunetto	H085
004	 179 	004179	Racconigi	H150
004	 180 	004180	Revello	H247
004	 181 	004181	Rifreddo	H285
004	 182 	004182	Rittana	H326
004	 183 	004183	Roaschia	H362
004	 184 	004184	Roascio	H363
004	 185 	004185	Robilante	H377
004	 186 	004186	Roburent	H378
004	 187 	004187	Roccabruna	H385
004	 188 	004188	Rocca CigliÔøΩ	H391
004	 189 	004189	Rocca de Baldi	H395
004	 190 	004190	Roccaforte MondovÔøΩ	H407
004	 191 	004191	Roccasparvera	H447
004	 192 	004192	Roccavione	H453
004	 193 	004193	Rocchetta Belbo	H462
004	 194 	004194	Roddi	H472
004	 195 	004195	Roddino	H473
004	 196 	004196	Rodello	H474
004	 197 	004197	Rossana	H578
004	 198 	004198	Ruffia	H633
004	 199 	004199	Sale delle Langhe	H695
004	 200 	004200	Sale San Giovanni	H704
004	 201 	004201	Saliceto	H710
004	 202 	004202	Salmour	H716
004	 203 	004203	Saluzzo	H727
004	 204 	004204	Sambuco	H746
004	 205 	004205	Sampeyre	H755
004	 206 	004206	San Benedetto Belbo	H770
004	 207 	004207	San Damiano Macra	H812
004	 208 	004208	SanfrÔøΩ	H851
004	 209 	004209	Sanfront	H852
004	 210 	004210	San Michele MondovÔøΩ	I037
004	 211 	004211	SantAlbano Stura	I210
004	 212 	004212	Santa Vittoria dAlba	I316
004	 213 	004213	Santo Stefano Belbo	I367
004	 214 	004214	Santo Stefano Roero	I372
004	 215 	004215	Savigliano	I470
004	 216 	004216	Scagnello	I484
004	 217 	004217	Scarnafigi	I512
004	 218 	004218	Serralunga dAlba	I646
004	 219 	004219	Serravalle Langhe	I659
004	 220 	004220	Sinio	I750
004	 221 	004221	Somano	I817
004	 222 	004222	Sommariva del Bosco	I822
004	 223 	004223	Sommariva Perno	I823
004	 224 	004224	Stroppo	I985
004	 225 	004225	Tarantasca	L048
004	 226 	004226	Torre Bormida	L252
004	 227 	004227	Torre MondovÔøΩ	L241
004	 228 	004228	Torre San Giorgio	L278
004	 229 	004229	Torresina	L281
004	 230 	004230	Treiso	L367
004	 231 	004231	Trezzo Tinella	L410
004	 232 	004232	TrinitÔøΩ	L427
004	 233 	004233	Valdieri	L558
004	 234 	004234	Valgrana	L580
004	 235 	004235	Valloriate	L631
004	 236 	004236	Valmala	L636
004	 237 	004237	Venasca	L729
004	 238 	004238	Verduno	L758
004	 239 	004239	Vernante	L771
004	 240 	004240	Verzuolo	L804
004	 241 	004241	Vezza dAlba	L817
004	 242 	004242	Vicoforte	L841
004	 243 	004243	Vignolo	L888
004	 244 	004244	Villafalletto	L942
004	 245 	004245	Villanova MondovÔøΩ	L974
004	 246 	004246	Villanova Solaro	L990
004	 247 	004247	Villar San Costanzo	M015
004	 248 	004248	Vinadio	M055
004	 249 	004249	Viola	M063
004	 250 	004250	Vottignasco	M136
005	 001 	005001	Agliano Terme	A072
005	 002 	005002	Albugnano	A173
005	 003 	005003	Antignano	A312
005	 004 	005004	Aramengo	A352
005	 005 	005005	Asti	A479
005	 006 	005006	Azzano dAsti	A527
005	 007 	005007	Baldichieri dAsti	A588
005	 008 	005008	Belveglio	A770
005	 009 	005009	Berzano di San Pietro	A812
005	 010 	005010	Bruno	B221
005	 011 	005011	Bubbio	B236
005	 012 	005012	Buttigliera dAsti	B306
005	 013 	005013	Calamandrana	B376
005	 014 	005014	Calliano	B418
005	 015 	005015	Calosso	B425
005	 016 	005016	Camerano Casasco	B469
005	 017 	005017	Canelli	B594
005	 018 	005018	Cantarana	B633
005	 019 	005019	Capriglio	B707
005	 020 	005020	Casorzo	B991
005	 021 	005021	Cassinasco	C022
005	 022 	005022	Castagnole delle Lanze	C049
005	 023 	005023	Castagnole Monferrato	C047
005	 024 	005024	Castel Boglione	C064
005	 025 	005025	CastellAlfero	C127
005	 026 	005026	Castellero	C154
005	 027 	005027	Castelletto Molina	C161
005	 028 	005028	Castello di Annone	A300
005	 029 	005029	Castelnuovo Belbo	C226
005	 030 	005030	Castelnuovo Calcea	C230
005	 031 	005031	Castelnuovo Don Bosco	C232
005	 032 	005032	Castel Rocchero	C253
005	 033 	005033	Cellarengo	C438
005	 034 	005034	Celle Enomondo	C440
005	 035 	005035	Cerreto dAsti	C528
005	 036 	005036	Cerro Tanaro	C533
005	 037 	005037	Cessole	C583
005	 038 	005038	Chiusano dAsti	C658
005	 039 	005039	Cinaglio	C701
005	 040 	005040	Cisterna dAsti	C739
005	 041 	005041	Coazzolo	C804
005	 042 	005042	Cocconato	C807
005	 044 	005044	Corsione	D046
005	 045 	005045	Cortandone	D050
005	 046 	005046	Cortanze	D051
005	 047 	005047	Cortazzone	D052
005	 048 	005048	Cortiglione	D072
005	 049 	005049	Cossombrato	D101
005	 050 	005050	Costigliole dAsti	D119
005	 051 	005051	Cunico	D207
005	 052 	005052	Dusino San Michele	D388
005	 053 	005053	Ferrere	D554
005	 054 	005054	Fontanile	D678
005	 055 	005055	Frinco	D802
005	 056 	005056	Grana	E134
005	 057 	005057	Grazzano Badoglio	E159
005	 058 	005058	Incisa Scapaccino	E295
005	 059 	005059	Isola dAsti	E338
005	 060 	005060	Loazzolo	E633
005	 061 	005061	Maranzana	E917
005	 062 	005062	Maretto	E944
005	 063 	005063	Moasca	F254
005	 064 	005064	Mombaldone	F308
005	 065 	005065	Mombaruzzo	F311
005	 066 	005066	Mombercelli	F316
005	 067 	005067	Monale	F323
005	 068 	005068	Monastero Bormida	F325
005	 069 	005069	Moncalvo	F336
005	 070 	005070	Moncucco Torinese	F343
005	 071 	005071	Mongardino	F361
005	 072 	005072	Montabone	F386
005	 073 	005073	Montafia	F390
005	 074 	005074	Montaldo Scarampi	F409
005	 075 	005075	Montechiaro dAsti	F468
005	 076 	005076	Montegrosso dAsti	F527
005	 077 	005077	Montemagno	F556
005	 079 	005079	Moransengo	F709
005	 080 	005080	Nizza Monferrato	F902
005	 081 	005081	Olmo Gentile	G048
005	 082 	005082	Passerano Marmorito	G358
005	 083 	005083	Penango	G430
005	 084 	005084	Piea	G593
005	 085 	005085	Pino dAsti	G676
005	 086 	005086	PiovÔøΩ Massaia	G692
005	 087 	005087	Portacomaro	G894
005	 088 	005088	Quaranti	H102
005	 089 	005089	Refrancore	H219
005	 090 	005090	Revigliasco dAsti	H250
005	 091 	005091	Roatto	H366
005	 092 	005092	Robella	H376
005	 093 	005093	Rocca dArazzo	H392
005	 094 	005094	Roccaverano	H451
005	 095 	005095	Rocchetta Palafea	H466
005	 096 	005096	Rocchetta Tanaro	H468
005	 097 	005097	San Damiano dAsti	H811
005	 098 	005098	San Giorgio Scarampi	H899
005	 099 	005099	San Martino Alfieri	H987
005	 100 	005100	San Marzano Oliveto	I017
005	 101 	005101	San Paolo Solbrito	I076
005	 103 	005103	Scurzolengo	I555
005	 104 	005104	Serole	I637
005	 105 	005105	Sessame	I678
005	 106 	005106	Settime	I698
005	 107 	005107	Soglio	I781
005	 108 	005108	Tigliole	L168
005	 109 	005109	Tonco	L203
005	 110 	005110	Tonengo	L204
005	 111 	005111	Vaglio Serra	L531
005	 112 	005112	Valfenera	L574
005	 113 	005113	Vesime	L807
005	 114 	005114	Viale	L829
005	 115 	005115	Viarigi	L834
005	 116 	005116	Vigliano dAsti	L879
005	 117 	005117	Villafranca dAsti	L945
005	 118 	005118	Villanova dAsti	L984
005	 119 	005119	Villa San Secondo	M019
005	 120 	005120	Vinchio	M058
005	 121 	005121	Montiglio Monferrato	M302
006	 001 	006001	Acqui Terme	A052
006	 002 	006002	Albera Ligure	A146
006	 003 	006003	Alessandria	A182
006	 004 	006004	Alfiano Natta	A189
006	 005 	006005	Alice Bel Colle	A197
006	 006 	006006	Alluvioni CambiÔøΩ	A211
006	 007 	006007	Altavilla Monferrato	A227
006	 008 	006008	Alzano Scrivia	A245
006	 009 	006009	Arquata Scrivia	A436
006	 010 	006010	Avolasca	A523
006	 011 	006011	Balzola	A605
006	 012 	006012	Basaluzzo	A689
006	 013 	006013	Bassignana	A708
006	 014 	006014	Belforte Monferrato	A738
006	 015 	006015	Bergamasco	A793
006	 016 	006016	Berzano di Tortona	A813
006	 017 	006017	Bistagno	A889
006	 018 	006018	Borghetto di Borbera	A998
006	 019 	006019	Borgoratto Alessandrino	B029
006	 020 	006020	Borgo San Martino	B037
006	 021 	006021	Bosco Marengo	B071
006	 022 	006022	Bosio	B080
006	 023 	006023	Bozzole	B109
006	 024 	006024	Brignano-Frascata	B179
006	 025 	006025	Cabella Ligure	B311
006	 026 	006026	Camagna Monferrato	B453
006	 027 	006027	Camino	B482
006	 028 	006028	Cantalupo Ligure	B629
006	 029 	006029	Capriata dOrba	B701
006	 030 	006030	Carbonara Scrivia	B736
006	 031 	006031	Carentino	B765
006	 032 	006032	Carezzano	B769
006	 033 	006033	Carpeneto	B818
006	 034 	006034	Carrega Ligure	B836
006	 035 	006035	Carrosio	B840
006	 036 	006036	Cartosio	B847
006	 037 	006037	Casal Cermelli	B870
006	 038 	006038	Casaleggio Boiro	B882
006	 039 	006039	Casale Monferrato	B885
006	 040 	006040	Casalnoceto	B902
006	 041 	006041	Casasco	B941
006	 042 	006042	Cassano Spinola	C005
006	 043 	006043	Cassine	C027
006	 044 	006044	Cassinelle	C030
006	 045 	006045	Castellania	C137
006	 046 	006046	Castellar Guidobono	C142
006	 047 	006047	Castellazzo Bormida	C148
006	 048 	006048	Castelletto dErro	C156
006	 049 	006049	Castelletto dOrba	C158
006	 050 	006050	Castelletto Merli	C160
006	 051 	006051	Castelletto Monferrato	C162
006	 052 	006052	Castelnuovo Bormida	C229
006	 053 	006053	Castelnuovo Scrivia	C243
006	 054 	006054	Castelspina	C274
006	 055 	006055	Cavatore	C387
006	 056 	006056	Cella Monte	C432
006	 057 	006057	Cereseto	C503
006	 058 	006058	Cerreto Grue	C507
006	 059 	006059	Cerrina Monferrato	C531
006	 060 	006060	Coniolo	C962
006	 061 	006061	Conzano	C977
006	 062 	006062	Costa Vescovato	D102
006	 063 	006063	Cremolino	D149
006	 064 	006064	Cuccaro Monferrato	D194
006	 065 	006065	Denice	D272
006	 066 	006066	Dernice	D277
006	 067 	006067	Fabbrica Curone	D447
006	 068 	006068	Felizzano	D528
006	 069 	006069	Fraconalto	D559
006	 070 	006070	Francavilla Bisio	D759
006	 071 	006071	Frascaro	D770
006	 072 	006072	Frassinello Monferrato	D777
006	 073 	006073	Frassineto Po	D780
006	 074 	006074	Fresonara	D797
006	 075 	006075	Frugarolo	D813
006	 076 	006076	Fubine	D814
006	 077 	006077	Gabiano	D835
006	 078 	006078	Gamalero	D890
006	 079 	006079	Garbagna	D910
006	 080 	006080	Gavazzana	D941
006	 081 	006081	Gavi	D944
006	 082 	006082	Giarole	E015
006	 083 	006083	Gremiasco	E164
006	 084 	006084	Grognardo	E188
006	 085 	006085	Grondona	E191
006	 086 	006086	Guazzora	E255
006	 087 	006087	Isola SantAntonio	E360
006	 088 	006088	Lerma	E543
006	 089 	006089	Lu	E712
006	 090 	006090	Malvicino	E870
006	 091 	006091	Masio	F015
006	 092 	006092	Melazzo	F096
006	 093 	006093	Merana	F131
006	 094 	006094	Mirabello Monferrato	F232
006	 095 	006095	Molare	F281
006	 096 	006096	Molino dei Torti	F293
006	 097 	006097	Mombello Monferrato	F313
006	 098 	006098	Momperone	F320
006	 099 	006099	Moncestino	F337
006	 100 	006100	Mongiardino Ligure	F365
006	 101 	006101	Monleale	F374
006	 102 	006102	Montacuto	F387
006	 103 	006103	Montaldeo	F403
006	 104 	006104	Montaldo Bormida	F404
006	 105 	006105	Montecastello	F455
006	 106 	006106	Montechiaro dAcqui	F469
006	 107 	006107	Montegioco	F518
006	 108 	006108	Montemarzino	F562
006	 109 	006109	Morano sul Po	F707
006	 110 	006110	Morbello	F713
006	 111 	006111	Mornese	F737
006	 112 	006112	Morsasco	F751
006	 113 	006113	Murisengo	F814
006	 114 	006114	Novi Ligure	F965
006	 115 	006115	Occimiano	F995
006	 116 	006116	Odalengo Grande	F997
006	 117 	006117	Odalengo Piccolo	F998
006	 118 	006118	Olivola	G042
006	 119 	006119	Orsara Bormida	G124
006	 120 	006120	Ottiglio	G193
006	 121 	006121	Ovada	G197
006	 122 	006122	Oviglio	G199
006	 123 	006123	Ozzano Monferrato	G204
006	 124 	006124	Paderna	G215
006	 125 	006125	Pareto	G334
006	 126 	006126	Parodi Ligure	G338
006	 127 	006127	Pasturana	G367
006	 128 	006128	Pecetto di Valenza	G397
006	 129 	006129	Pietra Marazzi	G619
006	 130 	006130	Piovera	G695
006	 131 	006131	Pomaro Monferrato	G807
006	 132 	006132	Pontecurone	G839
006	 133 	006133	Pontestura	G858
006	 134 	006134	Ponti	G861
006	 135 	006135	Ponzano Monferrato	G872
006	 136 	006136	Ponzone	G877
006	 137 	006137	Pozzol Groppo	G960
006	 138 	006138	Pozzolo Formigaro	G961
006	 139 	006139	Prasco	G987
006	 140 	006140	Predosa	H021
006	 141 	006141	Quargnento	H104
006	 142 	006142	Quattordio	H121
006	 143 	006143	Ricaldone	H272
006	 144 	006144	Rivalta Bormida	H334
006	 145 	006145	Rivarone	H343
006	 146 	006146	Roccaforte Ligure	H406
006	 147 	006147	Rocca Grimalda	H414
006	 148 	006148	Rocchetta Ligure	H465
006	 149 	006149	Rosignano Monferrato	H569
006	 150 	006150	Sala Monferrato	H677
006	 151 	006151	Sale	H694
006	 152 	006152	San Cristoforo	H810
006	 153 	006153	San Giorgio Monferrato	H878
006	 154 	006154	San Salvatore Monferrato	I144
006	 155 	006155	San Sebastiano Curone	I150
006	 156 	006156	SantAgata Fossili	I190
006	 157 	006157	Sardigliano	I429
006	 158 	006158	Sarezzano	I432
006	 159 	006159	Serralunga di Crea	I645
006	 160 	006160	Serravalle Scrivia	I657
006	 161 	006161	Sezzadio	I711
006	 162 	006162	Silvano dOrba	I738
006	 163 	006163	Solero	I798
006	 164 	006164	Solonghello	I808
006	 165 	006165	Spigno Monferrato	I901
006	 166 	006166	Spineto Scrivia	I911
006	 167 	006167	Stazzano	I941
006	 168 	006168	Strevi	I977
006	 169 	006169	Tagliolo Monferrato	L027
006	 170 	006170	Tassarolo	L059
006	 171 	006171	Terruggia	L139
006	 172 	006172	Terzo	L143
006	 173 	006173	Ticineto	L165
006	 174 	006174	Tortona	L304
006	 175 	006175	Treville	L403
006	 176 	006176	Trisobbio	L432
006	 177 	006177	Valenza	L570
006	 178 	006178	Valmacca	L633
006	 179 	006179	Vignale Monferrato	L881
006	 180 	006180	Vignole Borbera	L887
006	 181 	006181	Viguzzolo	L904
006	 182 	006182	Villadeati	L931
006	 183 	006183	Villalvernia	L963
006	 184 	006184	Villamiroglio	L970
006	 185 	006185	Villanova Monferrato	L972
006	 186 	006186	Villaromagnano	M009
006	 187 	006187	Visone	M077
006	 188 	006188	Volpedo	M120
006	 189 	006189	Volpeglino	M121
006	 190 	006190	Voltaggio	M123
007	 001 	007001	Allein	A205
007	 002 	007002	Antey-Saint-AndrÔøΩ	A305
007	 003 	007003	Aosta	A326
007	 004 	007004	Arnad	A424
007	 005 	007005	Arvier	A452
007	 006 	007006	Avise	A521
007	 007 	007007	Ayas	A094
007	 008 	007008	Aymavilles	A108
007	 009 	007009	Bard	A643
007	 010 	007010	Bionaz	A877
007	 011 	007011	Brissogne	B192
007	 012 	007012	Brusson	B230
007	 013 	007013	Challand-Saint-Anselme	C593
007	 014 	007014	Challand-Saint-Victor	C594
007	 015 	007015	Chambave	C595
007	 016 	007016	Chamois	B491
007	 017 	007017	Champdepraz	C596
007	 018 	007018	Champorcher	B540
007	 019 	007019	Charvensod	C598
007	 020 	007020	ChÔøΩtillon	C294
007	 021 	007021	Cogne	C821
007	 022 	007022	Courmayeur	D012
007	 023 	007023	Donnas	D338
007	 024 	007024	Doues	D356
007	 025 	007025	EmarÔøΩse	D402
007	 026 	007026	Etroubles	D444
007	 027 	007027	FÔøΩnis	D537
007	 028 	007028	Fontainemore	D666
007	 029 	007029	Gaby	D839
007	 030 	007030	Gignod	E029
007	 031 	007031	Gressan	E165
007	 032 	007032	Gressoney-La-TrinitÔøΩ	E167
007	 033 	007033	Gressoney-Saint-Jean	E168
007	 034 	007034	HÔøΩne	E273
007	 035 	007035	Introd	E306
007	 036 	007036	Issime	E369
007	 037 	007037	Issogne	E371
007	 038 	007038	JovenÔøΩan	E391
007	 039 	007039	La Magdeleine	A308
007	 040 	007040	La Salle	E458
007	 041 	007041	La Thuile	E470
007	 042 	007042	Lillianes	E587
007	 043 	007043	Montjovet	F367
007	 044 	007044	Morgex	F726
007	 045 	007045	Nus	F987
007	 046 	007046	Ollomont	G045
007	 047 	007047	Oyace	G012
007	 048 	007048	Perloz	G459
007	 049 	007049	Pollein	G794
007	 050 	007050	Pontboset	G545
007	 051 	007051	Pontey	G860
007	 052 	007052	Pont-Saint-Martin	G854
007	 053 	007053	PrÔøΩ-Saint-Didier	H042
007	 054 	007054	Quart	H110
007	 055 	007055	RhÔøΩmes-Notre-Dame	H262
007	 056 	007056	RhÔøΩmes-Saint-Georges	H263
007	 057 	007057	Roisan	H497
007	 058 	007058	Saint-Christophe	H669
007	 059 	007059	Saint-Denis	H670
007	 060 	007060	Saint-Marcel	H671
007	 061 	007061	Saint-Nicolas	H672
007	 062 	007062	Saint-Oyen	H673
007	 063 	007063	Saint-Pierre	H674
007	 064 	007064	Saint-RhÔøΩmy-en-Bosses	H675
007	 065 	007065	Saint-Vincent	H676
007	 066 	007066	Sarre	I442
007	 067 	007067	Torgnon	L217
007	 068 	007068	Valgrisenche	L582
007	 069 	007069	Valpelline	L643
007	 070 	007070	Valsavarenche	L647
007	 071 	007071	Valtournenche	L654
007	 072 	007072	Verrayes	L783
007	 073 	007073	VerrÔøΩs	C282
007	 074 	007074	Villeneuve	L981
008	 001 	008001	Airole	A111
008	 002 	008002	Apricale	A338
008	 003 	008003	Aquila dArroscia	A344
008	 004 	008004	Armo	A418
008	 005 	008005	Aurigo	A499
008	 006 	008006	Badalucco	A536
008	 007 	008007	Bajardo	A581
008	 008 	008008	Bordighera	A984
008	 009 	008009	Borghetto dArroscia	A993
008	 010 	008010	Borgomaro	B020
008	 011 	008011	Camporosso	B559
008	 012 	008012	Caravonica	B734
008	 013 	008013	Carpasio	B814
008	 014 	008014	Castellaro	C143
008	 015 	008015	Castel Vittorio	C110
008	 016 	008016	Ceriana	C511
008	 017 	008017	Cervo	C559
008	 018 	008018	Cesio	C578
008	 019 	008019	Chiusanico	C657
008	 020 	008020	Chiusavecchia	C660
008	 021 	008021	Cipressa	C718
008	 022 	008022	Civezza	C755
008	 023 	008023	Cosio dArroscia	D087
008	 024 	008024	Costarainera	D114
008	 025 	008025	Diano Arentino	D293
008	 026 	008026	Diano Castello	D296
008	 027 	008027	Diano Marina	D297
008	 028 	008028	Diano San Pietro	D298
008	 029 	008029	Dolceacqua	D318
008	 030 	008030	Dolcedo	D319
008	 031 	008031	Imperia	E290
008	 032 	008032	Isolabona	E346
008	 033 	008033	Lucinasco	E719
008	 034 	008034	Mendatica	F123
008	 035 	008035	Molini di Triora	F290
008	 036 	008036	Montalto Ligure	F406
008	 037 	008037	Montegrosso Pian Latte	F528
008	 038 	008038	Olivetta San Michele	G041
008	 039 	008039	Ospedaletti	G164
008	 040 	008040	Perinaldo	G454
008	 041 	008041	Pietrabruna	G607
008	 042 	008042	Pieve di Teco	G632
008	 043 	008043	Pigna	G660
008	 044 	008044	Pompeiana	G814
008	 045 	008045	Pontedassio	G840
008	 046 	008046	Pornassio	G890
008	 047 	008047	PrelÔøΩ	H027
008	 048 	008048	Ranzo	H180
008	 049 	008049	Rezzo	H257
008	 050 	008050	Riva Ligure	H328
008	 051 	008051	Rocchetta Nervina	H460
008	 052 	008052	San Bartolomeo al Mare	H763
008	 053 	008053	San Biagio della Cima	H780
008	 054 	008054	San Lorenzo al Mare	H957
008	 055 	008055	Sanremo	I138
008	 056 	008056	Santo Stefano al Mare	I365
008	 057 	008057	Seborga	I556
008	 058 	008058	Soldano	I796
008	 059 	008059	Taggia	L024
008	 060 	008060	Terzorio	L146
008	 061 	008061	Triora	L430
008	 062 	008062	Vallebona	L596
008	 063 	008063	Vallecrosia	L599
008	 064 	008064	Vasia	L693
008	 065 	008065	Ventimiglia	L741
008	 066 	008066	Vessalico	L809
008	 067 	008067	Villa Faraldi	L943
009	 001 	009001	Alassio	A122
009	 002 	009002	Albenga	A145
009	 003 	009003	Albissola Marina	A165
009	 004 	009004	Albisola Superiore	A166
009	 005 	009005	Altare	A226
009	 006 	009006	Andora	A278
009	 007 	009007	Arnasco	A422
009	 008 	009008	Balestrino	A593
009	 009 	009009	Bardineto	A647
009	 010 	009010	Bergeggi	A796
009	 011 	009011	Boissano	A931
009	 012 	009012	Borghetto Santo Spirito	A999
009	 013 	009013	Borgio Verezzi	B005
009	 014 	009014	Bormida	B048
009	 015 	009015	Cairo Montenotte	B369
009	 016 	009016	Calice Ligure	B409
009	 017 	009017	Calizzano	B416
009	 018 	009018	Carcare	B748
009	 019 	009019	Casanova Lerrone	B927
009	 020 	009020	Castelbianco	C063
009	 021 	009021	Castelvecchio di Rocca Barbena	C276
009	 022 	009022	Celle Ligure	C443
009	 023 	009023	Cengio	C463
009	 024 	009024	Ceriale	C510
009	 025 	009025	Cisano sul Neva	C729
009	 026 	009026	Cosseria	D095
009	 027 	009027	Dego	D264
009	 028 	009028	Erli	D424
009	 029 	009029	Finale Ligure	D600
009	 030 	009030	Garlenda	D927
009	 031 	009031	Giustenice	E064
009	 032 	009032	Giusvalla	E066
009	 033 	009033	Laigueglia	E414
009	 034 	009034	Loano	E632
009	 035 	009035	Magliolo	E816
009	 036 	009036	Mallare	E860
009	 037 	009037	Massimino	F046
009	 038 	009038	Millesimo	F213
009	 039 	009039	Mioglia	F226
009	 040 	009040	Murialdo	F813
009	 041 	009041	Nasino	F847
009	 042 	009042	Noli	F926
009	 043 	009043	Onzo	G076
009	 044 	009044	Orco Feglino	D522
009	 045 	009045	Ortovero	G144
009	 046 	009046	Osiglia	G155
009	 047 	009047	Pallare	G281
009	 048 	009048	Piana Crixia	G542
009	 049 	009049	Pietra Ligure	G605
009	 050 	009050	Plodio	G741
009	 051 	009051	Pontinvrea	G866
009	 052 	009052	Quiliano	H126
009	 053 	009053	Rialto	H266
009	 054 	009054	Roccavignale	H452
009	 055 	009055	Sassello	I453
009	 056 	009056	Savona	I480
009	 057 	009057	Spotorno	I926
009	 058 	009058	Stella	I946
009	 059 	009059	Stellanello	I947
009	 060 	009060	Testico	L152
009	 061 	009061	Toirano	L190
009	 062 	009062	Tovo San Giacomo	L315
009	 063 	009063	Urbe	L499
009	 064 	009064	Vado Ligure	L528
009	 065 	009065	Varazze	L675
009	 066 	009066	Vendone	L730
009	 067 	009067	Vezzi Portio	L823
009	 068 	009068	Villanova dAlbenga	L975
009	 069 	009069	Zuccarello	M197
010	 001 	010001	Arenzano	A388
010	 002 	010002	Avegno	A506
010	 003 	010003	Bargagli	A658
010	 004 	010004	Bogliasco	A922
010	 005 	010005	Borzonasca	B067
010	 006 	010006	Busalla	B282
010	 007 	010007	Camogli	B490
010	 008 	010008	Campo Ligure	B538
010	 009 	010009	Campomorone	B551
010	 010 	010010	Carasco	B726
010	 011 	010011	Casarza Ligure	B939
010	 012 	010012	Casella	B956
010	 013 	010013	Castiglione Chiavarese	C302
010	 014 	010014	Ceranesi	C481
010	 015 	010015	Chiavari	C621
010	 016 	010016	Cicagna	C673
010	 017 	010017	Cogoleto	C823
010	 018 	010018	Cogorno	C826
010	 019 	010019	Coreglia Ligure	C995
010	 020 	010020	Crocefieschi	D175
010	 021 	010021	Davagna	D255
010	 022 	010022	Fascia	D509
010	 023 	010023	Favale di Malvaro	D512
010	 024 	010024	Fontanigorda	D677
010	 025 	010025	Genova	D969
010	 026 	010026	Gorreto	E109
010	 027 	010027	Isola del Cantone	E341
010	 028 	010028	Lavagna	E488
010	 029 	010029	Leivi	E519
010	 030 	010030	Lorsica	E695
010	 031 	010031	Lumarzo	E737
010	 032 	010032	Masone	F020
010	 033 	010033	Mele	F098
010	 034 	010034	Mezzanego	F173
010	 035 	010035	Mignanego	F202
010	 036 	010036	Moconesi	F256
010	 037 	010037	Moneglia	F354
010	 038 	010038	Montebruno	F445
010	 039 	010039	Montoggio	F682
010	 040 	010040	Ne	F858
010	 041 	010041	Neirone	F862
010	 042 	010042	Orero	G093
010	 043 	010043	Pieve Ligure	G646
010	 044 	010044	Portofino	G913
010	 045 	010045	Propata	H073
010	 046 	010046	Rapallo	H183
010	 047 	010047	Recco	H212
010	 048 	010048	Rezzoaglio	H258
010	 049 	010049	Ronco Scrivia	H536
010	 050 	010050	Rondanina	H546
010	 051 	010051	Rossiglione	H581
010	 052 	010052	Rovegno	H599
010	 053 	010053	San Colombano Certenoli	H802
010	 054 	010054	Santa Margherita Ligure	I225
010	 055 	010055	SantOlcese	I346
010	 056 	010056	Santo Stefano dAveto	I368
010	 057 	010057	Savignone	I475
010	 058 	010058	Serra RiccÔøΩ	I640
010	 059 	010059	Sestri Levante	I693
010	 060 	010060	Sori	I852
010	 061 	010061	Tiglieto	L167
010	 062 	010062	Torriglia	L298
010	 063 	010063	Tribogna	L416
010	 064 	010064	Uscio	L507
010	 065 	010065	Valbrevenna	L546
010	 066 	010066	Vobbia	M105
010	 067 	010067	Zoagli	M182
011	 001 	011001	Ameglia	A261
011	 002 	011002	Arcola	A373
011	 003 	011003	Beverino	A836
011	 004 	011004	Bolano	A932
011	 005 	011005	Bonassola	A961
011	 006 	011006	Borghetto di Vara	A992
011	 007 	011007	Brugnato	B214
011	 008 	011008	Calice al Cornoviglio	B410
011	 009 	011009	Carro	B838
011	 010 	011010	Carrodano	B839
011	 011 	011011	Castelnuovo Magra	C240
011	 012 	011012	Deiva Marina	D265
011	 013 	011013	Follo	D655
011	 014 	011014	Framura	D758
011	 015 	011015	La Spezia	E463
011	 016 	011016	Lerici	E542
011	 017 	011017	Levanto	E560
011	 018 	011018	Maissana	E842
011	 019 	011019	Monterosso al Mare	F609
011	 020 	011020	Ortonovo	G143
011	 021 	011021	Pignone	G664
011	 022 	011022	Portovenere	G925
011	 023 	011023	RiccÔøΩ del Golfo di Spezia	H275
011	 024 	011024	Riomaggiore	H304
011	 025 	011025	Rocchetta di Vara	H461
011	 026 	011026	Santo Stefano di Magra	I363
011	 027 	011027	Sarzana	I449
011	 028 	011028	Sesta Godano	E070
011	 029 	011029	Varese Ligure	L681
011	 030 	011030	Vernazza	L774
011	 031 	011031	Vezzano Ligure	L819
011	 032 	011032	Zignago	M177
012	 001 	012001	Agra	A085
012	 002 	012002	Albizzate	A167
012	 003 	012003	Angera	A290
012	 004 	012004	Arcisate	A371
012	 005 	012005	Arsago Seprio	A441
012	 006 	012006	Azzate	A531
012	 007 	012007	Azzio	A532
012	 008 	012008	Barasso	A619
012	 009 	012009	Bardello	A645
012	 010 	012010	Bedero Valcuvia	A728
012	 011 	012011	Besano	A819
012	 012 	012012	Besnate	A825
012	 013 	012013	Besozzo	A826
012	 014 	012014	Biandronno	A845
012	 015 	012015	Bisuschio	A891
012	 016 	012016	Bodio Lomnago	A918
012	 017 	012017	Brebbia	B126
012	 018 	012018	Bregano	B131
012	 019 	012019	Brenta	B150
012	 020 	012020	Brezzo di Bedero	B166
012	 021 	012021	Brinzio	B182
012	 022 	012022	Brissago-Valtravaglia	B191
012	 023 	012023	Brunello	B219
012	 024 	012024	Brusimpiano	B228
012	 025 	012025	Buguggiate	B258
012	 026 	012026	Busto Arsizio	B300
012	 027 	012027	Cadegliano-Viconago	B326
012	 028 	012028	Cadrezzate	B347
012	 029 	012029	Cairate	B368
012	 030 	012030	Cantello	B634
012	 031 	012031	Caravate	B732
012	 032 	012032	Cardano al Campo	B754
012	 033 	012033	Carnago	B796
012	 034 	012034	Caronno Pertusella	B805
012	 035 	012035	Caronno Varesino	B807
012	 036 	012036	Casale Litta	B875
012	 037 	012037	Casalzuigno	B921
012	 038 	012038	Casciago	B949
012	 039 	012039	Casorate Sempione	B987
012	 040 	012040	Cassano Magnago	C004
012	 041 	012041	Cassano Valcuvia	B999
012	 042 	012042	Castellanza	C139
012	 043 	012043	Castello Cabiaglio	B312
012	 044 	012044	Castelseprio	C273
012	 045 	012045	Castelveccana	C181
012	 046 	012046	Castiglione Olona	C300
012	 047 	012047	Castronno	C343
012	 048 	012048	Cavaria con Premezzo	C382
012	 049 	012049	Cazzago Brabbia	C409
012	 050 	012050	Cislago	C732
012	 051 	012051	Cittiglio	C751
012	 052 	012052	Clivio	C796
012	 053 	012053	Cocquio-Trevisago	C810
012	 054 	012054	Comabbio	C911
012	 055 	012055	Comerio	C922
012	 056 	012056	Cremenaga	D144
012	 057 	012057	Crosio della Valle	D185
012	 058 	012058	Cuasso al Monte	D192
012	 059 	012059	Cugliate-Fabiasco	D199
012	 060 	012060	Cunardo	D204
012	 061 	012061	Curiglia con Monteviasco	D217
012	 062 	012062	Cuveglio	D238
012	 063 	012063	Cuvio	D239
012	 064 	012064	Daverio	D256
012	 065 	012065	Dumenza	D384
012	 066 	012066	Duno	D385
012	 067 	012067	Fagnano Olona	D467
012	 068 	012068	Ferno	D543
012	 069 	012069	Ferrera di Varese	D551
012	 070 	012070	Gallarate	D869
012	 071 	012071	Galliate Lombardo	D871
012	 072 	012072	Gavirate	D946
012	 073 	012073	Gazzada Schianno	D951
012	 074 	012074	Gemonio	D963
012	 075 	012075	Gerenzano	D981
012	 076 	012076	Germignaga	D987
012	 077 	012077	Golasecca	E079
012	 078 	012078	Gorla Maggiore	E101
012	 079 	012079	Gorla Minore	E102
012	 080 	012080	Gornate Olona	E104
012	 081 	012081	Grantola	E144
012	 082 	012082	Inarzo	E292
012	 083 	012083	Induno Olona	E299
012	 084 	012084	Ispra	E367
012	 085 	012085	Jerago con Orago	E386
012	 086 	012086	Lavena Ponte Tresa	E494
012	 087 	012087	Laveno-Mombello	E496
012	 088 	012088	Leggiuno	E510
012	 089 	012089	Lonate Ceppino	E665
012	 090 	012090	Lonate Pozzolo	E666
012	 091 	012091	Lozza	E707
012	 092 	012092	Luino	E734
012	 093 	012093	Luvinate	E769
012	 094 	012094	Maccagno	E775
012	 095 	012095	Malgesso	E856
012	 096 	012096	Malnate	E863
012	 097 	012097	Marchirolo	E929
012	 098 	012098	Marnate	E965
012	 099 	012099	Marzio	F002
012	 100 	012100	Masciago Primo	F007
012	 101 	012101	Mercallo	F134
012	 102 	012102	Mesenzana	F154
012	 103 	012103	Montegrino Valtravaglia	F526
012	 104 	012104	Monvalle	F703
012	 105 	012105	Morazzone	F711
012	 106 	012106	Mornago	F736
012	 107 	012107	Oggiona con Santo Stefano	G008
012	 108 	012108	Olgiate Olona	G028
012	 109 	012109	Origgio	G103
012	 110 	012110	Orino	G105
012	 111 	012111	Osmate	E529
012	 112 	012112	Pino sulla Sponda del Lago Maggiore	G677
012	 113 	012113	Porto Ceresio	G906
012	 114 	012114	Porto Valtravaglia	G907
012	 115 	012115	Rancio Valcuvia	H173
012	 116 	012116	Ranco	H174
012	 117 	012117	Saltrio	H723
012	 118 	012118	Samarate	H736
012	 119 	012119	Saronno	I441
012	 120 	012120	Sesto Calende	I688
012	 121 	012121	Solbiate Arno	I793
012	 122 	012122	Solbiate Olona	I794
012	 123 	012123	Somma Lombardo	I819
012	 124 	012124	Sumirago	L003
012	 125 	012125	Taino	L032
012	 126 	012126	Ternate	L115
012	 127 	012127	Tradate	L319
012	 128 	012128	Travedona-Monate	L342
012	 129 	012129	Tronzano Lago Maggiore	A705
012	 130 	012130	Uboldo	L480
012	 131 	012131	Valganna	L577
012	 132 	012132	Varano Borghi	L671
012	 133 	012133	Varese	L682
012	 134 	012134	Vedano Olona	L703
012	 135 	012135	Veddasca	L705
012	 136 	012136	Venegono Inferiore	L733
012	 137 	012137	Venegono Superiore	L734
012	 138 	012138	Vergiate	L765
012	 139 	012139	ViggiÔøΩ	L876
012	 140 	012140	Vizzola Ticino	M101
012	 141 	012141	Sangiano	H872
013	 003 	013003	Albavilla	A143
013	 004 	013004	Albese con Cassano	A153
013	 005 	013005	Albiolo	A164
013	 006 	013006	Alserio	A224
013	 007 	013007	Alzate Brianza	A249
013	 009 	013009	Anzano del Parco	A319
013	 010 	013010	Appiano Gentile	A333
013	 011 	013011	Argegno	A391
013	 012 	013012	Arosio	A430
013	 013 	013013	Asso	A476
013	 015 	013015	Barni	A670
013	 019 	013019	Bellagio	A744
013	 021 	013021	Bene Lario	A778
013	 022 	013022	Beregazzo con Figliaro	A791
013	 023 	013023	Binago	A870
013	 024 	013024	Bizzarone	A898
013	 025 	013025	Blessagno	A904
013	 026 	013026	Blevio	A905
013	 028 	013028	Bregnano	B134
013	 029 	013029	Brenna	B144
013	 030 	013030	Brienno	B172
013	 032 	013032	Brunate	B218
013	 034 	013034	Bulgarograsso	B262
013	 035 	013035	Cabiate	B313
013	 036 	013036	Cadorago	B346
013	 037 	013037	Caglio	B355
013	 038 	013038	Cagno	B359
013	 040 	013040	Campione dItalia	B513
013	 041 	013041	CantÔøΩ	B639
013	 042 	013042	Canzo	B641
013	 043 	013043	Capiago Intimiano	B653
013	 044 	013044	Carate Urio	B730
013	 045 	013045	Carbonate	B742
013	 046 	013046	Carimate	B778
013	 047 	013047	Carlazzo	B785
013	 048 	013048	Carugo	B851
013	 050 	013050	Casasco dIntelvi	B942
013	 052 	013052	Caslino dErba	B974
013	 053 	013053	Casnate con Bernate	B977
013	 055 	013055	Cassina Rizzardi	C020
013	 058 	013058	Castelmarte	C206
013	 059 	013059	Castelnuovo Bozzente	C220
013	 060 	013060	Castiglione dIntelvi	C299
013	 061 	013061	Cavallasca	C374
013	 062 	013062	Cavargna	C381
013	 063 	013063	Cerano dIntelvi	C482
013	 064 	013064	Cermenate	C516
013	 065 	013065	Cernobbio	C520
013	 068 	013068	Cirimido	C724
013	 070 	013070	Civenna	C754
013	 071 	013071	Claino con Osteno	C787
013	 074 	013074	Colonno	C902
013	 075 	013075	Como	C933
013	 077 	013077	Corrido	D041
013	 083 	013083	Cremia	D147
013	 084 	013084	Cucciago	D196
013	 085 	013085	Cusino	D232
013	 087 	013087	Dizzasco	D310
013	 089 	013089	Domaso	D329
013	 090 	013090	Dongo	D341
013	 092 	013092	Dosso del Liro	D355
013	 093 	013093	Drezzo	D369
013	 095 	013095	Erba	D416
013	 097 	013097	Eupilio	D445
013	 098 	013098	Faggeto Lario	D462
013	 099 	013099	Faloppio	D482
013	 100 	013100	FenegrÔøΩ	D531
013	 101 	013101	Figino Serenza	D579
013	 102 	013102	Fino Mornasco	D605
013	 106 	013106	Garzeno	D930
013	 107 	013107	Gera Lario	D974
013	 109 	013109	Gironico	E051
013	 110 	013110	Grandate	E139
013	 111 	013111	Grandola ed Uniti	E141
013	 113 	013113	Griante	E172
013	 114 	013114	Guanzate	E235
013	 118 	013118	Inverigo	E309
013	 119 	013119	Laglio	E405
013	 120 	013120	Laino	E416
013	 121 	013121	Lambrugo	E428
013	 122 	013122	Lanzo dIntelvi	E444
013	 123 	013123	Lasnigo	E462
013	 125 	013125	Lenno	E525
013	 126 	013126	Lezzeno	E569
013	 128 	013128	Limido Comasco	E593
013	 129 	013129	Lipomo	E607
013	 130 	013130	Livo	E623
013	 131 	013131	Locate Varesino	E638
013	 133 	013133	Lomazzo	E659
013	 134 	013134	Longone al Segrino	E679
013	 135 	013135	Luisago	E735
013	 136 	013136	Lurago dErba	E749
013	 137 	013137	Lurago Marinone	E750
013	 138 	013138	Lurate Caccivio	E753
013	 139 	013139	Magreglio	E830
013	 143 	013143	Mariano Comense	E951
013	 144 	013144	Maslianico	F017
013	 145 	013145	Menaggio	F120
013	 147 	013147	Merone	F151
013	 148 	013148	Mezzegra	F181
013	 152 	013152	Moltrasio	F305
013	 153 	013153	Monguzzo	F372
013	 154 	013154	Montano Lucino	F427
013	 155 	013155	Montemezzo	F564
013	 157 	013157	Montorfano	F688
013	 159 	013159	Mozzate	F788
013	 160 	013160	Musso	F828
013	 161 	013161	Nesso	F877
013	 163 	013163	Novedrate	F958
013	 165 	013165	Olgiate Comasco	G025
013	 169 	013169	Oltrona di San Mamette	G056
013	 170 	013170	Orsenigo	G126
013	 172 	013172	Ossuccio	G182
013	 175 	013175	ParÔøΩ	G329
013	 178 	013178	Peglio	G415
013	 179 	013179	Pellio Intelvi	G427
013	 183 	013183	Pianello del Lario	G556
013	 184 	013184	Pigra	G665
013	 185 	013185	Plesio	G737
013	 186 	013186	Pognana Lario	G773
013	 187 	013187	Ponna	G821
013	 188 	013188	Ponte Lambro	G847
013	 189 	013189	Porlezza	G889
013	 192 	013192	Proserpio	H074
013	 193 	013193	Pusiano	H094
013	 194 	013194	Ramponio Verna	H171
013	 195 	013195	Rezzago	H255
013	 197 	013197	Rodero	H478
013	 199 	013199	Ronago	H521
013	 201 	013201	Rovellasca	H601
013	 202 	013202	Rovello Porro	H602
013	 203 	013203	Sala Comacina	H679
013	 204 	013204	San Bartolomeo Val Cavargna	H760
013	 205 	013205	San Fedele Intelvi	H830
013	 206 	013206	San Fermo della Battaglia	H840
013	 207 	013207	San Nazzaro Val Cavargna	I051
013	 211 	013211	Schignano	I529
013	 212 	013212	Senna Comasco	I611
013	 215 	013215	Solbiate	I792
013	 216 	013216	Sorico	I856
013	 217 	013217	Sormano	I860
013	 218 	013218	Stazzona	I943
013	 222 	013222	Tavernerio	L071
013	 223 	013223	Torno	L228
013	 225 	013225	Tremezzo	L371
013	 226 	013226	Trezzone	L413
013	 227 	013227	Turate	L470
013	 228 	013228	Uggiate-Trevano	L487
013	 229 	013229	Valbrona	L547
013	 232 	013232	Valmorea	L640
013	 233 	013233	Val Rezzo	H259
013	 234 	013234	Valsolda	C936
013	 236 	013236	Veleso	L715
013	 238 	013238	Veniano	L737
013	 239 	013239	Vercana	L748
013	 242 	013242	Vertemate con Minoprio	L792
013	 245 	013245	Villa Guardia	L956
013	 246 	013246	Zelbio	M156
013	 248 	013248	San Siro	I162
013	 249 	013249	Gravedona ed Uniti	M315
014	 001 	014001	Albaredo per San Marco	A135
014	 002 	014002	Albosaggia	A172
014	 003 	014003	Andalo Valtellino	A273
014	 004 	014004	Aprica	A337
014	 005 	014005	Ardenno	A382
014	 006 	014006	Bema	A777
014	 007 	014007	Berbenno di Valtellina	A787
014	 008 	014008	Bianzone	A848
014	 009 	014009	Bormio	B049
014	 010 	014010	Buglio in Monte	B255
014	 011 	014011	Caiolo	B366
014	 012 	014012	Campodolcino	B530
014	 013 	014013	Caspoggio	B993
014	 014 	014014	Castello dellAcqua	C186
014	 015 	014015	Castione Andevenno	C325
014	 016 	014016	Cedrasco	C418
014	 017 	014017	Cercino	C493
014	 018 	014018	Chiavenna	C623
014	 019 	014019	Chiesa in Valmalenco	C628
014	 020 	014020	Chiuro	C651
014	 021 	014021	Cino	C709
014	 022 	014022	Civo	C785
014	 023 	014023	Colorina	C903
014	 024 	014024	Cosio Valtellino	D088
014	 025 	014025	Dazio	D258
014	 026 	014026	Delebio	D266
014	 027 	014027	Dubino	D377
014	 028 	014028	Faedo Valtellino	D456
014	 029 	014029	Forcola	D694
014	 030 	014030	Fusine	D830
014	 031 	014031	Gerola Alta	D990
014	 032 	014032	Gordona	E090
014	 033 	014033	Grosio	E200
014	 034 	014034	Grosotto	E201
014	 035 	014035	Madesimo	E342
014	 036 	014036	Lanzada	E443
014	 037 	014037	Livigno	E621
014	 038 	014038	Lovero	E705
014	 039 	014039	Mantello	E896
014	 040 	014040	Mazzo di Valtellina	F070
014	 041 	014041	Mello	F115
014	 042 	014042	Menarola	F121
014	 043 	014043	Mese	F153
014	 044 	014044	Montagna in Valtellina	F393
014	 045 	014045	Morbegno	F712
014	 046 	014046	Novate Mezzola	F956
014	 047 	014047	Pedesina	G410
014	 048 	014048	Piantedo	G572
014	 049 	014049	Piateda	G576
014	 050 	014050	Piuro	G718
014	 051 	014051	Poggiridenti	G431
014	 052 	014052	Ponte in Valtellina	G829
014	 053 	014053	Postalesio	G937
014	 054 	014054	Prata Camportaccio	G993
014	 055 	014055	Rasura	H192
014	 056 	014056	Rogolo	H493
014	 057 	014057	Samolaco	H752
014	 058 	014058	San Giacomo Filippo	H868
014	 059 	014059	Sernio	I636
014	 060 	014060	Sondalo	I828
014	 061 	014061	Sondrio	I829
014	 062 	014062	Spriana	I928
014	 063 	014063	Talamona	L035
014	 064 	014064	Tartano	L056
014	 065 	014065	Teglio	L084
014	 066 	014066	Tirano	L175
014	 067 	014067	Torre di Santa Maria	L244
014	 068 	014068	Tovo di SantAgata	L316
014	 069 	014069	Traona	L330
014	 070 	014070	Tresivio	L392
014	 071 	014071	Valdidentro	L557
014	 072 	014072	Valdisotto	L563
014	 073 	014073	Valfurva	L576
014	 074 	014074	Val Masino	L638
014	 075 	014075	Verceia	L749
014	 076 	014076	Vervio	L799
014	 077 	014077	Villa di Chiavenna	L907
014	 078 	014078	Villa di Tirano	L908
015	 002 	015002	Abbiategrasso	A010
015	 005 	015005	Albairate	A127
015	 007 	015007	Arconate	A375
015	 009 	015009	Arese	A389
015	 010 	015010	Arluno	A413
015	 011 	015011	Assago	A473
015	 012 	015012	Bareggio	A652
015	 014 	015014	Basiano	A697
015	 015 	015015	Basiglio	A699
015	 016 	015016	Bellinzago Lombardo	A751
015	 019 	015019	Bernate Ticino	A804
015	 022 	015022	Besate	A820
015	 024 	015024	Binasco	A872
015	 026 	015026	Boffalora sopra Ticino	A920
015	 027 	015027	Bollate	A940
015	 032 	015032	Bresso	B162
015	 035 	015035	Bubbiano	B235
015	 036 	015036	Buccinasco	B240
015	 038 	015038	Buscate	B286
015	 040 	015040	Bussero	B292
015	 041 	015041	Busto Garolfo	B301
015	 042 	015042	Calvignasco	B448
015	 044 	015044	Cambiago	B461
015	 046 	015046	Canegrate	B593
015	 050 	015050	Carpiano	B820
015	 051 	015051	Carugate	B850
015	 055 	015055	Casarile	B938
015	 058 	015058	Casorezzo	B989
015	 059 	015059	Cassano dAdda	C003
015	 060 	015060	Cassina de Pecchi	C014
015	 061 	015061	Cassinetta di Lugagnano	C033
015	 062 	015062	Castano Primo	C052
015	 070 	015070	Cernusco sul Naviglio	C523
015	 071 	015071	Cerro al Lambro	C536
015	 072 	015072	Cerro Maggiore	C537
015	 074 	015074	Cesano Boscone	C565
015	 076 	015076	Cesate	C569
015	 077 	015077	Cinisello Balsamo	C707
015	 078 	015078	Cisliano	C733
015	 081 	015081	Cologno Monzese	C895
015	 082 	015082	Colturano	C908
015	 085 	015085	Corbetta	C986
015	 086 	015086	Cormano	D013
015	 087 	015087	Cornaredo	D018
015	 093 	015093	Corsico	D045
015	 096 	015096	Cuggiono	D198
015	 097 	015097	Cusago	D229
015	 098 	015098	Cusano Milanino	D231
015	 099 	015099	Dairago	D244
015	 101 	015101	Dresano	D367
015	 103 	015103	Gaggiano	D845
015	 105 	015105	Garbagnate Milanese	D912
015	 106 	015106	Gessate	D995
015	 108 	015108	Gorgonzola	E094
015	 110 	015110	Grezzago	E170
015	 112 	015112	Gudo Visconti	E258
015	 113 	015113	Inveruno	E313
015	 114 	015114	Inzago	E317
015	 115 	015115	Lacchiarella	E395
015	 116 	015116	Lainate	E415
015	 118 	015118	Legnano	E514
015	 122 	015122	Liscate	E610
015	 125 	015125	Locate di Triulzi	E639
015	 130 	015130	Magenta	E801
015	 131 	015131	Magnago	E819
015	 134 	015134	Marcallo con Casone	E921
015	 136 	015136	Masate	F003
015	 139 	015139	Mediglia	F084
015	 140 	015140	Melegnano	F100
015	 142 	015142	Melzo	F119
015	 144 	015144	Mesero	F155
015	 146 	015146	Milano	F205
015	 150 	015150	Morimondo	D033
015	 151 	015151	Motta Visconti	F783
015	 154 	015154	Nerviano	F874
015	 155 	015155	Nosate	F939
015	 157 	015157	Novate Milanese	F955
015	 158 	015158	Noviglio	F968
015	 159 	015159	Opera	G078
015	 164 	015164	Ossona	G181
015	 165 	015165	Ozzero	G206
015	 166 	015166	Paderno Dugnano	G220
015	 167 	015167	Pantigliate	G316
015	 168 	015168	Parabiago	G324
015	 169 	015169	Paullo	G385
015	 170 	015170	Pero	C013
015	 171 	015171	Peschiera Borromeo	G488
015	 172 	015172	Pessano con Bornago	G502
015	 173 	015173	Pieve Emanuele	G634
015	 175 	015175	Pioltello	G686
015	 176 	015176	Pogliano Milanese	G772
015	 177 	015177	Pozzo dAdda	G955
015	 178 	015178	Pozzuolo Martesana	G965
015	 179 	015179	Pregnana Milanese	H026
015	 181 	015181	Rescaldina	H240
015	 182 	015182	Rho	H264
015	 183 	015183	Robecchetto con Induno	H371
015	 184 	015184	Robecco sul Naviglio	H373
015	 185 	015185	Rodano	H470
015	 188 	015188	Rosate	H560
015	 189 	015189	Rozzano	H623
015	 191 	015191	San Colombano al Lambro	H803
015	 192 	015192	San Donato Milanese	H827
015	 194 	015194	San Giorgio su Legnano	H884
015	 195 	015195	San Giuliano Milanese	H930
015	 200 	015200	Santo Stefano Ticino	I361
015	 201 	015201	San Vittore Olona	I409
015	 202 	015202	San Zenone al Lambro	I415
015	 204 	015204	Sedriano	I566
015	 205 	015205	Segrate	I577
015	 206 	015206	Senago	I602
015	 209 	015209	Sesto San Giovanni	I690
015	 210 	015210	Settala	I696
015	 211 	015211	Settimo Milanese	I700
015	 213 	015213	Solaro	I786
015	 219 	015219	Trezzano Rosa	L408
015	 220 	015220	Trezzano sul Naviglio	L409
015	 221 	015221	Trezzo sullAdda	L411
015	 222 	015222	Tribiano	L415
015	 224 	015224	Truccazzano	L454
015	 226 	015226	Turbigo	L471
015	 229 	015229	Vanzago	L665
015	 230 	015230	Vaprio dAdda	L667
015	 235 	015235	Vermezzo	L768
015	 236 	015236	Vernate	L773
015	 237 	015237	Vignate	L883
015	 242 	015242	Vimodrone	M053
015	 243 	015243	Vittuone	M091
015	 244 	015244	Vizzolo Predabissi	M102
015	 246 	015246	Zelo Surrigone	M160
015	 247 	015247	Zibido San Giacomo	M176
015	 248 	015248	Villa Cortese	L928
015	 249 	015249	Vanzaghello	L664
015	 250 	015250	Baranzate	A618
016	 001 	016001	Adrara San Martino	A057
016	 002 	016002	Adrara San Rocco	A058
016	 003 	016003	Albano SantAlessandro	A129
016	 004 	016004	Albino	A163
016	 005 	016005	AlmÔøΩ	A214
016	 006 	016006	Almenno San Bartolomeo	A216
016	 007 	016007	Almenno San Salvatore	A217
016	 008 	016008	Alzano Lombardo	A246
016	 009 	016009	Ambivere	A259
016	 010 	016010	Antegnate	A304
016	 011 	016011	Arcene	A365
016	 012 	016012	Ardesio	A383
016	 013 	016013	Arzago dAdda	A440
016	 014 	016014	Averara	A511
016	 015 	016015	Aviatico	A517
016	 016 	016016	Azzano San Paolo	A528
016	 017 	016017	Azzone	A533
016	 018 	016018	Bagnatica	A557
016	 019 	016019	Barbata	A631
016	 020 	016020	Bariano	A664
016	 021 	016021	Barzana	A684
016	 022 	016022	Bedulita	A732
016	 023 	016023	Berbenno	A786
016	 024 	016024	Bergamo	A794
016	 025 	016025	Berzo San Fermo	A815
016	 026 	016026	Bianzano	A846
016	 027 	016027	Blello	A903
016	 028 	016028	Bolgare	A937
016	 029 	016029	Boltiere	A950
016	 030 	016030	Bonate Sopra	A963
016	 031 	016031	Bonate Sotto	A962
016	 032 	016032	Borgo di Terzo	B010
016	 033 	016033	Bossico	B083
016	 034 	016034	Bottanuco	B088
016	 035 	016035	Bracca	B112
016	 036 	016036	Branzi	B123
016	 037 	016037	Brembate	B137
016	 038 	016038	Brembate di Sopra	B138
016	 039 	016039	Brembilla	B140
016	 040 	016040	Brignano Gera dAdda	B178
016	 041 	016041	Brumano	B217
016	 042 	016042	Brusaporto	B223
016	 043 	016043	Calcinate	B393
016	 044 	016044	Calcio	B395
016	 046 	016046	Calusco dAdda	B434
016	 047 	016047	Calvenzano	B442
016	 048 	016048	Camerata Cornello	B471
016	 049 	016049	Canonica dAdda	B618
016	 050 	016050	Capizzone	B661
016	 051 	016051	Capriate San Gervasio	B703
016	 052 	016052	Caprino Bergamasco	B710
016	 053 	016053	Caravaggio	B731
016	 055 	016055	Carobbio degli Angeli	B801
016	 056 	016056	Carona	B803
016	 057 	016057	Carvico	B854
016	 058 	016058	Casazza	B947
016	 059 	016059	Casirate dAdda	B971
016	 060 	016060	Casnigo	B978
016	 061 	016061	Cassiglio	C007
016	 062 	016062	Castelli Calepio	C079
016	 063 	016063	Castel Rozzone	C255
016	 064 	016064	Castione della Presolana	C324
016	 065 	016065	Castro	C337
016	 066 	016066	Cavernago	C396
016	 067 	016067	Cazzano SantAndrea	C410
016	 068 	016068	Cenate Sopra	C456
016	 069 	016069	Cenate Sotto	C457
016	 070 	016070	Cene	C459
016	 071 	016071	Cerete	C506
016	 072 	016072	Chignolo dIsola	C635
016	 073 	016073	Chiuduno	C649
016	 074 	016074	Cisano Bergamasco	C728
016	 075 	016075	Ciserano	C730
016	 076 	016076	Cividate al Piano	C759
016	 077 	016077	Clusone	C800
016	 078 	016078	Colere	C835
016	 079 	016079	Cologno al Serio	C894
016	 080 	016080	Colzate	C910
016	 081 	016081	Comun Nuovo	C937
016	 082 	016082	Corna Imagna	D015
016	 083 	016083	Cortenuova	D066
016	 084 	016084	Costa di Mezzate	D110
016	 085 	016085	Costa Valle Imagna	D103
016	 086 	016086	Costa Volpino	D117
016	 087 	016087	Covo	D126
016	 088 	016088	Credaro	D139
016	 089 	016089	Curno	D221
016	 090 	016090	Cusio	D233
016	 091 	016091	Dalmine	D245
016	 092 	016092	Dossena	D352
016	 093 	016093	Endine Gaiano	D406
016	 094 	016094	Entratico	D411
016	 096 	016096	Fara Gera dAdda	D490
016	 097 	016097	Fara Olivana con Sola	D491
016	 098 	016098	Filago	D588
016	 099 	016099	Fino del Monte	D604
016	 100 	016100	Fiorano al Serio	D606
016	 101 	016101	Fontanella	D672
016	 102 	016102	Fonteno	D684
016	 103 	016103	Foppolo	D688
016	 104 	016104	Foresto Sparso	D697
016	 105 	016105	Fornovo San Giovanni	D727
016	 106 	016106	Fuipiano Valle Imagna	D817
016	 107 	016107	Gandellino	D903
016	 108 	016108	Gandino	D905
016	 109 	016109	Gandosso	D906
016	 110 	016110	Gaverina Terme	D943
016	 111 	016111	Gazzaniga	D952
016	 112 	016112	Gerosa	D991
016	 113 	016113	Ghisalba	E006
016	 114 	016114	Gorlago	E100
016	 115 	016115	Gorle	E103
016	 116 	016116	Gorno	E106
016	 117 	016117	Grassobbio	E148
016	 118 	016118	Gromo	E189
016	 119 	016119	Grone	E192
016	 120 	016120	Grumello del Monte	E219
016	 121 	016121	Isola di Fondra	E353
016	 122 	016122	Isso	E370
016	 123 	016123	Lallio	E422
016	 124 	016124	Leffe	E509
016	 125 	016125	Lenna	E524
016	 126 	016126	Levate	E562
016	 127 	016127	Locatello	E640
016	 128 	016128	Lovere	E704
016	 129 	016129	Lurano	E751
016	 130 	016130	Luzzana	E770
016	 131 	016131	Madone	E794
016	 132 	016132	Mapello	E901
016	 133 	016133	Martinengo	E987
016	 134 	016134	Mezzoldo	F186
016	 135 	016135	Misano di Gera dAdda	F243
016	 136 	016136	Moio de Calvi	F276
016	 137 	016137	Monasterolo del Castello	F328
016	 139 	016139	Montello	F547
016	 140 	016140	Morengo	F720
016	 141 	016141	Mornico al Serio	F738
016	 142 	016142	Mozzanica	F786
016	 143 	016143	Mozzo	F791
016	 144 	016144	Nembro	F864
016	 145 	016145	Olmo al Brembo	G049
016	 146 	016146	Oltre il Colle	G050
016	 147 	016147	Oltressenda Alta	G054
016	 148 	016148	Oneta	G068
016	 149 	016149	Onore	G075
016	 150 	016150	Orio al Serio	G108
016	 151 	016151	Ornica	G118
016	 152 	016152	Osio Sopra	G159
016	 153 	016153	Osio Sotto	G160
016	 154 	016154	Pagazzano	G233
016	 155 	016155	Paladina	G249
016	 156 	016156	Palazzago	G259
016	 157 	016157	Palosco	G295
016	 158 	016158	Parre	G346
016	 159 	016159	Parzanica	G350
016	 160 	016160	Pedrengo	G412
016	 161 	016161	Peia	G418
016	 162 	016162	Pianico	G564
016	 163 	016163	Piario	G574
016	 164 	016164	Piazza Brembana	G579
016	 165 	016165	Piazzatorre	G583
016	 166 	016166	Piazzolo	G588
016	 167 	016167	Pognano	G774
016	 168 	016168	Ponte Nossa	F941
016	 169 	016169	Ponteranica	G853
016	 170 	016170	Ponte San Pietro	G856
016	 171 	016171	Pontida	G864
016	 172 	016172	Pontirolo Nuovo	G867
016	 173 	016173	Pradalunga	G968
016	 174 	016174	Predore	H020
016	 175 	016175	Premolo	H036
016	 176 	016176	Presezzo	H046
016	 177 	016177	Pumenengo	H091
016	 178 	016178	Ranica	H176
016	 179 	016179	Ranzanico	H177
016	 180 	016180	Riva di Solto	H331
016	 182 	016182	Rogno	H492
016	 183 	016183	Romano di Lombardia	H509
016	 184 	016184	Roncobello	H535
016	 185 	016185	Roncola	H544
016	 186 	016186	Rota dImagna	H584
016	 187 	016187	Rovetta	H615
016	 188 	016188	San Giovanni Bianco	H910
016	 189 	016189	San Paolo dArgon	B310
016	 190 	016190	San Pellegrino Terme	I079
016	 191 	016191	Santa Brigida	I168
016	 192 	016192	SantOmobono Terme	I349
016	 193 	016193	Sarnico	I437
016	 194 	016194	Scanzorosciate	I506
016	 195 	016195	Schilpario	I530
016	 196 	016196	Sedrina	I567
016	 197 	016197	Selvino	I597
016	 198 	016198	Seriate	I628
016	 199 	016199	Serina	I629
016	 200 	016200	Solto Collina	I812
016	 201 	016201	Songavazzo	I830
016	 202 	016202	Sorisole	I858
016	 203 	016203	Sotto il Monte Giovanni XXIII	I869
016	 204 	016204	Sovere	I873
016	 205 	016205	Spinone al Lago	I916
016	 206 	016206	Spirano	I919
016	 207 	016207	Stezzano	I951
016	 208 	016208	Strozza	I986
016	 209 	016209	Suisio	I997
016	 210 	016210	Taleggio	L037
016	 211 	016211	Tavernola Bergamasca	L073
016	 212 	016212	Telgate	L087
016	 213 	016213	Terno dIsola	L118
016	 214 	016214	Torre Boldone	L251
016	 216 	016216	Torre de Roveri	L265
016	 217 	016217	Torre Pallavicina	L276
016	 218 	016218	Trescore Balneario	L388
016	 219 	016219	Treviglio	L400
016	 220 	016220	Treviolo	L404
016	 221 	016221	Ubiale Clanezzo	C789
016	 222 	016222	Urgnano	L502
016	 223 	016223	Valbondione	L544
016	 224 	016224	Valbrembo	L545
016	 225 	016225	Valgoglio	L579
016	 226 	016226	Valleve	L623
016	 227 	016227	Valnegra	L642
016	 228 	016228	Valsecca	L649
016	 229 	016229	Valtorta	L655
016	 230 	016230	Vedeseta	L707
016	 232 	016232	Verdellino	L752
016	 233 	016233	Verdello	L753
016	 234 	016234	Vertova	L795
016	 235 	016235	Viadanica	L827
016	 236 	016236	Vigano San Martino	L865
016	 237 	016237	Vigolo	L894
016	 238 	016238	Villa dAdda	L929
016	 239 	016239	Villa dAlmÔøΩ	A215
016	 240 	016240	Villa di Serio	L936
016	 241 	016241	Villa dOgna	L938
016	 242 	016242	Villongo	M045
016	 243 	016243	Vilminore di Scalve	M050
016	 244 	016244	Zandobbio	M144
016	 245 	016245	Zanica	M147
016	 246 	016246	Zogno	M184
016	 247 	016247	Costa Serina	D111
016	 248 	016248	Algua	A193
016	 249 	016249	Cornalba	D016
016	 250 	016250	Medolago	F085
016	 251 	016251	Solza	I813
017	 001 	017001	Acquafredda	A034
017	 002 	017002	Adro	A060
017	 003 	017003	Agnosine	A082
017	 004 	017004	Alfianello	A188
017	 005 	017005	Anfo	A288
017	 006 	017006	Angolo Terme	A293
017	 007 	017007	Artogne	A451
017	 008 	017008	Azzano Mella	A529
017	 009 	017009	Bagnolo Mella	A569
017	 010 	017010	Bagolino	A578
017	 011 	017011	Barbariga	A630
017	 012 	017012	Barghe	A661
017	 013 	017013	Bassano Bresciano	A702
017	 014 	017014	Bedizzole	A729
017	 015 	017015	Berlingo	A799
017	 016 	017016	Berzo Demo	A816
017	 017 	017017	Berzo Inferiore	A817
017	 018 	017018	Bienno	A861
017	 019 	017019	Bione	A878
017	 020 	017020	Borgo San Giacomo	B035
017	 021 	017021	Borgosatollo	B040
017	 022 	017022	Borno	B054
017	 023 	017023	Botticino	B091
017	 024 	017024	Bovegno	B100
017	 025 	017025	Bovezzo	B102
017	 026 	017026	Brandico	B120
017	 027 	017027	Braone	B124
017	 028 	017028	Breno	B149
017	 029 	017029	Brescia	B157
017	 030 	017030	Brione	B184
017	 031 	017031	Caino	B365
017	 032 	017032	Calcinato	B394
017	 033 	017033	Calvagese della Riviera	B436
017	 034 	017034	Calvisano	B450
017	 035 	017035	Capo di Ponte	B664
017	 036 	017036	Capovalle	B676
017	 037 	017037	Capriano del Colle	B698
017	 038 	017038	Capriolo	B711
017	 039 	017039	Carpenedolo	B817
017	 040 	017040	Castegnato	C055
017	 041 	017041	Castelcovati	C072
017	 042 	017042	Castel Mella	C208
017	 043 	017043	Castenedolo	C293
017	 044 	017044	Casto	C330
017	 045 	017045	Castrezzato	C332
017	 046 	017046	Cazzago San Martino	C408
017	 047 	017047	Cedegolo	C417
017	 048 	017048	Cellatica	C439
017	 049 	017049	Cerveno	C549
017	 050 	017050	Ceto	C585
017	 051 	017051	Cevo	C591
017	 052 	017052	Chiari	C618
017	 053 	017053	Cigole	C685
017	 054 	017054	Cimbergo	C691
017	 055 	017055	Cividate Camuno	C760
017	 056 	017056	Coccaglio	C806
017	 057 	017057	Collebeato	C850
017	 058 	017058	Collio	C883
017	 059 	017059	Cologne	C893
017	 060 	017060	Comezzano-Cizzago	C925
017	 061 	017061	Concesio	C948
017	 062 	017062	Corte Franca	D058
017	 063 	017063	Corteno Golgi	D064
017	 064 	017064	Corzano	D082
017	 065 	017065	Darfo Boario Terme	D251
017	 066 	017066	Dello	D270
017	 067 	017067	Desenzano del Garda	D284
017	 068 	017068	Edolo	D391
017	 069 	017069	Erbusco	D421
017	 070 	017070	Esine	D434
017	 071 	017071	Fiesse	D576
017	 072 	017072	Flero	D634
017	 073 	017073	Gambara	D891
017	 074 	017074	Gardone Riviera	D917
017	 075 	017075	Gardone Val Trompia	D918
017	 076 	017076	Gargnano	D924
017	 077 	017077	Gavardo	D940
017	 078 	017078	Ghedi	D999
017	 079 	017079	Gianico	E010
017	 080 	017080	Gottolengo	E116
017	 081 	017081	Gussago	E271
017	 082 	017082	Idro	E280
017	 083 	017083	Incudine	E297
017	 084 	017084	Irma	E325
017	 085 	017085	Iseo	E333
017	 086 	017086	Isorella	E364
017	 087 	017087	Lavenone	E497
017	 088 	017088	Leno	E526
017	 089 	017089	Limone sul Garda	E596
017	 090 	017090	Lodrino	E652
017	 091 	017091	Lograto	E654
017	 092 	017092	Lonato del Garda	M312
017	 093 	017093	Longhena	E673
017	 094 	017094	Losine	E698
017	 095 	017095	Lozio	E706
017	 096 	017096	Lumezzane	E738
017	 097 	017097	Maclodio	E787
017	 098 	017098	Magasa	E800
017	 099 	017099	Mairano	E841
017	 100 	017100	Malegno	E851
017	 101 	017101	Malonno	E865
017	 102 	017102	Manerba del Garda	E883
017	 103 	017103	Manerbio	E884
017	 104 	017104	Marcheno	E928
017	 105 	017105	Marmentino	E961
017	 106 	017106	Marone	E967
017	 107 	017107	Mazzano	F063
017	 108 	017108	Milzano	F216
017	 109 	017109	Moniga del Garda	F373
017	 110 	017110	Monno	F375
017	 111 	017111	Monte Isola	F532
017	 112 	017112	Monticelli Brusati	F672
017	 113 	017113	Montichiari	F471
017	 114 	017114	Montirone	F680
017	 115 	017115	Mura	F806
017	 116 	017116	Muscoline	F820
017	 117 	017117	Nave	F851
017	 118 	017118	Niardo	F884
017	 119 	017119	Nuvolento	F989
017	 120 	017120	Nuvolera	F990
017	 121 	017121	Odolo	G001
017	 122 	017122	Offlaga	G006
017	 123 	017123	Ome	G061
017	 124 	017124	Ono San Pietro	G074
017	 125 	017125	Orzinuovi	G149
017	 126 	017126	Orzivecchi	G150
017	 127 	017127	Ospitaletto	G170
017	 128 	017128	Ossimo	G179
017	 129 	017129	Padenghe sul Garda	G213
017	 130 	017130	Paderno Franciacorta	G217
017	 131 	017131	Paisco Loveno	G247
017	 132 	017132	Paitone	G248
017	 133 	017133	Palazzolo sullOglio	G264
017	 134 	017134	Paratico	G327
017	 135 	017135	Paspardo	G354
017	 136 	017136	Passirano	G361
017	 137 	017137	Pavone del Mella	G391
017	 138 	017138	San Paolo	G407
017	 139 	017139	Pertica Alta	G474
017	 140 	017140	Pertica Bassa	G475
017	 141 	017141	Pezzaze	G529
017	 142 	017142	Pian Camuno	G546
017	 143 	017143	Pisogne	G710
017	 144 	017144	Polaveno	G779
017	 145 	017145	Polpenazze del Garda	G801
017	 146 	017146	Pompiano	G815
017	 147 	017147	Poncarale	G818
017	 148 	017148	Ponte di Legno	G844
017	 149 	017149	Pontevico	G859
017	 150 	017150	Pontoglio	G869
017	 151 	017151	Pozzolengo	G959
017	 152 	017152	Pralboino	G977
017	 153 	017153	Preseglie	H043
017	 154 	017154	Prestine	H050
017	 155 	017155	Prevalle	H055
017	 156 	017156	Provaglio dIseo	H078
017	 157 	017157	Provaglio Val Sabbia	H077
017	 158 	017158	Puegnago sul Garda	H086
017	 159 	017159	Quinzano dOglio	H140
017	 160 	017160	Remedello	H230
017	 161 	017161	Rezzato	H256
017	 162 	017162	Roccafranca	H410
017	 163 	017163	Rodengo Saiano	H477
017	 164 	017164	RoÔøΩ Volciano	H484
017	 165 	017165	Roncadelle	H525
017	 166 	017166	Rovato	H598
017	 167 	017167	Rudiano	H630
017	 168 	017168	Sabbio Chiese	H650
017	 169 	017169	Sale Marasino	H699
017	 170 	017170	SalÔøΩ	H717
017	 171 	017171	San Felice del Benaco	H838
017	 172 	017172	San Gervasio Bresciano	H865
017	 173 	017173	San Zeno Naviglio	I412
017	 174 	017174	Sarezzo	I433
017	 175 	017175	Saviore dellAdamello	I476
017	 176 	017176	Sellero	I588
017	 177 	017177	Seniga	I607
017	 178 	017178	Serle	I631
017	 179 	017179	Sirmione	I633
017	 180 	017180	Soiano del Lago	I782
017	 181 	017181	Sonico	I831
017	 182 	017182	Sulzano	L002
017	 183 	017183	Tavernole sul Mella	C698
017	 184 	017184	TemÔøΩ	L094
017	 185 	017185	Tignale	L169
017	 186 	017186	Torbole Casaglia	L210
017	 187 	017187	Toscolano-Maderno	L312
017	 188 	017188	Travagliato	L339
017	 189 	017189	Tremosine	L372
017	 190 	017190	Trenzano	L380
017	 191 	017191	Treviso Bresciano	L406
017	 192 	017192	Urago dOglio	L494
017	 193 	017193	Vallio Terme	L626
017	 194 	017194	Valvestino	L468
017	 195 	017195	Verolanuova	L777
017	 196 	017196	Verolavecchia	L778
017	 197 	017197	Vestone	L812
017	 198 	017198	Vezza dOglio	L816
017	 199 	017199	Villa Carcina	L919
017	 200 	017200	Villachiara	L923
017	 201 	017201	Villanuova sul Clisi	L995
017	 202 	017202	Vione	M065
017	 203 	017203	Visano	M070
017	 204 	017204	Vobarno	M104
017	 205 	017205	Zone	M188
017	 206 	017206	Piancogno	G549
018	 001 	018001	Alagna	A118
018	 002 	018002	Albaredo Arnaboldi	A134
018	 003 	018003	Albonese	A171
018	 004 	018004	Albuzzano	A175
018	 005 	018005	Arena Po	A387
018	 006 	018006	Badia Pavese	A538
018	 007 	018007	Bagnaria	A550
018	 008 	018008	Barbianello	A634
018	 009 	018009	BascapÔøΩ	A690
018	 010 	018010	Bastida de Dossi	A711
018	 011 	018011	Bastida Pancarana	A712
018	 012 	018012	Battuda	A718
018	 013 	018013	Belgioioso	A741
018	 014 	018014	Bereguardo	A792
018	 015 	018015	Borgarello	A989
018	 016 	018016	Borgo Priolo	B028
018	 017 	018017	Borgoratto Mormorolo	B030
018	 018 	018018	Borgo San Siro	B038
018	 019 	018019	Bornasco	B051
018	 020 	018020	Bosnasco	B082
018	 021 	018021	Brallo di Pregola	B117
018	 022 	018022	Breme	B142
018	 023 	018023	Bressana Bottarone	B159
018	 024 	018024	Broni	B201
018	 025 	018025	Calvignano	B447
018	 026 	018026	Campospinoso	B567
018	 027 	018027	Candia Lomellina	B587
018	 028 	018028	Canevino	B599
018	 029 	018029	Canneto Pavese	B613
018	 030 	018030	Carbonara al Ticino	B741
018	 031 	018031	Casanova Lonati	B929
018	 032 	018032	Casatisma	B945
018	 033 	018033	Casei Gerola	B954
018	 034 	018034	Casorate Primo	B988
018	 035 	018035	Cassolnovo	C038
018	 036 	018036	Castana	C050
018	 037 	018037	Casteggio	C053
018	 038 	018038	Castelletto di Branduzzo	C157
018	 039 	018039	Castello dAgogna	C184
018	 040 	018040	Castelnovetto	C213
018	 041 	018041	Cava Manara	C360
018	 042 	018042	Cecima	C414
018	 043 	018043	Ceranova	C484
018	 044 	018044	Ceretto Lomellina	C508
018	 045 	018045	Cergnago	C509
018	 046 	018046	Certosa di Pavia	C541
018	 047 	018047	Cervesina	C551
018	 048 	018048	Chignolo Po	C637
018	 049 	018049	Cigognola	C684
018	 050 	018050	Cilavegna	C686
018	 051 	018051	Codevilla	C813
018	 052 	018052	Confienza	C958
018	 053 	018053	Copiano	C979
018	 054 	018054	Corana	C982
018	 055 	018055	Cornale	D017
018	 056 	018056	Corteolona	D067
018	 057 	018057	Corvino San Quirico	D081
018	 058 	018058	Costa de Nobili	D109
018	 059 	018059	Cozzo	D127
018	 060 	018060	Cura Carpignano	B824
018	 061 	018061	Dorno	D348
018	 062 	018062	Ferrera Erbognone	D552
018	 063 	018063	Filighera	D594
018	 064 	018064	Fortunago	D732
018	 065 	018065	Frascarolo	D771
018	 066 	018066	Galliavola	D873
018	 067 	018067	Gambarana	D892
018	 068 	018068	GambolÔøΩ	D901
018	 069 	018069	Garlasco	D925
018	 070 	018070	Genzone	D973
018	 071 	018071	Gerenzago	D980
018	 072 	018072	Giussago	E062
018	 073 	018073	Godiasco Salice Terme	E072
018	 074 	018074	Golferenzo	E081
018	 075 	018075	Gravellona Lomellina	E152
018	 076 	018076	Gropello Cairoli	E195
018	 077 	018077	Inverno e Monteleone	E310
018	 078 	018078	Landriano	E437
018	 079 	018079	Langosco	E439
018	 080 	018080	Lardirago	E454
018	 081 	018081	Linarolo	E600
018	 082 	018082	Lirio	E608
018	 083 	018083	Lomello	E662
018	 084 	018084	Lungavilla	B387
018	 085 	018085	Magherno	E804
018	 086 	018086	Marcignago	E934
018	 087 	018087	Marzano	E999
018	 088 	018088	Mede	F080
018	 089 	018089	Menconico	F122
018	 090 	018090	Mezzana Bigli	F170
018	 091 	018091	Mezzana Rabattone	F171
018	 092 	018092	Mezzanino	F175
018	 093 	018093	Miradolo Terme	F238
018	 094 	018094	Montalto Pavese	F417
018	 095 	018095	Montebello della Battaglia	F440
018	 096 	018096	Montecalvo Versiggia	F449
018	 097 	018097	Montescano	F638
018	 098 	018098	Montesegale	F644
018	 099 	018099	Monticelli Pavese	F670
018	 100 	018100	MontÔøΩ Beccaria	F701
018	 101 	018101	Mornico Losana	F739
018	 102 	018102	Mortara	F754
018	 103 	018103	Nicorvo	F891
018	 104 	018104	Olevano di Lomellina	G021
018	 105 	018105	Oliva Gessi	G032
018	 106 	018106	Ottobiano	G194
018	 107 	018107	Palestro	G275
018	 108 	018108	Pancarana	G304
018	 109 	018109	Parona	G342
018	 110 	018110	Pavia	G388
018	 111 	018111	Pietra de Giorgi	G612
018	 112 	018112	Pieve Albignola	G635
018	 113 	018113	Pieve del Cairo	G639
018	 114 	018114	Pieve Porto Morone	G650
018	 115 	018115	Pinarolo Po	G671
018	 116 	018116	Pizzale	G720
018	 117 	018117	Ponte Nizza	G851
018	 118 	018118	Portalbera	G895
018	 119 	018119	Rea	H204
018	 120 	018120	Redavalle	H216
018	 121 	018121	Retorbido	H246
018	 122 	018122	Rivanazzano Terme	H336
018	 123 	018123	Robbio	H369
018	 124 	018124	Robecco Pavese	H375
018	 125 	018125	Rocca de Giorgi	H396
018	 126 	018126	Rocca Susella	H450
018	 127 	018127	Rognano	H491
018	 128 	018128	Romagnese	H505
018	 129 	018129	Roncaro	H527
018	 130 	018130	Rosasco	H559
018	 131 	018131	Rovescala	H614
018	 132 	018132	Ruino	H637
018	 133 	018133	San Cipriano Po	H799
018	 134 	018134	San Damiano al Colle	H814
018	 135 	018135	San Genesio ed Uniti	H859
018	 136 	018136	San Giorgio di Lomellina	H885
018	 137 	018137	San Martino Siccomario	I014
018	 138 	018138	Sannazzaro de Burgondi	I048
018	 139 	018139	Santa Cristina e Bissone	I175
018	 140 	018140	Santa Giuletta	I203
018	 141 	018141	SantAlessio con Vialone	I213
018	 142 	018142	Santa Margherita di Staffora	I230
018	 143 	018143	Santa Maria della Versa	I237
018	 144 	018144	SantAngelo Lomellina	I276
018	 145 	018145	San Zenone al Po	I416
018	 146 	018146	Sartirana Lomellina	I447
018	 147 	018147	Scaldasole	I487
018	 148 	018148	Semiana	I599
018	 149 	018149	Silvano Pietra	I739
018	 150 	018150	Siziano	E265
018	 151 	018151	Sommo	I825
018	 152 	018152	Spessa	I894
018	 153 	018153	Stradella	I968
018	 154 	018154	Suardi	B014
018	 155 	018155	Torrazza Coste	L237
018	 156 	018156	Torre Beretti e Castellaro	L250
018	 157 	018157	Torre dArese	L256
018	 158 	018158	Torre de Negri	L262
018	 159 	018159	Torre dIsola	L269
018	 160 	018160	Torrevecchia Pia	L285
018	 161 	018161	Torricella Verzate	L292
018	 162 	018162	TravacÔøΩ Siccomario	I236
018	 163 	018163	Trivolzio	L440
018	 164 	018164	Tromello	L449
018	 165 	018165	Trovo	L453
018	 166 	018166	Val di Nizza	L562
018	 167 	018167	Valeggio	L568
018	 168 	018168	Valle Lomellina	L593
018	 169 	018169	Valle Salimbene	L617
018	 170 	018170	Valverde	L659
018	 171 	018171	Varzi	L690
018	 172 	018172	Velezzo Lomellina	L716
018	 173 	018173	Vellezzo Bellini	L720
018	 174 	018174	Verretto	L784
018	 175 	018175	Verrua Po	L788
018	 176 	018176	Vidigulfo	L854
018	 177 	018177	Vigevano	L872
018	 178 	018178	Villa Biscossi	L917
018	 179 	018179	Villanova dArdenghi	L983
018	 180 	018180	Villanterio	L994
018	 181 	018181	Vistarino	M079
018	 182 	018182	Voghera	M109
018	 183 	018183	Volpara	M119
018	 184 	018184	Zavattarello	M150
018	 185 	018185	Zeccone	M152
018	 186 	018186	Zeme	M161
018	 187 	018187	Zenevredo	M162
018	 188 	018188	Zerbo	M166
018	 189 	018189	ZerbolÔøΩ	M167
018	 190 	018190	Zinasco	M180
019	 001 	019001	Acquanegra Cremonese	A039
019	 002 	019002	Agnadello	A076
019	 003 	019003	Annicco	A299
019	 004 	019004	Azzanello	A526
019	 005 	019005	Bagnolo Cremasco	A570
019	 006 	019006	Bonemerse	A972
019	 007 	019007	Bordolano	A986
019	 008 	019008	Ca dAndrea	B320
019	 009 	019009	Calvatone	B439
019	 010 	019010	Camisano	B484
019	 011 	019011	Campagnola Cremasca	B498
019	 012 	019012	Capergnanica	B650
019	 013 	019013	Cappella Cantone	B679
019	 014 	019014	Cappella de Picenardi	B680
019	 015 	019015	Capralba	B686
019	 016 	019016	Casalbuttano ed Uniti	B869
019	 017 	019017	Casale Cremasco-Vidolasco	B881
019	 018 	019018	Casaletto Ceredano	B889
019	 019 	019019	Casaletto di Sopra	B890
019	 020 	019020	Casaletto Vaprio	B891
019	 021 	019021	Casalmaggiore	B898
019	 022 	019022	Casalmorano	B900
019	 023 	019023	Casteldidone	C089
019	 024 	019024	Castel Gabbiano	C115
019	 025 	019025	Castelleone	C153
019	 026 	019026	Castelverde	B129
019	 027 	019027	Castelvisconti	C290
019	 028 	019028	Cella Dati	C435
019	 029 	019029	Chieve	C634
019	 030 	019030	Cicognolo	C678
019	 031 	019031	Cingia de Botti	C703
019	 032 	019032	Corte de Cortesi con Cignone	D056
019	 033 	019033	Corte de Frati	D057
019	 034 	019034	Credera Rubbiano	D141
019	 035 	019035	Crema	D142
019	 036 	019036	Cremona	D150
019	 037 	019037	Cremosano	D151
019	 038 	019038	Crotta dAdda	D186
019	 039 	019039	Cumignano sul Naviglio	D203
019	 040 	019040	Derovere	D278
019	 041 	019041	Dovera	D358
019	 042 	019042	Drizzona	D370
019	 043 	019043	Fiesco	D574
019	 044 	019044	Formigara	D710
019	 045 	019045	Gabbioneta-Binanuova	D834
019	 046 	019046	Gadesco-Pieve Delmona	D841
019	 047 	019047	Genivolta	D966
019	 048 	019048	Gerre de Caprioli	D993
019	 049 	019049	Gombito	E082
019	 050 	019050	Grontardo	E193
019	 051 	019051	Grumello Cremonese ed Uniti	E217
019	 052 	019052	Gussola	E272
019	 053 	019053	Isola Dovarese	E356
019	 054 	019054	Izano	E380
019	 055 	019055	Madignano	E793
019	 056 	019056	Malagnino	E843
019	 057 	019057	Martignana di Po	E983
019	 058 	019058	Monte Cremasco	F434
019	 059 	019059	Montodine	F681
019	 060 	019060	Moscazzano	F761
019	 061 	019061	Motta Baluffi	F771
019	 062 	019062	Offanengo	G004
019	 063 	019063	Olmeneta	G047
019	 064 	019064	Ostiano	G185
019	 065 	019065	Paderno Ponchielli	G222
019	 066 	019066	Palazzo Pignano	G260
019	 067 	019067	Pandino	G306
019	 068 	019068	Persico Dosimo	G469
019	 069 	019069	Pescarolo ed Uniti	G483
019	 070 	019070	Pessina Cremonese	G504
019	 071 	019071	Piadena	G536
019	 072 	019072	Pianengo	G558
019	 073 	019073	Pieranica	G603
019	 074 	019074	Pieve dOlmi	G647
019	 075 	019075	Pieve San Giacomo	G651
019	 076 	019076	Pizzighettone	G721
019	 077 	019077	Pozzaglio ed Uniti	B914
019	 078 	019078	Quintano	H130
019	 079 	019079	Ricengo	H276
019	 080 	019080	Ripalta Arpina	H314
019	 081 	019081	Ripalta Cremasca	H315
019	 082 	019082	Ripalta Guerina	H316
019	 083 	019083	Rivarolo del Re ed Uniti	H341
019	 084 	019084	Rivolta dAdda	H357
019	 085 	019085	Robecco dOglio	H372
019	 086 	019086	Romanengo	H508
019	 087 	019087	Salvirola	H731
019	 088 	019088	San Bassano	H767
019	 089 	019089	San Daniele Po	H815
019	 090 	019090	San Giovanni in Croce	H918
019	 091 	019091	San Martino del Lago	I007
019	 092 	019092	Scandolara Ravara	I497
019	 093 	019093	Scandolara Ripa dOglio	I498
019	 094 	019094	Sergnano	I627
019	 095 	019095	Sesto ed Uniti	I683
019	 096 	019096	Solarolo Rainerio	I790
019	 097 	019097	Soncino	I827
019	 098 	019098	Soresina	I849
019	 099 	019099	Sospiro	I865
019	 100 	019100	Spinadesco	I906
019	 101 	019101	Spineda	I909
019	 102 	019102	Spino dAdda	I914
019	 103 	019103	Stagno Lombardo	I935
019	 104 	019104	Ticengo	L164
019	 105 	019105	Torlino Vimercati	L221
019	 106 	019106	Tornata	L225
019	 107 	019107	Torre de Picenardi	L258
019	 108 	019108	Torricella del Pizzo	L296
019	 109 	019109	Trescore Cremasco	L389
019	 110 	019110	Trigolo	L426
019	 111 	019111	Vaiano Cremasco	L535
019	 112 	019112	Vailate	L539
019	 113 	019113	Vescovato	L806
019	 114 	019114	Volongo	M116
019	 115 	019115	Voltido	M127
020	 001 	020001	Acquanegra sul Chiese	A038
020	 002 	020002	Asola	A470
020	 003 	020003	Bagnolo San Vito	A575
020	 004 	020004	Bigarello	A866
020	 005 	020005	Borgoforte	B011
020	 006 	020006	Borgofranco sul Po	B013
020	 007 	020007	Bozzolo	B110
020	 008 	020008	Canneto sullOglio	B612
020	 009 	020009	Carbonara di Po	B739
020	 010 	020010	Casalmoro	B901
020	 011 	020011	Casaloldo	B907
020	 012 	020012	Casalromano	B911
020	 013 	020013	Castelbelforte	C059
020	 014 	020014	Castel dArio	C076
020	 015 	020015	Castel Goffredo	C118
020	 016 	020016	Castellucchio	C195
020	 017 	020017	Castiglione delle Stiviere	C312
020	 018 	020018	Cavriana	C406
020	 019 	020019	Ceresara	C502
020	 020 	020020	Commessaggio	C930
020	 021 	020021	Curtatone	D227
020	 022 	020022	Dosolo	D351
020	 023 	020023	Felonica	D529
020	 024 	020024	Gazoldo degli Ippoliti	D949
020	 025 	020025	Gazzuolo	D959
020	 026 	020026	Goito	E078
020	 027 	020027	Gonzaga	E089
020	 028 	020028	Guidizzolo	E261
020	 029 	020029	Magnacavallo	E818
020	 030 	020030	Mantova	E897
020	 031 	020031	Marcaria	E922
020	 032 	020032	Mariana Mantovana	E949
020	 033 	020033	Marmirolo	E962
020	 034 	020034	Medole	F086
020	 035 	020035	Moglia	F267
020	 036 	020036	Monzambano	F705
020	 037 	020037	Motteggiana	B012
020	 038 	020038	Ostiglia	G186
020	 039 	020039	Pegognaga	G417
020	 040 	020040	Pieve di Coriano	G633
020	 041 	020041	Piubega	G717
020	 042 	020042	Poggio Rusco	G753
020	 043 	020043	Pomponesco	G816
020	 044 	020044	Ponti sul Mincio	G862
020	 045 	020045	Porto Mantovano	G917
020	 046 	020046	Quingentole	H129
020	 047 	020047	Quistello	H143
020	 048 	020048	Redondesco	H218
020	 049 	020049	Revere	H248
020	 050 	020050	Rivarolo Mantovano	H342
020	 051 	020051	Rodigo	H481
020	 052 	020052	Roncoferraro	H541
020	 053 	020053	Roverbella	H604
020	 054 	020054	Sabbioneta	H652
020	 055 	020055	San Benedetto Po	H771
020	 056 	020056	San Giacomo delle Segnate	H870
020	 057 	020057	San Giorgio di Mantova	H883
020	 058 	020058	San Giovanni del Dosso	H912
020	 059 	020059	San Martino dallArgine	I005
020	 060 	020060	Schivenoglia	I532
020	 061 	020061	Sermide	I632
020	 062 	020062	Serravalle a Po	I662
020	 063 	020063	Solferino	I801
020	 064 	020064	Sustinente	L015
020	 065 	020065	Suzzara	L020
020	 066 	020066	Viadana	L826
020	 067 	020067	Villa Poma	F804
020	 068 	020068	Villimpenta	M044
020	 069 	020069	Virgilio	H123
020	 070 	020070	Volta Mantovana	M125
021	 001 	021001	Aldino	A179
021	 002 	021002	Andriano	A286
021	 003 	021003	Anterivo	A306
021	 004 	021004	Appiano sulla strada del vino	A332
021	 005 	021005	Avelengo	A507
021	 006 	021006	Badia	A537
021	 007 	021007	Barbiano	A635
021	 008 	021008	Bolzano	A952
021	 009 	021009	Braies	B116
021	 010 	021010	Brennero	B145
021	 011 	021011	Bressanone	B160
021	 012 	021012	Bronzolo	B203
021	 013 	021013	Brunico	B220
021	 014 	021014	Caines	B364
021	 015 	021015	Caldaro sulla strada del vino	B397
021	 016 	021016	Campo di Trens	B529
021	 017 	021017	Campo Tures	B570
021	 018 	021018	Castelbello-Ciardes	C062
021	 019 	021019	Castelrotto	C254
021	 020 	021020	Cermes	A022
021	 021 	021021	Chienes	C625
021	 022 	021022	Chiusa	C652
021	 023 	021023	Cornedo allIsarco	B799
021	 024 	021024	Cortaccia sulla strada del vino	D048
021	 025 	021025	Cortina sulla strada del vino	D075
021	 026 	021026	Corvara in Badia	D079
021	 027 	021027	Curon Venosta	D222
021	 028 	021028	Dobbiaco	D311
021	 029 	021029	Egna	D392
021	 030 	021030	Falzes	D484
021	 031 	021031	FiÔøΩ allo Sciliar	D571
021	 032 	021032	Fortezza	D731
021	 033 	021033	Funes	D821
021	 034 	021034	Gais	D860
021	 035 	021035	Gargazzone	D923
021	 036 	021036	Glorenza	E069
021	 037 	021037	Laces	E398
021	 038 	021038	Lagundo	E412
021	 039 	021039	Laion	E420
021	 040 	021040	Laives	E421
021	 041 	021041	Lana	E434
021	 042 	021042	Lasa	E457
021	 043 	021043	Lauregno	E481
021	 044 	021044	Luson	E764
021	 045 	021045	MagrÔøΩ sulla strada del vino	E829
021	 046 	021046	Malles Venosta	E862
021	 047 	021047	Marebbe	E938
021	 048 	021048	Marlengo	E959
021	 049 	021049	Martello	E981
021	 050 	021050	Meltina	F118
021	 051 	021051	Merano	F132
021	 052 	021052	Monguelfo-Tesido	F371
021	 053 	021053	Montagna	F392
021	 054 	021054	Moso in Passiria	F766
021	 055 	021055	Nalles	F836
021	 056 	021056	Naturno	F849
021	 057 	021057	Naz-Sciaves	F856
021	 058 	021058	Nova Levante	F949
021	 059 	021059	Nova Ponente	F950
021	 060 	021060	Ora	G083
021	 061 	021061	Ortisei	G140
021	 062 	021062	Parcines	G328
021	 063 	021063	Perca	G443
021	 064 	021064	Plaus	G299
021	 065 	021065	Ponte Gardena	G830
021	 066 	021066	Postal	G936
021	 067 	021067	Prato allo Stelvio	H004
021	 068 	021068	Predoi	H019
021	 069 	021069	Proves	H081
021	 070 	021070	Racines	H152
021	 071 	021071	Rasun-Anterselva	H189
021	 072 	021072	Renon	H236
021	 073 	021073	Rifiano	H284
021	 074 	021074	Rio di Pusteria	H299
021	 075 	021075	Rodengo	H475
021	 076 	021076	Salorno	H719
021	 077 	021077	San Candido	H786
021	 079 	021079	San Genesio Atesino	H858
022	 097 	022097	Imer	E288
021	 080 	021080	San Leonardo in Passiria	H952
021	 081 	021081	San Lorenzo di Sebato	H956
021	 082 	021082	San Martino in Badia	H988
021	 083 	021083	San Martino in Passiria	H989
021	 084 	021084	San Pancrazio	I065
021	 085 	021085	Santa Cristina Valgardena	I173
021	 086 	021086	Sarentino	I431
021	 087 	021087	Scena	I519
021	 088 	021088	Selva dei Molini	I593
021	 089 	021089	Selva di Val Gardena	I591
021	 091 	021091	Senales	I604
021	 092 	021092	Sesto	I687
021	 093 	021093	Silandro	I729
021	 094 	021094	Sluderno	I771
021	 095 	021095	Stelvio	I948
021	 096 	021096	Terento	L106
021	 097 	021097	Terlano	L108
021	 098 	021098	Termeno sulla strada del vino	L111
021	 099 	021099	Tesimo	L149
021	 100 	021100	Tires	L176
021	 101 	021101	Tirolo	L178
021	 102 	021102	Trodena nel parco naturale	L444
021	 103 	021103	Tubre	L455
021	 104 	021104	Ultimo	L490
021	 105 	021105	Vadena	L527
021	 106 	021106	Valdaora	L552
021	 107 	021107	Val di Vizze	L564
021	 108 	021108	Valle Aurina	L595
021	 109 	021109	Valle di Casies	L601
021	 110 	021110	Vandoies	L660
021	 111 	021111	Varna	L687
021	 112 	021112	Verano	L745
021	 113 	021113	Villabassa	L915
021	 114 	021114	Villandro	L971
021	 115 	021115	Vipiteno	M067
021	 116 	021116	Velturno	L724
021	 117 	021117	La Valle	E491
021	 118 	021118	Senale-San Felice	I603
022	 001 	022001	Ala	A116
022	 002 	022002	Albiano	A158
022	 003 	022003	Aldeno	A178
022	 004 	022004	Amblar	A260
022	 005 	022005	Andalo	A274
022	 006 	022006	Arco	A372
022	 007 	022007	Avio	A520
022	 009 	022009	Baselga di PinÔøΩ	A694
022	 011 	022011	Bedollo	A730
022	 012 	022012	Bersone	A808
022	 013 	022013	Besenello	A821
022	 015 	022015	Bieno	A863
022	 017 	022017	Bleggio Superiore	A902
022	 018 	022018	Bocenago	A916
022	 019 	022019	Bolbeno	A933
022	 020 	022020	Bondo	A967
022	 021 	022021	Bondone	A968
022	 022 	022022	Borgo Valsugana	B006
022	 023 	022023	Bosentino	B078
022	 024 	022024	Breguzzo	B135
022	 025 	022025	Brentonico	B153
022	 026 	022026	Bresimo	B158
022	 027 	022027	Brez	B165
022	 028 	022028	Brione	B185
022	 029 	022029	Caderzone Terme	B335
022	 030 	022030	CagnÔøΩ	B360
022	 031 	022031	Calavino	B386
022	 032 	022032	Calceranica al Lago	B389
022	 033 	022033	Caldes	B400
022	 034 	022034	Caldonazzo	B404
022	 035 	022035	Calliano	B419
022	 036 	022036	Campitello di Fassa	B514
022	 037 	022037	Campodenno	B525
022	 038 	022038	Canal San Bovo	B577
022	 039 	022039	Canazei	B579
022	 040 	022040	Capriana	B697
022	 041 	022041	Carano	B723
022	 042 	022042	Carisolo	B783
022	 043 	022043	Carzano	B856
022	 045 	022045	Castel Condino	C183
022	 046 	022046	Castelfondo	C103
022	 047 	022047	Castello-Molina di Fiemme	C189
022	 048 	022048	Castello Tesino	C194
022	 049 	022049	Castelnuovo	C216
022	 050 	022050	Cavalese	C372
022	 051 	022051	Cavareno	C380
022	 052 	022052	Cavedago	C392
022	 053 	022053	Cavedine	C393
022	 054 	022054	Cavizzana	C400
022	 055 	022055	Cembra	C452
022	 056 	022056	Centa San NicolÔøΩ	C467
022	 057 	022057	Cimego	C694
022	 058 	022058	Cimone	C700
022	 059 	022059	Cinte Tesino	C712
022	 060 	022060	Cis	C727
022	 061 	022061	Civezzano	C756
022	 062 	022062	Cles	C794
022	 063 	022063	Cloz	C797
022	 064 	022064	Commezzadura	C931
022	 066 	022066	Condino	C953
022	 067 	022067	Coredo	C994
022	 068 	022068	Croviana	D188
022	 069 	022069	Cunevo	D206
022	 070 	022070	Daiano	D243
022	 071 	022071	Dambel	D246
022	 072 	022072	Daone	D248
022	 073 	022073	DarÔøΩ	D250
022	 074 	022074	Denno	D273
022	 075 	022075	Dimaro	D302
022	 076 	022076	Don	D336
022	 077 	022077	Dorsino	D349
022	 078 	022078	Drena	D365
022	 079 	022079	Dro	D371
022	 080 	022080	Faedo	D457
022	 081 	022081	Fai della Paganella	D468
022	 082 	022082	Faver	D516
022	 083 	022083	FiavÔøΩ	D565
022	 084 	022084	Fiera di Primiero	D572
022	 085 	022085	Fierozzo	D573
022	 086 	022086	Flavon	D631
022	 087 	022087	Folgaria	D651
022	 088 	022088	Fondo	D663
022	 089 	022089	Fornace	D714
022	 090 	022090	Frassilongo	D775
022	 091 	022091	Garniga Terme	D928
022	 092 	022092	Giovo	E048
022	 093 	022093	Giustino	E065
022	 094 	022094	Grauno	E150
022	 095 	022095	Grigno	E178
022	 096 	022096	Grumes	E222
022	 098 	022098	Isera	E334
022	 099 	022099	Ivano-Fracena	E378
022	 100 	022100	Lardaro	E452
022	 101 	022101	Lasino	E461
022	 102 	022102	Lavarone	E492
022	 103 	022103	Lavis	E500
022	 104 	022104	Levico Terme	E565
022	 105 	022105	Lisignago	E614
022	 106 	022106	Livo	E624
022	 108 	022108	Lona-Lases	E664
022	 109 	022109	Luserna	E757
022	 110 	022110	MalÔøΩ	E850
022	 111 	022111	Malosco	E866
022	 112 	022112	Massimeno	F045
022	 113 	022113	Mazzin	F068
022	 114 	022114	Mezzana	F168
022	 115 	022115	Mezzano	F176
022	 116 	022116	Mezzocorona	F183
022	 117 	022117	Mezzolombardo	F187
022	 118 	022118	Moena	F263
022	 120 	022120	Molveno	F307
022	 121 	022121	Monclassico	F341
022	 122 	022122	Montagne	F396
022	 123 	022123	Mori	F728
022	 124 	022124	Nago-Torbole	F835
022	 125 	022125	Nanno	F837
022	 126 	022126	Nave San Rocco	F853
022	 127 	022127	Nogaredo	F920
022	 128 	022128	Nomi	F929
022	 129 	022129	Novaledo	F947
022	 130 	022130	Ospedaletto	G168
022	 131 	022131	Ossana	G173
022	 132 	022132	Padergnone	G214
022	 133 	022133	PalÔøΩ del Fersina	G296
022	 134 	022134	PanchiÔøΩ	G305
022	 135 	022135	Ronzo-Chienis	M303
022	 136 	022136	Peio	G419
022	 137 	022137	Pellizzano	G428
022	 138 	022138	Pelugo	G429
022	 139 	022139	Pergine Valsugana	G452
022	 140 	022140	Pieve di Bono	G641
022	 142 	022142	Pieve Tesino	G656
022	 143 	022143	Pinzolo	G681
022	 144 	022144	Pomarolo	G808
022	 145 	022145	Pozza di Fassa	G950
022	 146 	022146	Praso	G989
022	 147 	022147	Predazzo	H018
022	 148 	022148	Preore	H039
022	 149 	022149	Prezzo	H057
022	 150 	022150	Rabbi	H146
022	 151 	022151	Ragoli	H162
022	 152 	022152	RevÔøΩ	H254
022	 153 	022153	Riva del Garda	H330
022	 154 	022154	Romallo	H506
022	 155 	022155	Romeno	H517
022	 156 	022156	Roncegno Terme	H528
022	 157 	022157	Ronchi Valsugana	H532
022	 158 	022158	Roncone	H545
022	 159 	022159	Ronzone	H552
022	 160 	022160	RoverÔøΩ della Luna	H607
022	 161 	022161	Rovereto	H612
022	 162 	022162	RuffrÔøΩ-Mendola	H634
022	 163 	022163	Rumo	H639
022	 164 	022164	Sagron Mis	H666
022	 165 	022165	Samone	H754
022	 166 	022166	San Lorenzo in Banale	H966
022	 167 	022167	San Michele allAdige	I042
022	 168 	022168	SantOrsola Terme	I354
022	 169 	022169	Sanzeno	I411
022	 170 	022170	Sarnonico	I439
022	 171 	022171	Scurelle	I554
022	 172 	022172	Segonzano	I576
022	 173 	022173	Sfruz	I714
022	 174 	022174	Siror	I760
022	 175 	022175	Smarano	I772
022	 176 	022176	Soraga	I839
022	 177 	022177	Sover	I871
022	 178 	022178	Spera	I889
022	 179 	022179	Spiazzo	I899
022	 180 	022180	Spormaggiore	I924
022	 181 	022181	Sporminore	I925
022	 182 	022182	Stenico	I949
022	 183 	022183	Storo	I964
022	 184 	022184	Strembo	I975
022	 185 	022185	Strigno	I979
022	 186 	022186	Taio	L033
022	 187 	022187	Tassullo	L060
022	 188 	022188	Telve	L089
022	 189 	022189	Telve di Sopra	L090
022	 190 	022190	Tenna	L096
022	 191 	022191	Tenno	L097
022	 192 	022192	Terlago	L107
022	 193 	022193	Terragnolo	L121
022	 194 	022194	Terres	L137
022	 195 	022195	Terzolas	L145
022	 196 	022196	Tesero	L147
022	 199 	022199	Tione di Trento	L174
022	 200 	022200	Ton	L200
022	 201 	022201	Tonadico	L201
022	 202 	022202	Torcegno	L211
022	 203 	022203	Trambileno	L322
022	 204 	022204	Transacqua	L329
022	 205 	022205	Trento	L378
022	 206 	022206	Tres	L385
022	 207 	022207	Tuenno	L457
022	 208 	022208	Valda	L550
022	 209 	022209	Valfloriana	L575
022	 210 	022210	Vallarsa	L588
022	 211 	022211	Varena	L678
022	 212 	022212	Vattaro	L697
022	 213 	022213	Vermiglio	L769
022	 214 	022214	VervÔøΩ	L800
022	 215 	022215	Vezzano	L821
022	 216 	022216	Vignola-Falesina	L886
022	 217 	022217	Vigo di Fassa	L893
022	 219 	022219	Vigolo Vattaro	L896
022	 220 	022220	Vigo Rendena	L903
022	 221 	022221	Villa Agnedo	L910
022	 222 	022222	Villa Lagarina	L957
022	 223 	022223	Villa Rendena	M006
022	 224 	022224	Volano	M113
022	 225 	022225	Zambana	M142
022	 226 	022226	Ziano di Fiemme	M173
022	 227 	022227	Zuclo	M198
022	 228 	022228	Comano Terme	M314
022	 229 	022229	Ledro	M313
023	 001 	023001	Affi	A061
023	 002 	023002	Albaredo dAdige	A137
023	 003 	023003	Angiari	A292
023	 004 	023004	Arcole	A374
023	 005 	023005	Badia Calavena	A540
023	 006 	023006	Bardolino	A650
023	 007 	023007	Belfiore	A737
023	 008 	023008	Bevilacqua	A837
023	 009 	023009	Bonavigo	A964
023	 010 	023010	Boschi SantAnna	B070
023	 011 	023011	Bosco Chiesanuova	B073
023	 012 	023012	Bovolone	B107
023	 013 	023013	Brentino Belluno	B152
023	 014 	023014	Brenzone	B154
023	 015 	023015	Bussolengo	B296
023	 016 	023016	Buttapietra	B304
023	 017 	023017	Caldiero	B402
023	 018 	023018	Caprino Veronese	B709
023	 019 	023019	Casaleone	B886
023	 020 	023020	Castagnaro	C041
023	 021 	023021	Castel dAzzano	C078
023	 022 	023022	Castelnuovo del Garda	C225
023	 023 	023023	Cavaion Veronese	C370
023	 024 	023024	Cazzano di Tramigna	C412
023	 025 	023025	Cerea	C498
023	 026 	023026	Cerro Veronese	C538
023	 027 	023027	Cologna Veneta	C890
023	 028 	023028	Colognola ai Colli	C897
023	 029 	023029	Concamarise	C943
023	 030 	023030	Costermano	D118
023	 031 	023031	DolcÔøΩ	D317
023	 032 	023032	ErbÔøΩ	D419
023	 033 	023033	Erbezzo	D420
023	 034 	023034	Ferrara di Monte Baldo	D549
023	 035 	023035	Fumane	D818
023	 036 	023036	Garda	D915
023	 037 	023037	Gazzo Veronese	D957
023	 038 	023038	Grezzana	E171
023	 039 	023039	Illasi	E284
023	 040 	023040	Isola della Scala	E349
023	 041 	023041	Isola Rizza	E358
023	 042 	023042	Lavagno	E489
023	 043 	023043	Lazise	E502
023	 044 	023044	Legnago	E512
023	 045 	023045	Malcesine	E848
023	 046 	023046	Marano di Valpolicella	E911
023	 047 	023047	Mezzane di Sotto	F172
023	 048 	023048	Minerbe	F218
023	 049 	023049	Montecchia di Crosara	F461
023	 050 	023050	Monteforte dAlpone	F508
023	 051 	023051	Mozzecane	F789
023	 052 	023052	Negrar	F861
023	 053 	023053	Nogara	F918
023	 054 	023054	Nogarole Rocca	F921
023	 055 	023055	Oppeano	G080
023	 056 	023056	PalÔøΩ	G297
023	 057 	023057	Pastrengo	G365
023	 058 	023058	Pescantina	G481
023	 059 	023059	Peschiera del Garda	G489
023	 060 	023060	Povegliano Veronese	G945
023	 061 	023061	Pressana	H048
023	 062 	023062	Rivoli Veronese	H356
023	 063 	023063	RoncÔøΩ	H522
023	 064 	023064	Ronco allAdige	H540
023	 065 	023065	Roverchiara	H606
023	 066 	023066	Roveredo di GuÔøΩ	H610
023	 067 	023067	RoverÔøΩ Veronese	H608
023	 068 	023068	Salizzole	H714
023	 069 	023069	San Bonifacio	H783
023	 070 	023070	San Giovanni Ilarione	H916
023	 071 	023071	San Giovanni Lupatoto	H924
023	 072 	023072	Sanguinetto	H944
023	 073 	023073	San Martino Buon Albergo	I003
023	 074 	023074	San Mauro di Saline	H712
023	 075 	023075	San Pietro di Morubio	I105
023	 076 	023076	San Pietro in Cariano	I109
023	 077 	023077	SantAmbrogio di Valpolicella	I259
023	 078 	023078	SantAnna dAlfaedo	I292
023	 079 	023079	San Zeno di Montagna	I414
023	 080 	023080	Selva di Progno	I594
023	 081 	023081	Soave	I775
023	 082 	023082	Sommacampagna	I821
023	 083 	023083	Sona	I826
023	 084 	023084	SorgÔøΩ	I850
023	 085 	023085	Terrazzo	L136
023	 086 	023086	Torri del Benaco	L287
023	 087 	023087	Tregnago	L364
023	 088 	023088	Trevenzuolo	L396
023	 089 	023089	Valeggio sul Mincio	L567
023	 090 	023090	Velo Veronese	L722
023	 091 	023091	Verona	L781
023	 092 	023092	Veronella	D193
023	 093 	023093	Vestenanova	L810
023	 094 	023094	Vigasio	L869
023	 095 	023095	Villa Bartolomea	L912
023	 096 	023096	Villafranca di Verona	L949
023	 097 	023097	Zevio	M172
023	 098 	023098	Zimella	M178
024	 001 	024001	Agugliaro	A093
024	 002 	024002	Albettone	A154
024	 003 	024003	Alonte	A220
024	 004 	024004	Altavilla Vicentina	A231
024	 005 	024005	Altissimo	A236
024	 006 	024006	Arcugnano	A377
024	 007 	024007	Arsiero	A444
024	 008 	024008	Arzignano	A459
024	 009 	024009	Asiago	A465
024	 010 	024010	Asigliano Veneto	A467
024	 011 	024011	Barbarano Vicentino	A627
024	 012 	024012	Bassano del Grappa	A703
024	 013 	024013	Bolzano Vicentino	A954
024	 014 	024014	Breganze	B132
024	 015 	024015	Brendola	B143
024	 016 	024016	Bressanvido	B161
024	 017 	024017	Brogliano	B196
024	 018 	024018	Caldogno	B403
024	 019 	024019	Caltrano	B433
024	 020 	024020	Calvene	B441
024	 021 	024021	Camisano Vicentino	B485
024	 022 	024022	Campiglia dei Berici	B511
024	 023 	024023	Campolongo sul Brenta	B547
024	 024 	024024	CarrÔøΩ	B835
024	 025 	024025	Cartigliano	B844
024	 026 	024026	Cassola	C037
024	 027 	024027	Castegnero	C056
024	 028 	024028	Castelgomberto	C119
024	 029 	024029	Chiampo	C605
024	 030 	024030	Chiuppano	C650
024	 031 	024031	Cismon del Grappa	C734
024	 032 	024032	Cogollo del Cengio	C824
024	 033 	024033	Conco	C949
024	 034 	024034	Cornedo Vicentino	D020
024	 035 	024035	Costabissara	D107
024	 036 	024036	Creazzo	D136
024	 037 	024037	Crespadoro	D156
024	 038 	024038	Dueville	D379
024	 039 	024039	Enego	D407
024	 040 	024040	Fara Vicentino	D496
024	 041 	024041	Foza	D750
024	 042 	024042	Gallio	D882
024	 043 	024043	Gambellara	D897
024	 044 	024044	Gambugliano	D902
024	 045 	024045	Grancona	E138
024	 046 	024046	Grisignano di Zocco	E184
024	 047 	024047	Grumolo delle Abbadesse	E226
024	 048 	024048	Isola Vicentina	E354
024	 049 	024049	Laghi	E403
024	 050 	024050	Lastebasse	E465
024	 051 	024051	Longare	E671
024	 052 	024052	Lonigo	E682
024	 053 	024053	Lugo di Vicenza	E731
024	 054 	024054	Lusiana	E762
024	 055 	024055	Malo	E864
024	 056 	024056	Marano Vicentino	E912
024	 057 	024057	Marostica	E970
024	 058 	024058	Mason Vicentino	F019
024	 059 	024059	Molvena	F306
024	 060 	024060	Montebello Vicentino	F442
024	 061 	024061	Montecchio Maggiore	F464
024	 062 	024062	Montecchio Precalcino	F465
024	 063 	024063	Monte di Malo	F486
024	 064 	024064	Montegalda	F514
024	 065 	024065	Montegaldella	F515
024	 066 	024066	Monteviale	F662
024	 067 	024067	Monticello Conte Otto	F675
024	 068 	024068	Montorso Vicentino	F696
024	 069 	024069	Mossano	F768
024	 070 	024070	Mussolente	F829
024	 071 	024071	Nanto	F838
024	 072 	024072	Nogarole Vicentino	F922
024	 073 	024073	Nove	F957
024	 074 	024074	Noventa Vicentina	F964
024	 075 	024075	Orgiano	G095
024	 076 	024076	Pedemonte	G406
024	 077 	024077	Pianezze	G560
024	 078 	024078	Piovene Rocchette	G694
024	 079 	024079	Pojana Maggiore	G776
024	 080 	024080	Posina	G931
024	 081 	024081	Pove del Grappa	G943
024	 082 	024082	Pozzoleone	G957
024	 083 	024083	Quinto Vicentino	H134
024	 084 	024084	Recoaro Terme	H214
024	 085 	024085	Roana	H361
024	 086 	024086	Romano dEzzelino	H512
024	 087 	024087	RosÔøΩ	H556
024	 088 	024088	Rossano Veneto	H580
024	 089 	024089	Rotzo	H594
024	 090 	024090	Salcedo	F810
024	 091 	024091	Sandrigo	H829
024	 092 	024092	San Germano dei Berici	H863
024	 093 	024093	San Nazario	I047
024	 094 	024094	San Pietro Mussolino	I117
024	 095 	024095	Santorso	I353
024	 096 	024096	San Vito di Leguzzano	I401
024	 097 	024097	Sarcedo	I425
024	 098 	024098	Sarego	I430
024	 099 	024099	Schiavon	I527
024	 100 	024100	Schio	I531
024	 101 	024101	Solagna	I783
024	 102 	024102	Sossano	I867
024	 103 	024103	Sovizzo	I879
024	 104 	024104	Tezze sul Brenta	L156
024	 105 	024105	Thiene	L157
024	 106 	024106	Tonezza del Cimone	D717
024	 107 	024107	Torrebelvicino	L248
024	 108 	024108	Torri di Quartesolo	L297
024	 110 	024110	Trissino	L433
024	 111 	024111	Valdagno	L551
024	 112 	024112	Valdastico	L554
024	 113 	024113	Valli del Pasubio	L624
024	 114 	024114	Valstagna	L650
024	 115 	024115	Velo dAstico	L723
024	 116 	024116	Vicenza	L840
024	 117 	024117	Villaga	L952
024	 118 	024118	Villaverla	M032
024	 119 	024119	ZanÔøΩ	M145
024	 120 	024120	Zermeghedo	M170
024	 121 	024121	Zovencedo	M194
024	 122 	024122	Zugliano	M199
025	 001 	025001	Agordo	A083
025	 002 	025002	Alano di Piave	A121
025	 003 	025003	Alleghe	A206
025	 004 	025004	ArsiÔøΩ	A443
025	 005 	025005	Auronzo di Cadore	A501
025	 006 	025006	Belluno	A757
025	 007 	025007	Borca di Cadore	A982
025	 008 	025008	Calalzo di Cadore	B375
025	 009 	025009	Castellavazzo	C146
025	 010 	025010	Cencenighe Agordino	C458
025	 011 	025011	Cesiomaggiore	C577
025	 012 	025012	Chies dAlpago	C630
025	 013 	025013	Cibiana di Cadore	C672
025	 014 	025014	Colle Santa Lucia	C872
025	 015 	025015	Comelico Superiore	C920
025	 016 	025016	Cortina dAmpezzo	A266
025	 017 	025017	Danta di Cadore	D247
025	 018 	025018	Domegge di Cadore	D330
025	 019 	025019	Falcade	D470
025	 020 	025020	Farra dAlpago	D506
025	 021 	025021	Feltre	D530
025	 022 	025022	Fonzaso	D686
025	 023 	025023	Canale dAgordo	B574
025	 024 	025024	Forno di Zoldo	D726
025	 025 	025025	Gosaldo	E113
025	 026 	025026	Lamon	E429
025	 027 	025027	La Valle Agordina	E490
025	 028 	025028	Lentiai	C562
025	 029 	025029	Limana	E588
025	 030 	025030	Livinallongo del Col di Lana	E622
025	 031 	025031	Longarone	E672
025	 032 	025032	Lorenzago di Cadore	E687
025	 033 	025033	Lozzo di Cadore	E708
025	 034 	025034	Mel	F094
025	 035 	025035	Ospitale di Cadore	G169
025	 036 	025036	Pedavena	G404
025	 037 	025037	Perarolo di Cadore	G442
025	 038 	025038	Pieve dAlpago	G638
025	 039 	025039	Pieve di Cadore	G642
025	 040 	025040	Ponte nelle Alpi	B662
025	 041 	025041	Puos dAlpago	H092
025	 042 	025042	Quero	H124
025	 043 	025043	Rivamonte Agordino	H327
025	 044 	025044	Rocca Pietore	H379
025	 045 	025045	San Gregorio nelle Alpi	H938
025	 046 	025046	San NicolÔøΩ di Comelico	I063
025	 047 	025047	San Pietro di Cadore	I088
025	 048 	025048	Santa Giustina	I206
025	 049 	025049	San Tomaso Agordino	I347
025	 050 	025050	Santo Stefano di Cadore	C919
025	 051 	025051	San Vito di Cadore	I392
025	 052 	025052	Sappada	I421
025	 053 	025053	Sedico	I563
025	 054 	025054	Selva di Cadore	I592
025	 055 	025055	Seren del Grappa	I626
025	 056 	025056	Sospirolo	I866
025	 057 	025057	Soverzene	I876
025	 058 	025058	Sovramonte	I673
025	 059 	025059	Taibon Agordino	L030
025	 060 	025060	Tambre	L040
025	 061 	025061	Trichiana	L422
025	 062 	025062	Vallada Agordina	L584
025	 063 	025063	Valle di Cadore	L590
025	 064 	025064	Vas	L692
025	 065 	025065	Vigo di Cadore	L890
025	 066 	025066	Vodo Cadore	M108
025	 067 	025067	Voltago Agordino	M124
025	 068 	025068	Zoldo Alto	I345
025	 069 	025069	ZoppÔøΩ di Cadore	M189
026	 001 	026001	Altivole	A237
026	 002 	026002	Arcade	A360
026	 003 	026003	Asolo	A471
026	 004 	026004	Borso del Grappa	B061
026	 005 	026005	Breda di Piave	B128
026	 006 	026006	Caerano di San Marco	B349
026	 007 	026007	Cappella Maggiore	B678
026	 008 	026008	Carbonera	B744
026	 009 	026009	Casale sul Sile	B879
026	 010 	026010	Casier	B965
026	 011 	026011	Castelcucco	C073
026	 012 	026012	Castelfranco Veneto	C111
026	 013 	026013	Castello di Godego	C190
026	 014 	026014	Cavaso del Tomba	C384
026	 015 	026015	Cessalto	C580
026	 016 	026016	Chiarano	C614
026	 017 	026017	Cimadolmo	C689
026	 018 	026018	Cison di Valmarino	C735
026	 019 	026019	CodognÔøΩ	C815
026	 020 	026020	Colle Umberto	C848
026	 021 	026021	Conegliano	C957
026	 022 	026022	Cordignano	C992
026	 023 	026023	Cornuda	D030
026	 024 	026024	Crespano del Grappa	D157
026	 025 	026025	Crocetta del Montello	C670
026	 026 	026026	Farra di Soligo	D505
026	 027 	026027	Follina	D654
026	 028 	026028	Fontanelle	D674
026	 029 	026029	Fonte	D680
026	 030 	026030	Fregona	D794
026	 031 	026031	Gaiarine	D854
026	 032 	026032	Giavera del Montello	E021
026	 033 	026033	Godega di SantUrbano	E071
026	 034 	026034	Gorgo al Monticano	E092
026	 035 	026035	Istrana	E373
026	 036 	026036	Loria	E692
026	 037 	026037	MansuÔøΩ	E893
026	 038 	026038	Mareno di Piave	E940
026	 039 	026039	Maser	F009
026	 040 	026040	Maserada sul Piave	F012
026	 041 	026041	Meduna di Livenza	F088
026	 042 	026042	Miane	F190
026	 043 	026043	Mogliano Veneto	F269
026	 044 	026044	Monastier di Treviso	F332
026	 045 	026045	Monfumo	F360
026	 046 	026046	Montebelluna	F443
026	 047 	026047	Morgano	F725
026	 048 	026048	Moriago della Battaglia	F729
026	 049 	026049	Motta di Livenza	F770
026	 050 	026050	Nervesa della Battaglia	F872
026	 051 	026051	Oderzo	F999
026	 052 	026052	Ormelle	G115
026	 053 	026053	Orsago	G123
026	 054 	026054	Paderno del Grappa	G221
026	 055 	026055	Paese	G229
026	 056 	026056	Pederobba	G408
026	 057 	026057	Pieve di Soligo	G645
026	 058 	026058	Ponte di Piave	G846
026	 059 	026059	Ponzano Veneto	G875
026	 060 	026060	PortobuffolÔøΩ	G909
026	 061 	026061	Possagno	G933
026	 062 	026062	Povegliano	G944
026	 063 	026063	Preganziol	H022
026	 064 	026064	Quinto di Treviso	H131
026	 065 	026065	Refrontolo	H220
026	 066 	026066	Resana	H238
026	 067 	026067	Revine Lago	H253
026	 068 	026068	Riese Pio X	H280
026	 069 	026069	Roncade	H523
026	 070 	026070	Salgareda	H706
026	 071 	026071	San Biagio di Callalta	H781
026	 072 	026072	San Fior	H843
026	 073 	026073	San Pietro di Feletto	I103
026	 074 	026074	San Polo di Piave	I124
026	 075 	026075	Santa Lucia di Piave	I221
026	 076 	026076	San Vendemiano	I382
026	 077 	026077	San Zenone degli Ezzelini	I417
026	 078 	026078	Sarmede	I435
026	 079 	026079	Segusino	I578
026	 080 	026080	Sernaglia della Battaglia	I635
026	 081 	026081	Silea	F116
026	 082 	026082	Spresiano	I927
026	 083 	026083	Susegana	L014
026	 084 	026084	Tarzo	L058
026	 085 	026085	Trevignano	L402
026	 086 	026086	Treviso	L407
026	 087 	026087	Valdobbiadene	L565
026	 088 	026088	Vazzola	L700
026	 089 	026089	Vedelago	L706
026	 090 	026090	Vidor	L856
026	 091 	026091	Villorba	M048
026	 092 	026092	Vittorio Veneto	M089
026	 093 	026093	Volpago del Montello	M118
026	 094 	026094	Zenson di Piave	M163
026	 095 	026095	Zero Branco	M171
027	 001 	027001	Annone Veneto	A302
027	 002 	027002	Campagna Lupia	B493
027	 003 	027003	Campolongo Maggiore	B546
027	 004 	027004	Camponogara	B554
027	 005 	027005	Caorle	B642
027	 006 	027006	Cavarzere	C383
027	 007 	027007	Ceggia	C422
027	 008 	027008	Chioggia	C638
027	 009 	027009	Cinto Caomaggiore	C714
027	 010 	027010	Cona	C938
027	 011 	027011	Concordia Sagittaria	C950
027	 012 	027012	Dolo	D325
027	 013 	027013	Eraclea	D415
027	 014 	027014	Fiesso dArtico	D578
027	 015 	027015	Fossalta di Piave	D740
027	 016 	027016	Fossalta di Portogruaro	D741
027	 017 	027017	FossÔøΩ	D748
027	 018 	027018	Gruaro	E215
027	 019 	027019	Jesolo	C388
027	 020 	027020	Marcon	E936
027	 021 	027021	Martellago	E980
027	 022 	027022	Meolo	F130
027	 023 	027023	Mira	F229
027	 024 	027024	Mirano	F241
027	 025 	027025	Musile di Piave	F826
027	 026 	027026	Noale	F904
027	 027 	027027	Noventa di Piave	F963
027	 028 	027028	Pianiga	G565
027	 029 	027029	Portogruaro	G914
027	 030 	027030	Pramaggiore	G981
027	 031 	027031	Quarto dAltino	H117
027	 032 	027032	Salzano	H735
027	 033 	027033	San DonÔøΩ di Piave	H823
027	 034 	027034	San Michele al Tagliamento	I040
027	 035 	027035	Santa Maria di Sala	I242
027	 036 	027036	San Stino di Livenza	I373
027	 037 	027037	ScorzÔøΩ	I551
027	 038 	027038	Spinea	I908
027	 039 	027039	Stra	I965
027	 040 	027040	Teglio Veneto	L085
027	 041 	027041	Torre di Mosto	L267
027	 042 	027042	Venezia	L736
027	 043 	027043	Vigonovo	L899
027	 044 	027044	Cavallino-Treporti	M308
028	 001 	028001	Abano Terme	A001
028	 002 	028002	Agna	A075
028	 003 	028003	Albignasego	A161
028	 004 	028004	Anguillara Veneta	A296
028	 005 	028005	ArquÔøΩ Petrarca	A434
028	 006 	028006	Arre	A438
028	 007 	028007	Arzergrande	A458
028	 008 	028008	Bagnoli di Sopra	A568
028	 009 	028009	Baone	A613
028	 010 	028010	Barbona	A637
028	 011 	028011	Battaglia Terme	A714
028	 012 	028012	Boara Pisani	A906
028	 013 	028013	Borgoricco	B031
028	 014 	028014	Bovolenta	B106
028	 015 	028015	Brugine	B213
028	 016 	028016	Cadoneghe	B345
028	 017 	028017	Campodarsego	B524
028	 018 	028018	Campodoro	B531
028	 019 	028019	Camposampiero	B563
028	 020 	028020	Campo San Martino	B564
028	 021 	028021	Candiana	B589
028	 022 	028022	Carceri	B749
028	 023 	028023	Carmignano di Brenta	B795
028	 026 	028026	Cartura	B848
028	 027 	028027	Casale di Scodosia	B877
028	 028 	028028	Casalserugo	B912
028	 029 	028029	Castelbaldo	C057
028	 030 	028030	Cervarese Santa Croce	C544
028	 031 	028031	Cinto Euganeo	C713
028	 032 	028032	Cittadella	C743
028	 033 	028033	Codevigo	C812
028	 034 	028034	Conselve	C964
028	 035 	028035	Correzzola	D040
028	 036 	028036	Curtarolo	D226
028	 037 	028037	Este	D442
028	 038 	028038	Fontaniva	D679
028	 039 	028039	Galliera Veneta	D879
028	 040 	028040	Galzignano Terme	D889
028	 041 	028041	Gazzo	D956
028	 042 	028042	Grantorto	E145
028	 043 	028043	Granze	E146
028	 044 	028044	Legnaro	E515
028	 045 	028045	Limena	E592
028	 046 	028046	Loreggia	E684
028	 047 	028047	Lozzo Atestino	E709
028	 048 	028048	MaserÔøΩ di Padova	F011
028	 049 	028049	Masi	F013
028	 050 	028050	Massanzago	F033
028	 051 	028051	Megliadino San Fidenzio	F091
028	 052 	028052	Megliadino San Vitale	F092
028	 053 	028053	Merlara	F148
028	 054 	028054	Mestrino	F161
028	 055 	028055	Monselice	F382
028	 056 	028056	Montagnana	F394
028	 057 	028057	Montegrotto Terme	F529
028	 058 	028058	Noventa Padovana	F962
028	 059 	028059	Ospedaletto Euganeo	G167
028	 060 	028060	Padova	G224
028	 061 	028061	Pernumia	G461
028	 062 	028062	Piacenza dAdige	G534
028	 063 	028063	Piazzola sul Brenta	G587
028	 064 	028064	Piombino Dese	G688
028	 065 	028065	Piove di Sacco	G693
028	 066 	028066	Polverara	G802
028	 067 	028067	Ponso	G823
028	 068 	028068	Pontelongo	G850
028	 069 	028069	Ponte San NicolÔøΩ	G855
028	 070 	028070	Pozzonovo	G963
028	 071 	028071	Rovolon	H622
028	 072 	028072	Rubano	H625
028	 073 	028073	Saccolongo	H655
028	 074 	028074	Saletto	H705
028	 075 	028075	San Giorgio delle Pertiche	H893
028	 076 	028076	San Giorgio in Bosco	H897
028	 077 	028077	San Martino di Lupari	I008
028	 078 	028078	San Pietro in Gu	I107
028	 079 	028079	San Pietro Viminario	I120
028	 080 	028080	Santa Giustina in Colle	I207
028	 081 	028081	Santa Margherita dAdige	I226
028	 082 	028082	SantAngelo di Piove di Sacco	I275
028	 083 	028083	SantElena	I319
028	 084 	028084	SantUrbano	I375
028	 085 	028085	Saonara	I418
028	 086 	028086	Selvazzano Dentro	I595
028	 087 	028087	Solesino	I799
028	 088 	028088	Stanghella	I938
028	 089 	028089	Teolo	L100
028	 090 	028090	Terrassa Padovana	L132
028	 091 	028091	Tombolo	L199
028	 092 	028092	Torreglia	L270
028	 093 	028093	Trebaseleghe	L349
028	 094 	028094	Tribano	L414
028	 095 	028095	Urbana	L497
028	 096 	028096	Veggiano	L710
028	 097 	028097	Vescovana	L805
028	 098 	028098	Vighizzolo dEste	L878
028	 099 	028099	Vigodarzere	L892
028	 100 	028100	Vigonza	L900
028	 101 	028101	Villa del Conte	L934
028	 102 	028102	Villa Estense	L937
028	 103 	028103	Villafranca Padovana	L947
028	 104 	028104	Villanova di Camposampiero	L979
028	 105 	028105	Vo	M103
028	 106 	028106	Due Carrare	M300
029	 001 	029001	Adria	A059
029	 002 	029002	Ariano nel Polesine	A400
029	 003 	029003	ArquÔøΩ Polesine	A435
029	 004 	029004	Badia Polesine	A539
029	 005 	029005	Bagnolo di Po	A574
029	 006 	029006	Bergantino	A795
029	 007 	029007	Bosaro	B069
029	 008 	029008	Calto	B432
029	 009 	029009	Canaro	B578
029	 010 	029010	Canda	B582
029	 011 	029011	Castelguglielmo	C122
029	 012 	029012	Castelmassa	C207
029	 013 	029013	Castelnovo Bariano	C215
029	 014 	029014	Ceneselli	C461
029	 015 	029015	Ceregnano	C500
029	 017 	029017	Corbola	C987
029	 018 	029018	Costa di Rovigo	D105
029	 019 	029019	Crespino	D161
029	 021 	029021	Ficarolo	D568
029	 022 	029022	Fiesso Umbertiano	D577
029	 023 	029023	Frassinelle Polesine	D776
029	 024 	029024	Fratta Polesine	D788
029	 025 	029025	Gaiba	D855
029	 026 	029026	Gavello	D942
029	 027 	029027	Giacciano con Baruchella	E008
029	 028 	029028	Guarda Veneta	E240
029	 029 	029029	Lendinara	E522
029	 030 	029030	Loreo	E689
029	 031 	029031	Lusia	E761
029	 032 	029032	Melara	F095
029	 033 	029033	Occhiobello	F994
029	 034 	029034	Papozze	G323
029	 035 	029035	Pettorazza Grimani	G525
029	 036 	029036	Pincara	G673
029	 037 	029037	Polesella	G782
029	 038 	029038	Pontecchio Polesine	G836
029	 039 	029039	Porto Tolle	G923
029	 040 	029040	Rosolina	H573
029	 041 	029041	Rovigo	H620
029	 042 	029042	Salara	H689
029	 043 	029043	San Bellino	H768
029	 044 	029044	San Martino di Venezze	H996
029	 045 	029045	Stienta	I953
029	 046 	029046	Taglio di Po	L026
029	 047 	029047	Trecenta	L359
029	 048 	029048	Villadose	L939
029	 049 	029049	Villamarzana	L967
029	 050 	029050	Villanova del Ghebbo	L985
029	 051 	029051	Villanova Marchesana	L988
029	 052 	029052	Porto Viro	G926
030	 001 	030001	Aiello del Friuli	A103
030	 002 	030002	Amaro	A254
030	 003 	030003	Ampezzo	A267
030	 004 	030004	Aquileia	A346
030	 005 	030005	Arta Terme	A447
030	 006 	030006	Artegna	A448
030	 007 	030007	Attimis	A491
030	 008 	030008	Bagnaria Arsa	A553
030	 009 	030009	Basiliano	A700
030	 010 	030010	Bertiolo	A810
030	 011 	030011	Bicinicco	A855
030	 012 	030012	Bordano	A983
030	 013 	030013	Buja	B259
030	 014 	030014	Buttrio	B309
030	 015 	030015	Camino al Tagliamento	B483
030	 016 	030016	Campoformido	B536
030	 018 	030018	Carlino	B788
030	 019 	030019	Cassacco	B994
030	 020 	030020	Castions di Strada	C327
030	 021 	030021	Cavazzo Carnico	C389
030	 022 	030022	Cercivento	C494
030	 023 	030023	Cervignano del Friuli	C556
030	 024 	030024	Chiopris-Viscone	C641
030	 025 	030025	Chiusaforte	C656
030	 026 	030026	Cividale del Friuli	C758
030	 027 	030027	Codroipo	C817
030	 028 	030028	Colloredo di Monte Albano	C885
030	 029 	030029	Comeglians	C918
030	 030 	030030	Corno di Rosazzo	D027
030	 031 	030031	Coseano	D085
030	 032 	030032	Dignano	D300
030	 033 	030033	Dogna	D316
030	 034 	030034	Drenchia	D366
030	 035 	030035	Enemonzo	D408
030	 036 	030036	Faedis	D455
030	 037 	030037	Fagagna	D461
030	 038 	030038	Fiumicello	D627
030	 039 	030039	Flaibano	D630
030	 040 	030040	Forni Avoltri	D718
030	 041 	030041	Forni di Sopra	D719
030	 042 	030042	Forni di Sotto	D720
030	 043 	030043	Gemona del Friuli	D962
030	 044 	030044	Gonars	E083
030	 045 	030045	Grimacco	E179
030	 046 	030046	Latisana	E473
030	 047 	030047	Lauco	E476
030	 048 	030048	Lestizza	E553
030	 049 	030049	Lignano Sabbiadoro	E584
030	 050 	030050	Ligosullo	E586
030	 051 	030051	Lusevera	E760
030	 052 	030052	Magnano in Riviera	E820
030	 053 	030053	Majano	E833
030	 054 	030054	Malborghetto Valbruna	E847
030	 055 	030055	Manzano	E899
030	 056 	030056	Marano Lagunare	E910
030	 057 	030057	Martignacco	E982
030	 058 	030058	Mereto di Tomba	F144
030	 059 	030059	Moggio Udinese	F266
030	 060 	030060	Moimacco	F275
030	 061 	030061	Montenars	F574
030	 062 	030062	Mortegliano	F756
030	 063 	030063	Moruzzo	F760
030	 064 	030064	Muzzana del Turgnano	F832
030	 065 	030065	Nimis	F898
030	 066 	030066	Osoppo	G163
030	 067 	030067	Ovaro	G198
030	 068 	030068	Pagnacco	G238
030	 069 	030069	Palazzolo dello Stella	G268
030	 070 	030070	Palmanova	G284
030	 071 	030071	Paluzza	G300
030	 072 	030072	Pasian di Prato	G352
030	 073 	030073	Paularo	G381
030	 074 	030074	Pavia di Udine	G389
030	 075 	030075	Pocenia	G743
030	 076 	030076	Pontebba	G831
030	 077 	030077	Porpetto	G891
030	 078 	030078	Povoletto	G949
030	 079 	030079	Pozzuolo del Friuli	G966
030	 080 	030080	Pradamano	G969
030	 081 	030081	Prato Carnico	H002
030	 082 	030082	Precenicco	H014
030	 083 	030083	Premariacco	H029
030	 084 	030084	Preone	H038
030	 085 	030085	Prepotto	H040
030	 086 	030086	Pulfero	H089
030	 087 	030087	Ragogna	H161
030	 088 	030088	Ravascletto	H196
030	 089 	030089	Raveo	H200
030	 090 	030090	Reana del Rojale	H206
030	 091 	030091	Remanzacco	H229
030	 092 	030092	Resia	H242
030	 093 	030093	Resiutta	H244
030	 094 	030094	Rigolato	H289
030	 095 	030095	Rive dArcano	H347
030	 096 	030096	Rivignano	H352
030	 097 	030097	Ronchis	H533
030	 098 	030098	Ruda	H629
030	 099 	030099	San Daniele del Friuli	H816
030	 100 	030100	San Giorgio di Nogaro	H895
030	 101 	030101	San Giovanni al Natisone	H906
030	 102 	030102	San Leonardo	H951
030	 103 	030103	San Pietro al Natisone	I092
030	 104 	030104	Santa Maria la Longa	I248
030	 105 	030105	San Vito al Torre	I404
030	 106 	030106	San Vito di Fagagna	I405
030	 107 	030107	Sauris	I464
030	 108 	030108	Savogna	I478
030	 109 	030109	Sedegliano	I562
030	 110 	030110	Socchieve	I777
030	 111 	030111	Stregna	I974
030	 112 	030112	Sutrio	L018
030	 113 	030113	Taipana	G736
030	 114 	030114	Talmassons	L039
030	 116 	030116	Tarcento	L050
030	 117 	030117	Tarvisio	L057
030	 118 	030118	Tavagnacco	L065
030	 119 	030119	Teor	L101
030	 120 	030120	Terzo dAquileia	L144
030	 121 	030121	Tolmezzo	L195
030	 122 	030122	Torreano	L246
030	 123 	030123	Torviscosa	L309
030	 124 	030124	Trasaghis	L335
030	 125 	030125	Treppo Carnico	L381
030	 126 	030126	Treppo Grande	L382
030	 127 	030127	Tricesimo	L421
030	 128 	030128	Trivignano Udinese	L438
030	 129 	030129	Udine	L483
030	 130 	030130	Varmo	L686
030	 131 	030131	Venzone	L743
030	 132 	030132	Verzegnis	L801
030	 133 	030133	Villa Santina	L909
030	 134 	030134	Villa Vicentina	M034
030	 135 	030135	Visco	M073
030	 136 	030136	Zuglio	M200
030	 137 	030137	Forgaria nel Friuli	D700
030	 138 	030138	Campolongo Tapogliano	M311
031	 001 	031001	Capriva del Friuli	B712
031	 002 	031002	Cormons	D014
031	 003 	031003	DoberdÔøΩ del Lago	D312
031	 004 	031004	Dolegna del Collio	D321
031	 005 	031005	Farra dIsonzo	D504
031	 006 	031006	Fogliano Redipuglia	D645
031	 007 	031007	Gorizia	E098
031	 008 	031008	Gradisca dIsonzo	E124
031	 009 	031009	Grado	E125
031	 010 	031010	Mariano del Friuli	E952
031	 011 	031011	Medea	F081
031	 012 	031012	Monfalcone	F356
031	 013 	031013	Moraro	F710
031	 014 	031014	Mossa	F767
031	 015 	031015	Romans dIsonzo	H514
031	 016 	031016	Ronchi dei Legionari	H531
031	 017 	031017	Sagrado	H665
031	 018 	031018	San Canzian dIsonzo	H787
031	 019 	031019	San Floriano del Collio	H845
031	 020 	031020	San Lorenzo Isontino	H964
031	 021 	031021	San Pier dIsonzo	I082
031	 022 	031022	Savogna dIsonzo	I479
031	 023 	031023	Staranzano	I939
031	 024 	031024	Turriaco	L474
031	 025 	031025	Villesse	M043
032	 001 	032001	Duino-Aurisina	D383
032	 002 	032002	Monrupino	F378
032	 003 	032003	Muggia	F795
032	 004 	032004	San Dorligo della Valle-Dolina	D324
032	 005 	032005	Sgonico	I715
032	 006 	032006	Trieste	L424
033	 001 	033001	Agazzano	A067
033	 002 	033002	Alseno	A223
033	 003 	033003	Besenzone	A823
033	 004 	033004	Bettola	A831
033	 005 	033005	Bobbio	A909
033	 006 	033006	Borgonovo Val Tidone	B025
033	 007 	033007	Cadeo	B332
033	 008 	033008	Calendasco	B405
033	 009 	033009	Caminata	B479
033	 010 	033010	Caorso	B643
033	 011 	033011	Carpaneto Piacentino	B812
033	 012 	033012	CastellArquato	C145
033	 013 	033013	Castel San Giovanni	C261
033	 014 	033014	Castelvetro Piacentino	C288
033	 015 	033015	Cerignale	C513
033	 016 	033016	Coli	C838
033	 017 	033017	Corte Brugnatella	D054
033	 018 	033018	Cortemaggiore	D061
033	 019 	033019	Farini	D502
033	 020 	033020	Ferriere	D555
033	 021 	033021	Fiorenzuola dArda	D611
033	 022 	033022	Gazzola	D958
033	 023 	033023	Gossolengo	E114
033	 024 	033024	Gragnano Trebbiense	E132
033	 025 	033025	Gropparello	E196
033	 026 	033026	Lugagnano Val dArda	E726
033	 027 	033027	Monticelli dOngina	F671
033	 028 	033028	Morfasso	F724
033	 029 	033029	Nibbiano	F885
033	 030 	033030	Ottone	G195
033	 031 	033031	Pecorara	G399
033	 032 	033032	Piacenza	G535
033	 033 	033033	Pianello Val Tidone	G557
033	 034 	033034	Piozzano	G696
033	 035 	033035	Podenzano	G747
033	 036 	033036	Ponte dellOlio	G842
033	 037 	033037	Pontenure	G852
033	 038 	033038	Rivergaro	H350
033	 039 	033039	Rottofreno	H593
033	 040 	033040	San Giorgio Piacentino	H887
033	 041 	033041	San Pietro in Cerro	G788
033	 042 	033042	Sarmato	I434
033	 043 	033043	Travo	L348
033	 044 	033044	Vernasca	L772
033	 045 	033045	Vigolzone	L897
033	 046 	033046	Villanova sullArda	L980
033	 047 	033047	Zerba	M165
033	 048 	033048	Ziano Piacentino	L848
034	 001 	034001	Albareto	A138
034	 002 	034002	Bardi	A646
034	 003 	034003	Bedonia	A731
034	 004 	034004	Berceto	A788
034	 005 	034005	Bore	A987
034	 006 	034006	Borgo Val di Taro	B042
034	 007 	034007	Busseto	B293
034	 008 	034008	Calestano	B408
034	 009 	034009	Collecchio	C852
034	 010 	034010	Colorno	C904
034	 011 	034011	Compiano	C934
034	 012 	034012	Corniglio	D026
034	 013 	034013	Felino	D526
034	 014 	034014	Fidenza	B034
034	 015 	034015	Fontanellato	D673
034	 016 	034016	Fontevivo	D685
034	 017 	034017	Fornovo di Taro	D728
034	 018 	034018	Langhirano	E438
034	 019 	034019	Lesignano de Bagni	E547
034	 020 	034020	Medesano	F082
034	 021 	034021	Mezzani	F174
034	 022 	034022	Monchio delle Corti	F340
034	 023 	034023	Montechiarugolo	F473
034	 024 	034024	Neviano degli Arduini	F882
034	 025 	034025	Noceto	F914
034	 026 	034026	Palanzano	G255
034	 027 	034027	Parma	G337
034	 028 	034028	Pellegrino Parmense	G424
034	 029 	034029	Polesine Parmense	G783
034	 030 	034030	Roccabianca	H384
034	 031 	034031	Sala Baganza	H682
034	 032 	034032	Salsomaggiore Terme	H720
034	 033 	034033	San Secondo Parmense	I153
034	 034 	034034	Sissa	I763
034	 035 	034035	Solignano	I803
034	 036 	034036	Soragna	I840
034	 037 	034037	Sorbolo	I845
034	 038 	034038	Terenzo	E548
034	 039 	034039	Tizzano Val Parma	L183
034	 040 	034040	Tornolo	L229
034	 041 	034041	Torrile	L299
034	 042 	034042	Traversetolo	L346
034	 043 	034043	Trecasali	L354
034	 044 	034044	Valmozzola	L641
034	 045 	034045	Varano de Melegari	L672
034	 046 	034046	Varsi	L689
034	 048 	034048	Zibello	M174
035	 001 	035001	Albinea	A162
035	 002 	035002	Bagnolo in Piano	A573
035	 003 	035003	Baiso	A586
035	 004 	035004	Bibbiano	A850
035	 005 	035005	Boretto	A988
035	 006 	035006	Brescello	B156
035	 007 	035007	Busana	B283
035	 008 	035008	Cadelbosco di Sopra	B328
035	 009 	035009	Campagnola Emilia	B499
035	 010 	035010	Campegine	B502
035	 011 	035011	Carpineti	B825
035	 012 	035012	Casalgrande	B893
035	 013 	035013	Casina	B967
035	 014 	035014	Castellarano	C141
035	 015 	035015	Castelnovo di Sotto	C218
035	 016 	035016	Castelnovo ne Monti	C219
035	 017 	035017	Cavriago	C405
035	 018 	035018	Canossa	C669
035	 019 	035019	Collagna	C840
035	 020 	035020	Correggio	D037
035	 021 	035021	Fabbrico	D450
035	 022 	035022	Gattatico	D934
035	 023 	035023	Gualtieri	E232
035	 024 	035024	Guastalla	E253
035	 025 	035025	Ligonchio	E585
035	 026 	035026	Luzzara	E772
035	 027 	035027	Montecchio Emilia	F463
035	 028 	035028	Novellara	F960
035	 029 	035029	Poviglio	G947
035	 030 	035030	Quattro Castella	H122
035	 031 	035031	Ramiseto	G654
035	 032 	035032	Reggiolo	H225
035	 033 	035033	Reggio nellEmilia	H223
035	 034 	035034	Rio Saliceto	H298
035	 035 	035035	Rolo	H500
035	 036 	035036	Rubiera	H628
035	 037 	035037	San Martino in Rio	I011
035	 038 	035038	San Polo dEnza	I123
035	 039 	035039	SantIlario dEnza	I342
035	 040 	035040	Scandiano	I496
035	 041 	035041	Toano	L184
035	 042 	035042	Vetto	L815
035	 043 	035043	Vezzano sul Crostolo	L820
035	 044 	035044	Viano	L831
035	 045 	035045	Villa Minozzo	L969
036	 001 	036001	Bastiglia	A713
036	 002 	036002	Bomporto	A959
036	 003 	036003	Campogalliano	B539
036	 004 	036004	Camposanto	B566
036	 005 	036005	Carpi	B819
036	 006 	036006	Castelfranco Emilia	C107
036	 007 	036007	Castelnuovo Rangone	C242
036	 008 	036008	Castelvetro di Modena	C287
036	 009 	036009	Cavezzo	C398
036	 010 	036010	Concordia sulla Secchia	C951
036	 011 	036011	Fanano	D486
036	 012 	036012	Finale Emilia	D599
036	 013 	036013	Fiorano Modenese	D607
036	 014 	036014	Fiumalbo	D617
036	 015 	036015	Formigine	D711
036	 016 	036016	Frassinoro	D783
036	 017 	036017	Guiglia	E264
036	 018 	036018	Lama Mocogno	E426
036	 019 	036019	Maranello	E904
036	 020 	036020	Marano sul Panaro	E905
036	 021 	036021	Medolla	F087
036	 022 	036022	Mirandola	F240
036	 023 	036023	Modena	F257
036	 024 	036024	Montecreto	F484
036	 025 	036025	Montefiorino	F503
036	 026 	036026	Montese	F642
036	 027 	036027	Nonantola	F930
036	 028 	036028	Novi di Modena	F966
036	 029 	036029	Palagano	G250
036	 030 	036030	Pavullo nel Frignano	G393
036	 031 	036031	Pievepelago	G649
036	 032 	036032	Polinago	G789
036	 033 	036033	Prignano sulla Secchia	H061
036	 034 	036034	Ravarino	H195
036	 035 	036035	Riolunato	H303
036	 036 	036036	San Cesario sul Panaro	H794
036	 037 	036037	San Felice sul Panaro	H835
036	 038 	036038	San Possidonio	I128
036	 039 	036039	San Prospero	I133
036	 040 	036040	Sassuolo	I462
036	 041 	036041	Savignano sul Panaro	I473
036	 042 	036042	Serramazzoni	F357
036	 043 	036043	Sestola	I689
036	 044 	036044	Soliera	I802
036	 045 	036045	Spilamberto	I903
036	 046 	036046	Vignola	L885
036	 047 	036047	Zocca	M183
037	 001 	037001	Anzola dellEmilia	A324
037	 002 	037002	Argelato	A392
037	 003 	037003	Baricella	A665
037	 004 	037004	Bazzano	A726
037	 005 	037005	Bentivoglio	A785
037	 006 	037006	Bologna	A944
037	 007 	037007	Borgo Tossignano	B044
037	 008 	037008	Budrio	B249
037	 009 	037009	Calderara di Reno	B399
037	 010 	037010	Camugnano	B572
037	 011 	037011	Casalecchio di Reno	B880
037	 012 	037012	Casalfiumanese	B892
037	 013 	037013	Castel dAiano	C075
037	 014 	037014	Castel del Rio	C086
037	 015 	037015	Castel di Casio	B969
037	 016 	037016	Castel Guelfo di Bologna	C121
037	 017 	037017	Castello dArgile	C185
037	 018 	037018	Castello di Serravalle	C191
037	 019 	037019	Castel Maggiore	C204
037	 020 	037020	Castel San Pietro Terme	C265
037	 021 	037021	Castenaso	C292
037	 022 	037022	Castiglione dei Pepoli	C296
037	 023 	037023	Crespellano	D158
037	 024 	037024	Crevalcore	D166
037	 025 	037025	Dozza	D360
037	 026 	037026	Fontanelice	D668
037	 027 	037027	Gaggio Montano	D847
037	 028 	037028	Galliera	D878
037	 029 	037029	Granaglione	E135
037	 030 	037030	Granarolo dellEmilia	E136
037	 031 	037031	Grizzana Morandi	E187
037	 032 	037032	Imola	E289
037	 033 	037033	Lizzano in Belvedere	A771
037	 034 	037034	Loiano	E655
037	 035 	037035	Malalbergo	E844
037	 036 	037036	Marzabotto	B689
037	 037 	037037	Medicina	F083
037	 038 	037038	Minerbio	F219
037	 039 	037039	Molinella	F288
037	 040 	037040	Monghidoro	F363
037	 041 	037041	Monterenzio	F597
037	 042 	037042	Monte San Pietro	F627
037	 043 	037043	Monteveglio	F659
037	 044 	037044	Monzuno	F706
037	 045 	037045	Mordano	F718
037	 046 	037046	Ozzano dellEmilia	G205
037	 047 	037047	Pianoro	G570
037	 048 	037048	Pieve di Cento	G643
037	 049 	037049	Porretta Terme	A558
037	 050 	037050	Sala Bolognese	H678
037	 051 	037051	San Benedetto Val di Sambro	G566
037	 052 	037052	San Giorgio di Piano	H896
037	 053 	037053	San Giovanni in Persiceto	G467
037	 054 	037054	San Lazzaro di Savena	H945
037	 055 	037055	San Pietro in Casale	I110
037	 056 	037056	SantAgata Bolognese	I191
037	 057 	037057	Sasso Marconi	G972
037	 058 	037058	Savigno	I474
037	 059 	037059	Vergato	L762
037	 060 	037060	Zola Predosa	M185
038	 001 	038001	Argenta	A393
038	 002 	038002	Berra	A806
038	 003 	038003	Bondeno	A965
038	 004 	038004	Cento	C469
038	 005 	038005	Codigoro	C814
038	 006 	038006	Comacchio	C912
038	 007 	038007	Copparo	C980
038	 008 	038008	Ferrara	D548
038	 009 	038009	Formignana	D713
038	 010 	038010	Jolanda di Savoia	E320
038	 011 	038011	Lagosanto	E410
038	 012 	038012	Masi Torello	F016
038	 013 	038013	Massa Fiscaglia	F026
038	 014 	038014	Mesola	F156
038	 015 	038015	Migliarino	F198
038	 016 	038016	Mirabello	F235
038	 017 	038017	Ostellato	G184
038	 018 	038018	Poggio Renatico	G768
038	 019 	038019	Portomaggiore	G916
038	 020 	038020	Ro	H360
038	 021 	038021	SantAgostino	I209
038	 022 	038022	Vigarano Mainarda	L868
038	 023 	038023	Voghiera	M110
038	 024 	038024	Tresigallo	L390
038	 025 	038025	Goro	E107
038	 026 	038026	Migliaro	F199
039	 001 	039001	Alfonsine	A191
039	 002 	039002	Bagnacavallo	A547
039	 003 	039003	Bagnara di Romagna	A551
039	 004 	039004	Brisighella	B188
039	 005 	039005	Casola Valsenio	B982
039	 006 	039006	Castel Bolognese	C065
039	 007 	039007	Cervia	C553
039	 008 	039008	Conselice	C963
039	 009 	039009	Cotignola	D121
039	 010 	039010	Faenza	D458
039	 011 	039011	Fusignano	D829
039	 012 	039012	Lugo	E730
039	 013 	039013	Massa Lombarda	F029
039	 014 	039014	Ravenna	H199
039	 015 	039015	Riolo Terme	H302
039	 016 	039016	Russi	H642
039	 017 	039017	SantAgata sul Santerno	I196
039	 018 	039018	Solarolo	I787
040	 001 	040001	Bagno di Romagna	A565
040	 003 	040003	Bertinoro	A809
040	 004 	040004	Borghi	B001
040	 005 	040005	Castrocaro Terme e Terra del Sole	C339
040	 007 	040007	Cesena	C573
040	 008 	040008	Cesenatico	C574
040	 009 	040009	Civitella di Romagna	C777
040	 011 	040011	Dovadola	D357
040	 012 	040012	ForlÔøΩ	D704
040	 013 	040013	Forlimpopoli	D705
040	 014 	040014	Galeata	D867
040	 015 	040015	Gambettola	D899
040	 016 	040016	Gatteo	D935
040	 018 	040018	Longiano	E675
040	 019 	040019	Meldola	F097
040	 020 	040020	Mercato Saraceno	F139
040	 022 	040022	Modigliana	F259
040	 028 	040028	Montiano	F668
040	 031 	040031	Portico e San Benedetto	G904
040	 032 	040032	Predappio	H017
040	 033 	040033	Premilcuore	H034
040	 036 	040036	Rocca San Casciano	H437
040	 037 	040037	Roncofreddo	H542
040	 041 	040041	San Mauro Pascoli	I027
040	 043 	040043	Santa Sofia	I310
040	 044 	040044	Sarsina	I444
040	 045 	040045	Savignano sul Rubicone	I472
040	 046 	040046	Sogliano al Rubicone	I779
040	 049 	040049	Tredozio	L361
040	 050 	040050	Verghereto	L764
041	 001 	041001	Acqualagna	A035
041	 002 	041002	Apecchio	A327
041	 003 	041003	Auditore	A493
041	 004 	041004	Barchi	A639
041	 005 	041005	Belforte allIsauro	A740
041	 006 	041006	Borgo Pace	B026
041	 007 	041007	Cagli	B352
041	 008 	041008	Cantiano	B636
041	 009 	041009	Carpegna	B816
041	 010 	041010	Cartoceto	B846
041	 012 	041012	Colbordolo	C830
041	 013 	041013	Fano	D488
041	 014 	041014	Fermignano	D541
041	 015 	041015	Fossombrone	D749
041	 016 	041016	Fratte Rosa	D791
041	 017 	041017	Frontino	D807
041	 018 	041018	Frontone	D808
041	 019 	041019	Gabicce Mare	D836
041	 020 	041020	Gradara	E122
041	 021 	041021	Isola del Piano	E351
041	 022 	041022	Lunano	E743
041	 023 	041023	Macerata Feltria	E785
041	 025 	041025	Mercatello sul Metauro	F135
041	 026 	041026	Mercatino Conca	F136
041	 027 	041027	Mombaroccio	F310
041	 028 	041028	Mondavio	F347
041	 029 	041029	Mondolfo	F348
041	 030 	041030	Montecalvo in Foglia	F450
041	 031 	041031	Monte Cerignone	F467
041	 032 	041032	Monteciccardo	F474
041	 033 	041033	Montecopiolo	F478
041	 034 	041034	Montefelcino	F497
041	 035 	041035	Monte Grimano Terme	F524
041	 036 	041036	Montelabbate	F533
041	 037 	041037	Montemaggiore al Metauro	F555
041	 038 	041038	Monte Porzio	F589
041	 040 	041040	Orciano di Pesaro	G089
041	 041 	041041	Peglio	G416
041	 043 	041043	Pergola	G453
041	 044 	041044	Pesaro	G479
041	 045 	041045	Petriano	G514
041	 046 	041046	Piagge	G537
041	 047 	041047	Piandimeleto	G551
041	 048 	041048	Pietrarubbia	G627
041	 049 	041049	Piobbico	G682
041	 050 	041050	Saltara	H721
041	 051 	041051	San Costanzo	H809
041	 052 	041052	San Giorgio di Pesaro	H886
041	 054 	041054	San Lorenzo in Campo	H958
041	 056 	041056	SantAngelo in Lizzola	I285
041	 057 	041057	SantAngelo in Vado	I287
041	 058 	041058	SantIppolito	I344
041	 059 	041059	Sassocorvaro	I459
041	 060 	041060	Sassofeltrio	I460
041	 061 	041061	Serra SantAbbondio	I654
041	 062 	041062	Serrungarina	I670
041	 064 	041064	Tavoleto	L078
041	 065 	041065	Tavullia	L081
041	 066 	041066	Urbania	L498
041	 067 	041067	Urbino	L500
042	 001 	042001	Agugliano	A092
042	 002 	042002	Ancona	A271
042	 003 	042003	Arcevia	A366
042	 004 	042004	Barbara	A626
042	 005 	042005	Belvedere Ostrense	A769
042	 006 	042006	Camerano	B468
042	 007 	042007	Camerata Picena	B470
042	 008 	042008	Castelbellino	C060
042	 009 	042009	Castel Colonna	C071
042	 010 	042010	Castelfidardo	C100
042	 011 	042011	Castelleone di Suasa	C152
042	 012 	042012	Castelplanio	C248
042	 013 	042013	Cerreto dEsi	C524
042	 014 	042014	Chiaravalle	C615
042	 015 	042015	Corinaldo	D007
042	 016 	042016	Cupramontana	D211
042	 017 	042017	Fabriano	D451
042	 018 	042018	Falconara Marittima	D472
042	 019 	042019	Filottrano	D597
042	 020 	042020	Genga	D965
042	 021 	042021	Jesi	E388
042	 022 	042022	Loreto	E690
042	 023 	042023	Maiolati Spontini	E837
042	 024 	042024	Mergo	F145
042	 025 	042025	Monsano	F381
042	 026 	042026	Montecarotto	F453
042	 027 	042027	Montemarciano	F560
042	 028 	042028	Monterado	F593
042	 029 	042029	Monte Roberto	F600
042	 030 	042030	Monte San Vito	F634
042	 031 	042031	Morro dAlba	F745
042	 032 	042032	Numana	F978
042	 033 	042033	Offagna	G003
042	 034 	042034	Osimo	G157
042	 035 	042035	Ostra	F401
042	 036 	042036	Ostra Vetere	F581
042	 037 	042037	Poggio San Marcello	G771
042	 038 	042038	Polverigi	G803
042	 039 	042039	Ripe	H322
042	 040 	042040	Rosora	H575
042	 041 	042041	San Marcello	H979
042	 042 	042042	San Paolo di Jesi	I071
042	 043 	042043	Santa Maria Nuova	I251
042	 044 	042044	Sassoferrato	I461
042	 045 	042045	Senigallia	I608
042	 046 	042046	Serra de Conti	I643
042	 047 	042047	Serra San Quirico	I653
042	 048 	042048	Sirolo	I758
042	 049 	042049	Staffolo	I932
043	 001 	043001	Acquacanina	A031
043	 002 	043002	Apiro	A329
043	 003 	043003	Appignano	A334
043	 004 	043004	Belforte del Chienti	A739
043	 005 	043005	Bolognola	A947
043	 006 	043006	Caldarola	B398
043	 007 	043007	Camerino	B474
043	 008 	043008	Camporotondo di Fiastrone	B562
043	 009 	043009	Castelraimondo	C251
043	 010 	043010	Castelsantangelo sul Nera	C267
043	 011 	043011	Cessapalombo	C582
043	 012 	043012	Cingoli	C704
043	 013 	043013	Civitanova Marche	C770
043	 014 	043014	Colmurano	C886
043	 015 	043015	Corridonia	D042
043	 016 	043016	Esanatoglia	D429
043	 017 	043017	Fiastra	D564
043	 018 	043018	Fiordimonte	D609
043	 019 	043019	Fiuminata	D628
043	 020 	043020	Gagliole	D853
043	 021 	043021	Gualdo	E228
043	 022 	043022	Loro Piceno	E694
043	 023 	043023	Macerata	E783
043	 024 	043024	Matelica	F051
043	 025 	043025	Mogliano	F268
043	 026 	043026	Montecassiano	F454
043	 027 	043027	Monte Cavallo	F460
043	 028 	043028	Montecosaro	F482
043	 029 	043029	Montefano	F496
043	 030 	043030	Montelupone	F552
043	 031 	043031	Monte San Giusto	F621
043	 032 	043032	Monte San Martino	F622
043	 033 	043033	Morrovalle	F749
043	 034 	043034	Muccia	F793
043	 035 	043035	Penna San Giovanni	G436
043	 036 	043036	Petriolo	G515
043	 037 	043037	Pievebovigliana	G637
043	 038 	043038	Pieve Torina	G657
043	 039 	043039	Pioraco	G690
043	 040 	043040	Poggio San Vicino	D566
043	 041 	043041	Pollenza	F567
043	 042 	043042	Porto Recanati	G919
043	 043 	043043	Potenza Picena	F632
043	 044 	043044	Recanati	H211
043	 045 	043045	Ripe San Ginesio	H323
043	 046 	043046	San Ginesio	H876
043	 047 	043047	San Severino Marche	I156
043	 048 	043048	SantAngelo in Pontano	I286
043	 049 	043049	Sarnano	I436
043	 050 	043050	Sefro	I569
043	 051 	043051	Serrapetrona	I651
043	 052 	043052	Serravalle di Chienti	I661
043	 053 	043053	Tolentino	L191
043	 054 	043054	Treia	L366
043	 055 	043055	Urbisaglia	L501
043	 056 	043056	Ussita	L517
043	 057 	043057	Visso	M078
044	 001 	044001	Acquasanta Terme	A044
044	 002 	044002	Acquaviva Picena	A047
044	 005 	044005	Appignano del Tronto	A335
044	 006 	044006	Arquata del Tronto	A437
044	 007 	044007	Ascoli Piceno	A462
044	 010 	044010	Carassai	B727
044	 011 	044011	Castel di Lama	C093
044	 012 	044012	Castignano	C321
044	 013 	044013	Castorano	C331
044	 014 	044014	Colli del Tronto	C877
044	 015 	044015	Comunanza	C935
044	 016 	044016	Cossignano	D096
044	 017 	044017	Cupra Marittima	D210
044	 020 	044020	Folignano	D652
044	 021 	044021	Force	D691
044	 023 	044023	Grottammare	E207
044	 027 	044027	Maltignano	E868
044	 029 	044029	Massignano	F044
044	 031 	044031	Monsampolo del Tronto	F380
044	 032 	044032	Montalto delle Marche	F415
044	 034 	044034	Montedinove	F487
044	 036 	044036	Montefiore dellAso	F501
044	 038 	044038	Montegallo	F516
044	 044 	044044	Montemonaco	F570
044	 045 	044045	Monteprandone	F591
044	 054 	044054	Offida	G005
044	 056 	044056	Palmiano	G289
044	 063 	044063	Ripatransone	H321
044	 064 	044064	Roccafluvione	H390
044	 065 	044065	Rotella	H588
044	 066 	044066	San Benedetto del Tronto	H769
044	 071 	044071	Spinetoli	I912
044	 073 	044073	Venarotta	L728
045	 001 	045001	Aulla	A496
045	 002 	045002	Bagnone	A576
045	 003 	045003	Carrara	B832
045	 004 	045004	Casola in Lunigiana	B979
045	 005 	045005	Comano	C914
045	 006 	045006	Filattiera	D590
045	 007 	045007	Fivizzano	D629
045	 008 	045008	Fosdinovo	D735
045	 009 	045009	Licciana Nardi	E574
045	 010 	045010	Massa	F023
045	 011 	045011	Montignoso	F679
045	 012 	045012	Mulazzo	F802
045	 013 	045013	Podenzana	G746
045	 014 	045014	Pontremoli	G870
045	 015 	045015	Tresana	L386
045	 016 	045016	Villafranca in Lunigiana	L946
045	 017 	045017	Zeri	M169
046	 001 	046001	Altopascio	A241
046	 002 	046002	Bagni di Lucca	A560
046	 003 	046003	Barga	A657
046	 004 	046004	Borgo a Mozzano	B007
046	 005 	046005	Camaiore	B455
046	 006 	046006	Camporgiano	B557
046	 007 	046007	Capannori	B648
046	 008 	046008	Careggine	B760
046	 009 	046009	Castelnuovo di Garfagnana	C236
046	 010 	046010	Castiglione di Garfagnana	C303
046	 011 	046011	Coreglia Antelminelli	C996
046	 012 	046012	Fabbriche di Vallico	D449
046	 013 	046013	Forte dei Marmi	D730
046	 014 	046014	Fosciandora	D734
046	 015 	046015	Gallicano	D874
046	 016 	046016	Giuncugnano	E059
046	 017 	046017	Lucca	E715
046	 018 	046018	Massarosa	F035
046	 019 	046019	Minucciano	F225
046	 020 	046020	Molazzana	F283
046	 021 	046021	Montecarlo	F452
046	 022 	046022	Pescaglia	G480
046	 023 	046023	Piazza al Serchio	G582
046	 024 	046024	Pietrasanta	G628
046	 025 	046025	Pieve Fosciana	G648
046	 026 	046026	Porcari	G882
046	 027 	046027	San Romano in Garfagnana	I142
046	 028 	046028	Seravezza	I622
046	 029 	046029	Sillano	I737
046	 030 	046030	Stazzema	I942
046	 031 	046031	Vagli Sotto	L533
046	 032 	046032	Vergemoli	L763
046	 033 	046033	Viareggio	L833
046	 034 	046034	Villa Basilica	L913
046	 035 	046035	Villa Collemandina	L926
047	 001 	047001	Abetone	A012
047	 002 	047002	Agliana	A071
047	 003 	047003	Buggiano	B251
047	 004 	047004	Cutigliano	D235
047	 005 	047005	Lamporecchio	E432
047	 006 	047006	Larciano	E451
047	 007 	047007	Marliana	E960
047	 008 	047008	Massa e Cozzile	F025
047	 009 	047009	Monsummano Terme	F384
047	 010 	047010	Montale	F410
047	 011 	047011	Montecatini-Terme	A561
047	 012 	047012	Pescia	G491
047	 013 	047013	Pieve a Nievole	G636
047	 014 	047014	Pistoia	G713
047	 015 	047015	Piteglio	G715
047	 016 	047016	Ponte Buggianese	G833
047	 017 	047017	Quarrata	H109
047	 018 	047018	Sambuca Pistoiese	H744
047	 019 	047019	San Marcello Pistoiese	H980
047	 020 	047020	Serravalle Pistoiese	I660
047	 021 	047021	Uzzano	L522
047	 022 	047022	Chiesina Uzzanese	C631
048	 001 	048001	Bagno a Ripoli	A564
048	 002 	048002	Barberino di Mugello	A632
048	 003 	048003	Barberino Val dElsa	A633
048	 004 	048004	Borgo San Lorenzo	B036
048	 005 	048005	Calenzano	B406
048	 006 	048006	Campi Bisenzio	B507
048	 008 	048008	Capraia e Limite	B684
048	 010 	048010	Castelfiorentino	C101
048	 011 	048011	Cerreto Guidi	C529
048	 012 	048012	Certaldo	C540
048	 013 	048013	Dicomano	D299
048	 014 	048014	Empoli	D403
048	 015 	048015	Fiesole	D575
048	 016 	048016	Figline Valdarno	D583
048	 017 	048017	Firenze	D612
048	 018 	048018	Firenzuola	D613
048	 019 	048019	Fucecchio	D815
048	 020 	048020	Gambassi Terme	D895
048	 021 	048021	Greve in Chianti	E169
048	 022 	048022	Impruneta	E291
048	 023 	048023	Incisa in Val dArno	E296
048	 024 	048024	Lastra a Signa	E466
048	 025 	048025	Londa	E668
048	 026 	048026	Marradi	E971
048	 027 	048027	Montaione	F398
048	 028 	048028	Montelupo Fiorentino	F551
048	 030 	048030	Montespertoli	F648
048	 031 	048031	Palazzuolo sul Senio	G270
048	 032 	048032	Pelago	G420
048	 033 	048033	Pontassieve	G825
048	 035 	048035	Reggello	H222
048	 036 	048036	Rignano sullArno	H286
048	 037 	048037	Rufina	H635
048	 038 	048038	San Casciano in Val di Pesa	H791
048	 039 	048039	San Godenzo	H937
048	 040 	048040	San Piero a Sieve	I085
048	 041 	048041	Scandicci	B962
048	 042 	048042	Scarperia	I514
048	 043 	048043	Sesto Fiorentino	I684
048	 044 	048044	Signa	I728
048	 045 	048045	Tavarnelle Val di Pesa	L067
048	 046 	048046	Vaglia	L529
048	 049 	048049	Vicchio	L838
048	 050 	048050	Vinci	M059
049	 001 	049001	Bibbona	A852
049	 002 	049002	Campiglia Marittima	B509
049	 003 	049003	Campo nellElba	B553
049	 004 	049004	Capoliveri	B669
049	 005 	049005	Capraia Isola	B685
049	 006 	049006	Castagneto Carducci	C044
049	 007 	049007	Cecina	C415
049	 008 	049008	Collesalvetti	C869
049	 009 	049009	Livorno	E625
049	 010 	049010	Marciana	E930
049	 011 	049011	Marciana Marina	E931
049	 012 	049012	Piombino	G687
049	 013 	049013	Porto Azzurro	E680
049	 014 	049014	Portoferraio	G912
049	 015 	049015	Rio Marina	H305
049	 016 	049016	Rio nellElba	H297
049	 017 	049017	Rosignano Marittimo	H570
049	 018 	049018	San Vincenzo	I390
049	 019 	049019	Sassetta	I454
049	 020 	049020	Suvereto	L019
050	 001 	050001	Bientina	A864
050	 002 	050002	Buti	B303
050	 003 	050003	Calci	B390
050	 004 	050004	Calcinaia	B392
050	 005 	050005	Capannoli	B647
050	 006 	050006	Casale Marittimo	B878
050	 007 	050007	Casciana Terme	A559
050	 008 	050008	Cascina	B950
050	 009 	050009	Castelfranco di Sotto	C113
050	 010 	050010	Castellina Marittima	C174
050	 011 	050011	Castelnuovo di Val di Cecina	C244
050	 012 	050012	Chianni	C609
050	 013 	050013	Crespina	D160
050	 014 	050014	Fauglia	D510
050	 015 	050015	Guardistallo	E250
050	 016 	050016	Lajatico	E413
050	 017 	050017	Lari	E455
050	 018 	050018	Lorenzana	E688
050	 019 	050019	Montecatini Val di Cecina	F458
050	 020 	050020	Montescudaio	F640
050	 021 	050021	Monteverdi Marittimo	F661
050	 022 	050022	Montopoli in Val dArno	F686
050	 023 	050023	Orciano Pisano	G090
050	 024 	050024	Palaia	G254
050	 025 	050025	Peccioli	G395
050	 026 	050026	Pisa	G702
050	 027 	050027	Pomarance	G804
050	 028 	050028	Ponsacco	G822
050	 029 	050029	Pontedera	G843
050	 030 	050030	Riparbella	H319
050	 031 	050031	San Giuliano Terme	A562
050	 032 	050032	San Miniato	I046
050	 033 	050033	Santa Croce sullArno	I177
050	 034 	050034	Santa Luce	I217
050	 035 	050035	Santa Maria a Monte	I232
050	 036 	050036	Terricciola	L138
050	 037 	050037	Vecchiano	L702
050	 038 	050038	Vicopisano	L850
050	 039 	050039	Volterra	M126
051	 001 	051001	Anghiari	A291
051	 002 	051002	Arezzo	A390
051	 003 	051003	Badia Tedalda	A541
051	 004 	051004	Bibbiena	A851
051	 005 	051005	Bucine	B243
051	 006 	051006	Capolona	B670
051	 007 	051007	Caprese Michelangelo	B693
051	 008 	051008	Castel Focognano	C102
051	 009 	051009	Castelfranco di Sopra	C112
051	 010 	051010	Castel San NiccolÔøΩ	C263
051	 011 	051011	Castiglion Fibocchi	C318
051	 012 	051012	Castiglion Fiorentino	C319
051	 013 	051013	Cavriglia	C407
051	 014 	051014	Chitignano	C648
051	 015 	051015	Chiusi della Verna	C663
051	 016 	051016	Civitella in Val di Chiana	C774
051	 017 	051017	Cortona	D077
051	 018 	051018	Foiano della Chiana	D649
051	 019 	051019	Laterina	E468
051	 020 	051020	Loro Ciuffenna	E693
051	 021 	051021	Lucignano	E718
051	 022 	051022	Marciano della Chiana	E933
051	 023 	051023	Montemignaio	F565
051	 024 	051024	Monterchi	F594
051	 025 	051025	Monte San Savino	F628
051	 026 	051026	Montevarchi	F656
051	 027 	051027	Ortignano Raggiolo	G139
051	 028 	051028	Pergine Valdarno	G451
051	 029 	051029	Pian di Sco	G552
051	 030 	051030	Pieve Santo Stefano	G653
051	 031 	051031	Poppi	G879
051	 032 	051032	Pratovecchio	H008
051	 033 	051033	San Giovanni Valdarno	H901
051	 034 	051034	Sansepolcro	I155
051	 035 	051035	Sestino	I681
051	 036 	051036	Stia	I952
051	 037 	051037	Subbiano	I991
051	 038 	051038	Talla	L038
051	 039 	051039	Terranuova Bracciolini	L123
052	 001 	052001	Abbadia San Salvatore	A006
052	 002 	052002	Asciano	A461
052	 003 	052003	Buonconvento	B269
052	 004 	052004	Casole dElsa	B984
052	 005 	052005	Castellina in Chianti	C172
052	 006 	052006	Castelnuovo Berardenga	C227
052	 007 	052007	Castiglione dOrcia	C313
052	 008 	052008	Cetona	C587
052	 009 	052009	Chianciano Terme	C608
052	 010 	052010	Chiusdino	C661
052	 011 	052011	Chiusi	C662
052	 012 	052012	Colle di Val dElsa	C847
052	 013 	052013	Gaiole in Chianti	D858
052	 014 	052014	Montalcino	F402
052	 015 	052015	Montepulciano	F592
052	 016 	052016	Monteriggioni	F598
052	 017 	052017	Monteroni dArbia	F605
052	 018 	052018	Monticiano	F676
052	 019 	052019	Murlo	F815
052	 020 	052020	Piancastagnaio	G547
052	 021 	052021	Pienza	G602
052	 022 	052022	Poggibonsi	G752
052	 023 	052023	Radda in Chianti	H153
052	 024 	052024	Radicofani	H156
052	 025 	052025	Radicondoli	H157
052	 026 	052026	Rapolano Terme	H185
052	 027 	052027	San Casciano dei Bagni	H790
052	 028 	052028	San Gimignano	H875
052	 029 	052029	San Giovanni dAsso	H911
052	 030 	052030	San Quirico dOrcia	I135
052	 031 	052031	Sarteano	I445
052	 032 	052032	Siena	I726
052	 033 	052033	Sinalunga	A468
052	 034 	052034	Sovicille	I877
052	 035 	052035	Torrita di Siena	L303
052	 036 	052036	Trequanda	L384
053	 001 	053001	Arcidosso	A369
053	 002 	053002	Campagnatico	B497
053	 003 	053003	Capalbio	B646
053	 004 	053004	Castel del Piano	C085
053	 005 	053005	CastellAzzara	C147
053	 006 	053006	Castiglione della Pescaia	C310
053	 007 	053007	Cinigiano	C705
053	 008 	053008	Civitella Paganico	C782
053	 009 	053009	Follonica	D656
053	 010 	053010	Gavorrano	D948
053	 011 	053011	Grosseto	E202
053	 012 	053012	Isola del Giglio	E348
053	 013 	053013	Magliano in Toscana	E810
053	 014 	053014	Manciano	E875
053	 015 	053015	Massa Marittima	F032
053	 016 	053016	Monte Argentario	F437
053	 017 	053017	Montieri	F677
053	 018 	053018	Orbetello	G088
053	 019 	053019	Pitigliano	G716
053	 020 	053020	Roccalbegna	H417
053	 021 	053021	Roccastrada	H449
053	 022 	053022	Santa Fiora	I187
053	 023 	053023	Scansano	I504
053	 024 	053024	Scarlino	I510
053	 025 	053025	Seggiano	I571
053	 026 	053026	Sorano	I841
053	 027 	053027	Monterotondo Marittimo	F612
053	 028 	053028	Semproniano	I601
054	 001 	054001	Assisi	A475
054	 002 	054002	Bastia Umbra	A710
054	 003 	054003	Bettona	A832
054	 004 	054004	Bevagna	A835
054	 005 	054005	Campello sul Clitunno	B504
054	 006 	054006	Cannara	B609
054	 007 	054007	Cascia	B948
054	 008 	054008	Castel Ritaldi	C252
054	 009 	054009	Castiglione del Lago	C309
054	 010 	054010	Cerreto di Spoleto	C527
054	 011 	054011	Citerna	C742
054	 012 	054012	CittÔøΩ della Pieve	C744
054	 013 	054013	CittÔøΩ di Castello	C745
054	 014 	054014	Collazzone	C845
054	 015 	054015	Corciano	C990
054	 016 	054016	Costacciaro	D108
054	 017 	054017	Deruta	D279
054	 018 	054018	Foligno	D653
054	 019 	054019	Fossato di Vico	D745
054	 020 	054020	Fratta Todina	D787
054	 021 	054021	Giano dellUmbria	E012
054	 022 	054022	Gualdo Cattaneo	E229
054	 023 	054023	Gualdo Tadino	E230
054	 024 	054024	Gubbio	E256
054	 025 	054025	Lisciano Niccone	E613
054	 026 	054026	Magione	E805
054	 027 	054027	Marsciano	E975
054	 028 	054028	Massa Martana	F024
054	 029 	054029	Monte Castello di Vibio	F456
054	 030 	054030	Montefalco	F492
054	 031 	054031	Monteleone di Spoleto	F540
054	 032 	054032	Monte Santa Maria Tiberina	F629
054	 033 	054033	Montone	F685
054	 034 	054034	Nocera Umbra	F911
054	 035 	054035	Norcia	F935
054	 036 	054036	Paciano	G212
054	 037 	054037	Panicale	G308
054	 038 	054038	Passignano sul Trasimeno	G359
054	 039 	054039	Perugia	G478
054	 040 	054040	Piegaro	G601
054	 041 	054041	Pietralunga	G618
054	 042 	054042	Poggiodomo	G758
054	 043 	054043	Preci	H015
054	 044 	054044	San Giustino	H935
054	 045 	054045	SantAnatolia di Narco	I263
054	 046 	054046	Scheggia e Pascelupo	I522
054	 047 	054047	Scheggino	I523
054	 048 	054048	Sellano	I585
054	 049 	054049	Sigillo	I727
054	 050 	054050	Spello	I888
054	 051 	054051	Spoleto	I921
054	 052 	054052	Todi	L188
054	 053 	054053	Torgiano	L216
054	 054 	054054	Trevi	L397
054	 055 	054055	Tuoro sul Trasimeno	L466
054	 056 	054056	Umbertide	D786
054	 057 	054057	Valfabbrica	L573
054	 058 	054058	Vallo di Nera	L627
054	 059 	054059	Valtopina	L653
055	 001 	055001	Acquasparta	A045
055	 002 	055002	Allerona	A207
055	 003 	055003	Alviano	A242
055	 004 	055004	Amelia	A262
055	 005 	055005	Arrone	A439
055	 006 	055006	Attigliano	A490
055	 007 	055007	Baschi	A691
055	 008 	055008	Calvi dellUmbria	B446
055	 009 	055009	Castel Giorgio	C117
055	 010 	055010	Castel Viscardo	C289
055	 011 	055011	Fabro	D454
055	 012 	055012	Ferentillo	D538
055	 013 	055013	Ficulle	D570
055	 014 	055014	Giove	E045
055	 015 	055015	Guardea	E241
055	 016 	055016	Lugnano in Teverina	E729
055	 017 	055017	Montecastrilli	F457
055	 018 	055018	Montecchio	F462
055	 019 	055019	Montefranco	F510
055	 020 	055020	Montegabbione	F513
055	 021 	055021	Monteleone dOrvieto	F543
055	 022 	055022	Narni	F844
055	 023 	055023	Orvieto	G148
055	 024 	055024	Otricoli	G189
055	 025 	055025	Parrano	G344
055	 026 	055026	Penna in Teverina	G432
055	 027 	055027	Polino	G790
055	 028 	055028	Porano	G881
055	 029 	055029	San Gemini	H857
055	 030 	055030	San Venanzo	I381
055	 031 	055031	Stroncone	I981
055	 032 	055032	Terni	L117
055	 033 	055033	Avigliano Umbro	M258
056	 001 	056001	Acquapendente	A040
056	 002 	056002	Arlena di Castro	A412
056	 003 	056003	Bagnoregio	A577
056	 004 	056004	Barbarano Romano	A628
056	 005 	056005	Bassano Romano	A704
056	 006 	056006	Bassano in Teverina	A706
056	 007 	056007	Blera	A857
056	 008 	056008	Bolsena	A949
056	 009 	056009	Bomarzo	A955
056	 010 	056010	Calcata	B388
056	 011 	056011	Canepina	B597
056	 012 	056012	Canino	B604
056	 013 	056013	Capodimonte	B663
056	 014 	056014	Capranica	B688
056	 015 	056015	Caprarola	B691
056	 016 	056016	Carbognano	B735
056	 017 	056017	Castel SantElia	C269
056	 018 	056018	Castiglione in Teverina	C315
056	 019 	056019	Celleno	C446
056	 020 	056020	Cellere	C447
056	 021 	056021	Civita Castellana	C765
056	 022 	056022	Civitella dAgliano	C780
056	 023 	056023	Corchiano	C988
056	 024 	056024	Fabrica di Roma	D452
056	 025 	056025	Faleria	D475
056	 026 	056026	Farnese	D503
056	 027 	056027	Gallese	D870
056	 028 	056028	Gradoli	E126
056	 029 	056029	Graffignano	E128
056	 030 	056030	Grotte di Castro	E210
056	 031 	056031	Ischia di Castro	E330
056	 032 	056032	Latera	E467
056	 033 	056033	Lubriano	E713
056	 034 	056034	Marta	E978
056	 035 	056035	Montalto di Castro	F419
056	 036 	056036	Montefiascone	F499
056	 037 	056037	Monte Romano	F603
056	 038 	056038	Monterosi	F606
056	 039 	056039	Nepi	F868
056	 040 	056040	Onano	G065
056	 041 	056041	Oriolo Romano	G111
056	 042 	056042	Orte	G135
056	 043 	056043	Piansano	G571
056	 044 	056044	Proceno	H071
056	 045 	056045	Ronciglione	H534
056	 046 	056046	Villa San Giovanni in Tuscia	H913
056	 047 	056047	San Lorenzo Nuovo	H969
056	 048 	056048	Soriano nel Cimino	I855
056	 049 	056049	Sutri	L017
056	 050 	056050	Tarquinia	D024
056	 051 	056051	Tessennano	L150
056	 052 	056052	Tuscania	L310
056	 053 	056053	Valentano	L569
056	 054 	056054	Vallerano	L612
056	 055 	056055	Vasanello	A701
056	 056 	056056	Vejano	L713
056	 057 	056057	Vetralla	L814
056	 058 	056058	Vignanello	L882
056	 059 	056059	Viterbo	M082
056	 060 	056060	Vitorchiano	M086
057	 001 	057001	Accumoli	A019
057	 002 	057002	Amatrice	A258
057	 003 	057003	Antrodoco	A315
057	 004 	057004	Ascrea	A464
057	 005 	057005	Belmonte in Sabina	A765
057	 006 	057006	Borbona	A981
057	 007 	057007	Borgorose	B008
057	 008 	057008	Borgo Velino	A996
057	 009 	057009	Cantalice	B627
057	 010 	057010	Cantalupo in Sabina	B631
057	 011 	057011	Casaprota	B934
057	 012 	057012	Casperia	A472
057	 013 	057013	Castel di Tora	C098
057	 014 	057014	Castelnuovo di Farfa	C224
057	 015 	057015	Castel SantAngelo	C268
057	 016 	057016	Cittaducale	C746
057	 017 	057017	Cittareale	C749
057	 018 	057018	Collalto Sabino	C841
057	 019 	057019	Colle di Tora	C857
057	 020 	057020	Collegiove	C859
057	 021 	057021	Collevecchio	C876
057	 022 	057022	Colli sul Velino	C880
057	 023 	057023	Concerviano	C946
057	 024 	057024	Configni	C959
057	 025 	057025	Contigliano	C969
057	 026 	057026	Cottanello	D124
057	 027 	057027	Fara in Sabina	D493
057	 028 	057028	Fiamignano	D560
057	 029 	057029	Forano	D689
057	 030 	057030	Frasso Sabino	D785
057	 031 	057031	Greccio	E160
057	 032 	057032	Labro	E393
057	 033 	057033	Leonessa	E535
057	 034 	057034	Longone Sabino	E681
057	 035 	057035	Magliano Sabina	E812
057	 036 	057036	Marcetelli	E927
057	 037 	057037	Micigliano	F193
057	 038 	057038	Mompeo	F319
057	 039 	057039	Montasola	F430
057	 040 	057040	Montebuono	F446
057	 041 	057041	Monteleone Sabino	F541
057	 042 	057042	Montenero Sabino	F579
057	 043 	057043	Monte San Giovanni in Sabina	F619
057	 044 	057044	Montopoli di Sabina	F687
057	 045 	057045	Morro Reatino	F746
057	 046 	057046	Nespolo	F876
057	 047 	057047	Orvinio	B595
057	 048 	057048	Paganico Sabino	G232
057	 049 	057049	Pescorocchiano	G498
057	 050 	057050	Petrella Salto	G513
057	 051 	057051	Poggio Bustone	G756
057	 052 	057052	Poggio Catino	G757
057	 053 	057053	Poggio Mirteto	G763
057	 054 	057054	Poggio Moiano	G764
057	 055 	057055	Poggio Nativo	G765
057	 056 	057056	Poggio San Lorenzo	G770
057	 057 	057057	Posta	G934
057	 058 	057058	Pozzaglia Sabina	G951
057	 059 	057059	Rieti	H282
057	 060 	057060	Rivodutri	H354
057	 061 	057061	Roccantica	H427
057	 062 	057062	Rocca Sinibalda	H446
057	 063 	057063	Salisano	H713
057	 064 	057064	Scandriglia	I499
057	 065 	057065	Selci	I581
057	 066 	057066	Stimigliano	I959
057	 067 	057067	Tarano	L046
057	 068 	057068	Toffia	L189
057	 069 	057069	Torricella in Sabina	L293
057	 070 	057070	Torri in Sabina	L286
057	 071 	057071	Turania	G507
057	 072 	057072	Vacone	L525
057	 073 	057073	Varco Sabino	L676
058	 001 	058001	Affile	A062
058	 002 	058002	Agosta	A084
058	 003 	058003	Albano Laziale	A132
058	 004 	058004	Allumiere	A210
058	 005 	058005	Anguillara Sabazia	A297
058	 006 	058006	Anticoli Corrado	A309
058	 007 	058007	Anzio	A323
058	 008 	058008	Arcinazzo Romano	A370
058	 009 	058009	Ariccia	A401
058	 010 	058010	Arsoli	A446
058	 011 	058011	Artena	A449
058	 012 	058012	Bellegra	A749
058	 013 	058013	Bracciano	B114
058	 014 	058014	Camerata Nuova	B472
058	 015 	058015	Campagnano di Roma	B496
058	 016 	058016	Canale Monterano	B576
058	 017 	058017	Canterano	B635
058	 018 	058018	Capena	B649
058	 019 	058019	Capranica Prenestina	B687
058	 020 	058020	Carpineto Romano	B828
058	 021 	058021	Casape	B932
058	 022 	058022	Castel Gandolfo	C116
058	 023 	058023	Castel Madama	C203
058	 024 	058024	Castelnuovo di Porto	C237
058	 025 	058025	Castel San Pietro Romano	C266
058	 026 	058026	Cave	C390
058	 027 	058027	Cerreto Laziale	C518
058	 028 	058028	Cervara di Roma	C543
058	 029 	058029	Cerveteri	C552
058	 030 	058030	Ciciliano	C677
058	 031 	058031	Cineto Romano	C702
058	 032 	058032	Civitavecchia	C773
058	 033 	058033	Civitella San Paolo	C784
058	 034 	058034	Colleferro	C858
058	 035 	058035	Colonna	C900
058	 036 	058036	Fiano Romano	D561
058	 037 	058037	Filacciano	D586
058	 038 	058038	Formello	D707
058	 039 	058039	Frascati	D773
058	 040 	058040	Gallicano nel Lazio	D875
058	 041 	058041	Gavignano	D945
058	 042 	058042	Genazzano	D964
058	 043 	058043	Genzano di Roma	D972
058	 044 	058044	Gerano	D978
058	 045 	058045	Gorga	E091
058	 046 	058046	Grottaferrata	E204
058	 047 	058047	Guidonia Montecelio	E263
058	 048 	058048	Jenne	E382
058	 049 	058049	Labico	E392
058	 050 	058050	Lanuvio	C767
058	 051 	058051	Licenza	E576
058	 052 	058052	Magliano Romano	E813
058	 053 	058053	Mandela	B632
058	 054 	058054	Manziana	E900
058	 055 	058055	Marano Equo	E908
058	 056 	058056	Marcellina	E924
058	 057 	058057	Marino	E958
058	 058 	058058	Mazzano Romano	F064
058	 059 	058059	Mentana	F127
058	 060 	058060	Monte Compatri	F477
058	 061 	058061	Monteflavio	F504
058	 062 	058062	Montelanico	F534
058	 063 	058063	Montelibretti	F545
058	 064 	058064	Monte Porzio Catone	F590
058	 065 	058065	Monterotondo	F611
058	 066 	058066	Montorio Romano	F692
058	 067 	058067	Moricone	F730
058	 068 	058068	Morlupo	F734
058	 069 	058069	Nazzano	F857
058	 070 	058070	Nemi	F865
058	 071 	058071	Nerola	F871
058	 072 	058072	Nettuno	F880
058	 073 	058073	Olevano Romano	G022
058	 074 	058074	Palestrina	G274
058	 075 	058075	Palombara Sabina	G293
058	 076 	058076	Percile	G444
058	 077 	058077	Pisoniano	G704
058	 078 	058078	Poli	G784
058	 079 	058079	Pomezia	G811
058	 080 	058080	Ponzano Romano	G874
058	 081 	058081	Riano	H267
058	 082 	058082	Rignano Flaminio	H288
058	 083 	058083	Riofreddo	H300
058	 084 	058084	Rocca Canterano	H387
058	 085 	058085	Rocca di Cave	H401
058	 086 	058086	Rocca di Papa	H404
058	 087 	058087	Roccagiovine	H411
058	 088 	058088	Rocca Priora	H432
058	 089 	058089	Rocca Santo Stefano	H441
058	 090 	058090	Roiate	H494
058	 091 	058091	Roma	H501
058	 092 	058092	Roviano	H618
058	 093 	058093	Sacrofano	H658
058	 094 	058094	Sambuci	H745
058	 095 	058095	San Gregorio da Sassola	H942
058	 096 	058096	San Polo dei Cavalieri	I125
058	 097 	058097	Santa Marinella	I255
058	 098 	058098	SantAngelo Romano	I284
058	 099 	058099	SantOreste	I352
058	 100 	058100	San Vito Romano	I400
058	 101 	058101	Saracinesco	I424
058	 102 	058102	Segni	I573
058	 103 	058103	Subiaco	I992
058	 104 	058104	Tivoli	L182
058	 105 	058105	Tolfa	L192
058	 106 	058106	Torrita Tiberina	L302
058	 107 	058107	Trevignano Romano	L401
058	 108 	058108	Vallepietra	L611
058	 109 	058109	Vallinfreda	L625
058	 110 	058110	Valmontone	L639
058	 111 	058111	Velletri	L719
058	 112 	058112	Vicovaro	L851
058	 113 	058113	Vivaro Romano	M095
058	 114 	058114	Zagarolo	M141
058	 115 	058115	Lariano	M207
058	 116 	058116	Ladispoli	M212
058	 117 	058117	Ardea	M213
058	 118 	058118	Ciampino	M272
058	 119 	058119	San Cesareo	M295
058	 120 	058120	Fiumicino	M297
058	 122 	058122	Fonte Nuova	M309
059	 001 	059001	Aprilia	A341
059	 002 	059002	Bassiano	A707
059	 003 	059003	Campodimele	B527
059	 004 	059004	Castelforte	C104
059	 005 	059005	Cisterna di Latina	C740
059	 006 	059006	Cori	D003
059	 007 	059007	Fondi	D662
059	 008 	059008	Formia	D708
059	 009 	059009	Gaeta	D843
059	 010 	059010	Itri	E375
059	 011 	059011	Latina	E472
059	 012 	059012	Lenola	E527
059	 013 	059013	Maenza	E798
059	 014 	059014	Minturno	F224
059	 015 	059015	Monte San Biagio	F616
059	 016 	059016	Norma	F937
059	 017 	059017	Pontinia	G865
059	 018 	059018	Ponza	G871
059	 019 	059019	Priverno	G698
059	 020 	059020	Prossedi	H076
059	 021 	059021	Roccagorga	H413
059	 022 	059022	Rocca Massima	H421
059	 023 	059023	Roccasecca dei Volsci	H444
059	 024 	059024	Sabaudia	H647
059	 025 	059025	San Felice Circeo	H836
059	 026 	059026	Santi Cosma e Damiano	I339
059	 027 	059027	Sermoneta	I634
059	 028 	059028	Sezze	I712
059	 029 	059029	Sonnino	I832
059	 030 	059030	Sperlonga	I892
059	 031 	059031	Spigno Saturnia	I902
059	 032 	059032	Terracina	L120
059	 033 	059033	Ventotene	L742
060	 001 	060001	Acquafondata	A032
060	 002 	060002	Acuto	A054
060	 003 	060003	Alatri	A123
060	 004 	060004	Alvito	A244
060	 005 	060005	Amaseno	A256
060	 006 	060006	Anagni	A269
060	 007 	060007	Aquino	A348
060	 008 	060008	Arce	A363
060	 009 	060009	Arnara	A421
060	 010 	060010	Arpino	A433
060	 011 	060011	Atina	A486
060	 012 	060012	Ausonia	A502
060	 013 	060013	Belmonte Castello	A763
060	 014 	060014	Boville Ernica	A720
060	 015 	060015	Broccostella	B195
060	 016 	060016	Campoli Appennino	B543
060	 017 	060017	Casalattico	B862
060	 018 	060018	Casalvieri	B919
060	 019 	060019	Cassino	C034
060	 020 	060020	Castelliri	C177
060	 021 	060021	Castelnuovo Parano	C223
060	 022 	060022	Castrocielo	C340
060	 023 	060023	Castro dei Volsci	C338
060	 024 	060024	Ceccano	C413
060	 025 	060025	Ceprano	C479
060	 026 	060026	Cervaro	C545
060	 027 	060027	Colfelice	C836
060	 028 	060028	Collepardo	C864
060	 029 	060029	Colle San Magno	C870
060	 030 	060030	Coreno Ausonio	C998
060	 031 	060031	Esperia	D440
060	 032 	060032	Falvaterra	D483
060	 033 	060033	Ferentino	D539
060	 034 	060034	Filettino	D591
060	 035 	060035	Fiuggi	A310
060	 036 	060036	Fontana Liri	D667
060	 037 	060037	Fontechiari	D682
060	 038 	060038	Frosinone	D810
060	 039 	060039	Fumone	D819
060	 040 	060040	Gallinaro	D881
060	 041 	060041	Giuliano di Roma	E057
060	 042 	060042	Guarcino	E236
060	 043 	060043	Isola del Liri	E340
060	 044 	060044	Monte San Giovanni Campano	F620
060	 045 	060045	Morolo	F740
060	 046 	060046	Paliano	G276
060	 047 	060047	Pastena	G362
060	 048 	060048	Patrica	G374
060	 049 	060049	Pescosolido	G500
060	 050 	060050	Picinisco	G591
060	 051 	060051	Pico	G592
060	 052 	060052	Piedimonte San Germano	G598
060	 053 	060053	Piglio	G659
060	 054 	060054	Pignataro Interamna	G662
060	 055 	060055	Pofi	G749
060	 056 	060056	Pontecorvo	G838
060	 057 	060057	Posta Fibreno	G935
060	 058 	060058	Ripi	H324
060	 059 	060059	Rocca dArce	H393
060	 060 	060060	Roccasecca	H443
060	 061 	060061	San Biagio Saracinisco	H779
060	 062 	060062	San Donato Val di Comino	H824
060	 063 	060063	San Giorgio a Liri	H880
060	 064 	060064	San Giovanni Incarico	H917
060	 065 	060065	SantAmbrogio sul Garigliano	I256
060	 066 	060066	SantAndrea del Garigliano	I265
060	 067 	060067	SantApollinare	I302
060	 068 	060068	SantElia Fiumerapido	I321
060	 069 	060069	Santopadre	I351
060	 070 	060070	San Vittore del Lazio	I408
060	 071 	060071	Serrone	I669
060	 072 	060072	Settefrati	I697
060	 073 	060073	Sgurgola	I716
060	 074 	060074	Sora	I838
060	 075 	060075	Strangolagalli	I973
060	 076 	060076	Supino	L009
060	 077 	060077	Terelle	L105
060	 078 	060078	Torre Cajetani	L243
060	 079 	060079	Torrice	L290
060	 080 	060080	Trevi nel Lazio	L398
060	 081 	060081	Trivigliano	L437
060	 082 	060082	Vallecorsa	L598
060	 083 	060083	Vallemaio	L605
060	 084 	060084	Vallerotonda	L614
060	 085 	060085	Veroli	L780
060	 086 	060086	Vicalvi	L836
060	 087 	060087	Vico nel Lazio	L843
060	 088 	060088	Villa Latina	A081
060	 089 	060089	Villa Santa Lucia	L905
060	 090 	060090	Villa Santo Stefano	I364
060	 091 	060091	Viticuso	M083
061	 001 	061001	Ailano	A106
061	 002 	061002	Alife	A200
061	 003 	061003	Alvignano	A243
061	 004 	061004	Arienzo	A403
061	 005 	061005	Aversa	A512
061	 006 	061006	Baia e Latina	A579
061	 007 	061007	Bellona	A755
061	 008 	061008	Caianello	B361
061	 009 	061009	Caiazzo	B362
061	 010 	061010	Calvi Risorta	B445
061	 011 	061011	Camigliano	B477
061	 012 	061012	Cancello ed Arnone	B581
061	 013 	061013	Capodrise	B667
061	 014 	061014	Capriati a Volturno	B704
061	 015 	061015	Capua	B715
061	 016 	061016	Carinaro	B779
061	 017 	061017	Carinola	B781
061	 018 	061018	Casagiove	B860
061	 019 	061019	Casal di Principe	B872
061	 020 	061020	Casaluce	B916
061	 021 	061021	Casapulla	B935
061	 022 	061022	Caserta	B963
061	 023 	061023	Castel Campagnano	B494
061	 024 	061024	Castel di Sasso	C097
061	 025 	061025	Castello del Matese	C178
061	 026 	061026	Castel Morrone	C211
061	 027 	061027	Castel Volturno	C291
061	 028 	061028	Cervino	C558
061	 029 	061029	Cesa	C561
061	 030 	061030	Ciorlano	C716
061	 031 	061031	Conca della Campania	C939
061	 032 	061032	Curti	D228
061	 033 	061033	Dragoni	D361
061	 034 	061034	Fontegreca	D683
061	 035 	061035	Formicola	D709
061	 036 	061036	Francolise	D769
061	 037 	061037	Frignano	D799
061	 038 	061038	Gallo Matese	D884
061	 039 	061039	Galluccio	D886
061	 040 	061040	Giano Vetusto	E011
061	 041 	061041	Gioia Sannitica	E039
061	 042 	061042	Grazzanise	E158
061	 043 	061043	Gricignano di Aversa	E173
061	 044 	061044	Letino	E554
061	 045 	061045	Liberi	E570
061	 046 	061046	Lusciano	E754
061	 047 	061047	Macerata Campania	E784
061	 048 	061048	Maddaloni	E791
061	 049 	061049	Marcianise	E932
061	 050 	061050	Marzano Appio	E998
061	 051 	061051	Mignano Monte Lungo	F203
061	 052 	061052	Mondragone	F352
061	 053 	061053	Orta di Atella	G130
061	 054 	061054	Parete	G333
061	 055 	061055	Pastorano	G364
061	 056 	061056	Piana di Monte Verna	G541
061	 057 	061057	Piedimonte Matese	G596
061	 058 	061058	Pietramelara	G620
061	 059 	061059	Pietravairano	G630
061	 060 	061060	Pignataro Maggiore	G661
061	 061 	061061	Pontelatone	G849
061	 062 	061062	Portico di Caserta	G903
061	 063 	061063	Prata Sannita	G991
061	 064 	061064	Pratella	G995
061	 065 	061065	Presenzano	H045
061	 066 	061066	Raviscanina	H202
061	 067 	061067	Recale	H210
061	 068 	061068	Riardo	H268
061	 069 	061069	Rocca dEvandro	H398
061	 070 	061070	Roccamonfina	H423
061	 071 	061071	Roccaromana	H436
061	 072 	061072	Rocchetta e Croce	H459
061	 073 	061073	Ruviano	H165
061	 074 	061074	San Cipriano dAversa	H798
061	 075 	061075	San Felice a Cancello	H834
061	 076 	061076	San Gregorio Matese	H939
061	 077 	061077	San Marcellino	H978
061	 078 	061078	San Nicola la Strada	I056
061	 079 	061079	San Pietro Infine	I113
061	 080 	061080	San Potito Sannitico	I130
061	 081 	061081	San Prisco	I131
061	 082 	061082	Santa Maria a Vico	I233
061	 083 	061083	Santa Maria Capua Vetere	I234
061	 084 	061084	Santa Maria la Fossa	I247
061	 085 	061085	San Tammaro	I261
061	 086 	061086	SantAngelo dAlife	I273
061	 087 	061087	SantArpino	I306
061	 088 	061088	Sessa Aurunca	I676
061	 089 	061089	Sparanise	I885
061	 090 	061090	Succivo	I993
061	 091 	061091	Teano	L083
061	 092 	061092	Teverola	L155
061	 093 	061093	Tora e Piccilli	L205
061	 094 	061094	Trentola-Ducenta	L379
061	 095 	061095	Vairano Patenora	L540
061	 096 	061096	Valle Agricola	L594
061	 097 	061097	Valle di Maddaloni	L591
061	 098 	061098	Villa di Briano	D801
061	 099 	061099	Villa Literno	L844
061	 100 	061100	Vitulazio	M092
061	 101 	061101	Falciano del Massico	D471
061	 102 	061102	Cellole	M262
061	 103 	061103	Casapesenna	M260
061	 104 	061104	San Marco Evangelista	F043
062	 001 	062001	Airola	A110
062	 002 	062002	Amorosi	A265
062	 003 	062003	Apice	A328
062	 004 	062004	Apollosa	A330
062	 005 	062005	Arpaia	A431
062	 006 	062006	Arpaise	A432
062	 007 	062007	Baselice	A696
062	 008 	062008	Benevento	A783
062	 009 	062009	Bonea	A970
062	 010 	062010	Bucciano	B239
062	 011 	062011	Buonalbergo	B267
062	 012 	062012	Calvi	B444
062	 013 	062013	Campolattaro	B541
062	 014 	062014	Campoli del Monte Taburno	B542
062	 015 	062015	Casalduni	B873
062	 016 	062016	Castelfranco in Miscano	C106
062	 017 	062017	Castelpagano	C245
062	 018 	062018	Castelpoto	C250
062	 019 	062019	Castelvenere	C280
062	 020 	062020	Castelvetere in Val Fortore	C284
062	 021 	062021	Cautano	C359
062	 022 	062022	Ceppaloni	C476
062	 023 	062023	Cerreto Sannita	C525
062	 024 	062024	Circello	C719
062	 025 	062025	Colle Sannita	C846
062	 026 	062026	Cusano Mutri	D230
062	 027 	062027	Dugenta	D380
062	 028 	062028	Durazzano	D386
062	 029 	062029	Faicchio	D469
062	 030 	062030	Foglianise	D644
062	 031 	062031	Foiano di Val Fortore	D650
062	 032 	062032	Forchia	D693
062	 033 	062033	Fragneto lAbate	D755
062	 034 	062034	Fragneto Monforte	D756
062	 035 	062035	Frasso Telesino	D784
062	 036 	062036	Ginestra degli Schiavoni	E034
062	 037 	062037	Guardia Sanframondi	E249
062	 038 	062038	Limatola	E589
062	 039 	062039	Melizzano	F113
062	 040 	062040	Moiano	F274
062	 041 	062041	Molinara	F287
062	 042 	062042	Montefalcone di Val Fortore	F494
062	 043 	062043	Montesarchio	F636
062	 044 	062044	Morcone	F717
062	 045 	062045	Paduli	G227
062	 046 	062046	Pago Veiano	G243
062	 047 	062047	Pannarano	G311
062	 048 	062048	Paolisi	G318
062	 049 	062049	Paupisi	G386
062	 050 	062050	Pesco Sannita	G494
062	 051 	062051	Pietraroja	G626
062	 052 	062052	Pietrelcina	G631
062	 053 	062053	Ponte	G827
062	 054 	062054	Pontelandolfo	G848
062	 055 	062055	Puglianello	H087
062	 056 	062056	Reino	H227
062	 057 	062057	San Bartolomeo in Galdo	H764
062	 058 	062058	San Giorgio del Sannio	H894
062	 059 	062059	San Giorgio La Molara	H898
062	 060 	062060	San Leucio del Sannio	H953
062	 061 	062061	San Lorenzello	H955
062	 062 	062062	San Lorenzo Maggiore	H967
062	 063 	062063	San Lupo	H973
062	 064 	062064	San Marco dei Cavoti	H984
062	 065 	062065	San Martino Sannita	I002
062	 066 	062066	San Nazzaro	I049
062	 067 	062067	San Nicola Manfredi	I062
062	 068 	062068	San Salvatore Telesino	I145
062	 069 	062069	Santa Croce del Sannio	I179
062	 070 	062070	SantAgata de Goti	I197
062	 071 	062071	SantAngelo a Cupolo	I277
062	 072 	062072	Sassinoro	I455
062	 073 	062073	Solopaca	I809
062	 074 	062074	Telese Terme	L086
062	 075 	062075	Tocco Caudio	L185
062	 076 	062076	Torrecuso	L254
062	 077 	062077	Vitulano	M093
062	 078 	062078	SantArcangelo Trimonte	F557
063	 001 	063001	Acerra	A024
063	 002 	063002	Afragola	A064
063	 003 	063003	Agerola	A068
063	 004 	063004	Anacapri	A268
063	 005 	063005	Arzano	A455
063	 006 	063006	Bacoli	A535
063	 007 	063007	Barano dIschia	A617
063	 008 	063008	Boscoreale	B076
063	 009 	063009	Boscotrecase	B077
063	 010 	063010	Brusciano	B227
063	 011 	063011	Caivano	B371
063	 012 	063012	Calvizzano	B452
063	 013 	063013	Camposano	B565
063	 014 	063014	Capri	B696
063	 015 	063015	Carbonara di Nola	B740
063	 016 	063016	Cardito	B759
063	 017 	063017	Casalnuovo di Napoli	B905
063	 018 	063018	Casamarciano	B922
063	 019 	063019	Casamicciola Terme	B924
063	 020 	063020	Casandrino	B925
063	 021 	063021	Casavatore	B946
063	 022 	063022	Casola di Napoli	B980
063	 023 	063023	Casoria	B990
063	 024 	063024	Castellammare di Stabia	C129
063	 025 	063025	Castello di Cisterna	C188
063	 026 	063026	Cercola	C495
063	 027 	063027	Cicciano	C675
063	 028 	063028	Cimitile	C697
063	 029 	063029	Comiziano	C929
063	 030 	063030	Crispano	D170
063	 031 	063031	Forio	D702
063	 032 	063032	Frattamaggiore	D789
063	 033 	063033	Frattaminore	D790
063	 034 	063034	Giugliano in Campania	E054
063	 035 	063035	Gragnano	E131
063	 036 	063036	Grumo Nevano	E224
063	 037 	063037	Ischia	E329
063	 038 	063038	Lacco Ameno	E396
063	 039 	063039	Lettere	E557
063	 040 	063040	Liveri	E620
063	 041 	063041	Marano di Napoli	E906
063	 042 	063042	Mariglianella	E954
063	 043 	063043	Marigliano	E955
063	 044 	063044	Massa Lubrense	F030
063	 045 	063045	Melito di Napoli	F111
063	 046 	063046	Meta	F162
063	 047 	063047	Monte di Procida	F488
063	 048 	063048	Mugnano di Napoli	F799
063	 049 	063049	Napoli	F839
063	 050 	063050	Nola	F924
063	 051 	063051	Ottaviano	G190
063	 052 	063052	Palma Campania	G283
063	 053 	063053	Piano di Sorrento	G568
063	 054 	063054	Pimonte	G670
063	 055 	063055	Poggiomarino	G762
063	 056 	063056	Pollena Trocchia	G795
063	 057 	063057	Pomigliano dArco	G812
063	 058 	063058	Pompei	G813
063	 059 	063059	Portici	G902
063	 060 	063060	Pozzuoli	G964
063	 061 	063061	Procida	H072
063	 062 	063062	Qualiano	H101
063	 063 	063063	Quarto	H114
063	 064 	063064	Ercolano	H243
063	 065 	063065	Roccarainola	H433
063	 066 	063066	San Gennaro Vesuviano	H860
063	 067 	063067	San Giorgio a Cremano	H892
063	 068 	063068	San Giuseppe Vesuviano	H931
063	 069 	063069	San Paolo Bel Sito	I073
063	 070 	063070	San Sebastiano al Vesuvio	I151
063	 071 	063071	SantAgnello	I208
063	 072 	063072	SantAnastasia	I262
063	 073 	063073	SantAntimo	I293
063	 074 	063074	SantAntonio Abate	I300
063	 075 	063075	San Vitaliano	I391
063	 076 	063076	Saviano	I469
063	 077 	063077	Scisciano	I540
063	 078 	063078	Serrara Fontana	I652
063	 079 	063079	Somma Vesuviana	I820
063	 080 	063080	Sorrento	I862
063	 081 	063081	Striano	I978
063	 082 	063082	Terzigno	L142
063	 083 	063083	Torre Annunziata	L245
063	 084 	063084	Torre del Greco	L259
063	 085 	063085	Tufino	L460
063	 086 	063086	Vico Equense	L845
063	 087 	063087	Villaricca	G309
063	 088 	063088	Visciano	M072
063	 089 	063089	Volla	M115
063	 090 	063090	Santa Maria la CaritÔøΩ	M273
063	 091 	063091	Trecase	M280
063	 092 	063092	Massa di Somma	M289
064	 001 	064001	Aiello del Sabato	A101
064	 002 	064002	Altavilla Irpina	A228
064	 003 	064003	Andretta	A284
064	 004 	064004	Aquilonia	A347
064	 005 	064005	Ariano Irpino	A399
064	 006 	064006	Atripalda	A489
064	 007 	064007	Avella	A508
064	 008 	064008	Avellino	A509
064	 009 	064009	Bagnoli Irpino	A566
064	 010 	064010	Baiano	A580
064	 011 	064011	Bisaccia	A881
064	 012 	064012	Bonito	A975
064	 013 	064013	Cairano	B367
064	 014 	064014	Calabritto	B374
064	 015 	064015	Calitri	B415
064	 016 	064016	Candida	B590
064	 017 	064017	Caposele	B674
064	 018 	064018	Capriglia Irpina	B706
064	 019 	064019	Carife	B776
064	 020 	064020	Casalbore	B866
064	 021 	064021	Cassano Irpino	B997
064	 022 	064022	Castel Baronia	C058
064	 023 	064023	Castelfranci	C105
064	 024 	064024	Castelvetere sul Calore	C283
064	 025 	064025	Cervinara	C557
064	 026 	064026	Cesinali	C576
064	 027 	064027	Chianche	C606
064	 028 	064028	Chiusano di San Domenico	C659
064	 029 	064029	Contrada	C971
064	 030 	064030	Conza della Campania	C976
064	 031 	064031	Domicella	D331
064	 032 	064032	Flumeri	D638
064	 033 	064033	Fontanarosa	D671
064	 034 	064034	Forino	D701
064	 035 	064035	Frigento	D798
064	 036 	064036	Gesualdo	D998
064	 037 	064037	Greci	E161
064	 038 	064038	Grottaminarda	E206
064	 039 	064039	Grottolella	E214
064	 040 	064040	Guardia Lombardi	E245
064	 041 	064041	Lacedonia	E397
064	 042 	064042	Lapio	E448
064	 043 	064043	Lauro	E487
064	 044 	064044	Lioni	E605
064	 045 	064045	Luogosano	E746
064	 046 	064046	Manocalzati	E891
064	 047 	064047	Marzano di Nola	E997
064	 048 	064048	Melito Irpino	F110
064	 049 	064049	Mercogliano	F141
064	 050 	064050	Mirabella Eclano	F230
064	 051 	064051	Montaguto	F397
064	 052 	064052	Montecalvo Irpino	F448
064	 053 	064053	Montefalcione	F491
064	 054 	064054	Monteforte Irpino	F506
064	 055 	064055	Montefredane	F511
064	 056 	064056	Montefusco	F512
064	 057 	064057	Montella	F546
064	 058 	064058	Montemarano	F559
064	 059 	064059	Montemiletto	F566
064	 060 	064060	Monteverde	F660
064	 061 	064061	Montoro Inferiore	F693
064	 062 	064062	Montoro Superiore	F694
064	 063 	064063	Morra De Sanctis	F744
064	 064 	064064	Moschiano	F762
064	 065 	064065	Mugnano del Cardinale	F798
064	 066 	064066	Nusco	F988
064	 067 	064067	Ospedaletto dAlpinolo	G165
064	 068 	064068	Pago del Vallo di Lauro	G242
064	 069 	064069	Parolise	G340
064	 070 	064070	Paternopoli	G370
064	 071 	064071	Petruro Irpino	G519
064	 072 	064072	Pietradefusi	G611
064	 073 	064073	Pietrastornina	G629
064	 074 	064074	Prata di Principato Ultra	G990
064	 075 	064075	Pratola Serra	H006
064	 076 	064076	Quadrelle	H097
064	 077 	064077	Quindici	H128
064	 078 	064078	Roccabascerana	H382
064	 079 	064079	Rocca San Felice	H438
064	 080 	064080	Rotondi	H592
064	 081 	064081	Salza Irpina	H733
064	 082 	064082	San Mango sul Calore	H975
064	 083 	064083	San Martino Valle Caudina	I016
064	 084 	064084	San Michele di Serino	I034
064	 085 	064085	San Nicola Baronia	I061
064	 086 	064086	San Potito Ultra	I129
064	 087 	064087	San Sossio Baronia	I163
064	 088 	064088	Santa Lucia di Serino	I219
064	 089 	064089	SantAndrea di Conza	I264
064	 090 	064090	SantAngelo allEsca	I279
064	 091 	064091	SantAngelo a Scala	I280
064	 092 	064092	SantAngelo dei Lombardi	I281
064	 093 	064093	Santa Paolina	I301
064	 095 	064095	Santo Stefano del Sole	I357
064	 096 	064096	Savignano Irpino	I471
064	 097 	064097	Scampitella	I493
064	 098 	064098	Senerchia	I606
064	 099 	064099	Serino	I630
064	 100 	064100	Sirignano	I756
064	 101 	064101	Solofra	I805
064	 102 	064102	Sorbo Serpico	I843
064	 103 	064103	Sperone	I893
064	 104 	064104	Sturno	I990
064	 105 	064105	Summonte	L004
064	 106 	064106	Taurano	L061
064	 107 	064107	Taurasi	L062
064	 108 	064108	Teora	L102
064	 109 	064109	Torella dei Lombardi	L214
064	 110 	064110	Torre Le Nocelle	L272
064	 111 	064111	Torrioni	L301
064	 112 	064112	Trevico	L399
064	 113 	064113	Tufo	L461
064	 114 	064114	Vallata	L589
064	 115 	064115	Vallesaccarda	L616
064	 116 	064116	Venticano	L739
064	 117 	064117	Villamaina	L965
064	 118 	064118	Villanova del Battista	L973
064	 119 	064119	Volturara Irpina	M130
064	 120 	064120	Zungoli	M203
065	 001 	065001	Acerno	A023
065	 002 	065002	Agropoli	A091
065	 003 	065003	Albanella	A128
065	 004 	065004	Alfano	A186
065	 005 	065005	Altavilla Silentina	A230
065	 006 	065006	Amalfi	A251
065	 007 	065007	Angri	A294
065	 008 	065008	Aquara	A343
065	 009 	065009	Ascea	A460
065	 010 	065010	Atena Lucana	A484
065	 011 	065011	Atrani	A487
065	 012 	065012	Auletta	A495
065	 013 	065013	Baronissi	A674
065	 014 	065014	Battipaglia	A717
065	 015 	065015	Bellosguardo	A756
065	 016 	065016	Bracigliano	B115
065	 017 	065017	Buccino	B242
065	 018 	065018	Buonabitacolo	B266
065	 019 	065019	Caggiano	B351
065	 020 	065020	Calvanico	B437
065	 021 	065021	Camerota	B476
065	 022 	065022	Campagna	B492
065	 023 	065023	Campora	B555
065	 024 	065024	Cannalonga	B608
065	 025 	065025	Capaccio	B644
065	 026 	065026	Casalbuono	B868
065	 027 	065027	Casaletto Spartano	B888
065	 028 	065028	Casal Velino	B895
065	 029 	065029	Caselle in Pittari	B959
065	 030 	065030	Castelcivita	C069
065	 031 	065031	Castellabate	C125
065	 032 	065032	Castelnuovo Cilento	C231
065	 033 	065033	Castelnuovo di Conza	C235
065	 034 	065034	Castel San Giorgio	C259
065	 035 	065035	Castel San Lorenzo	C262
065	 036 	065036	Castiglione del Genovesi	C306
065	 037 	065037	Cava de Tirreni	C361
065	 038 	065038	Celle di Bulgheria	C444
065	 039 	065039	Centola	C470
065	 040 	065040	Ceraso	C485
065	 041 	065041	Cetara	C584
065	 042 	065042	Cicerale	C676
065	 043 	065043	Colliano	C879
065	 044 	065044	Conca dei Marini	C940
065	 045 	065045	Controne	C973
065	 046 	065046	Contursi Terme	C974
065	 047 	065047	Corbara	C984
065	 048 	065048	Corleto Monforte	D011
065	 049 	065049	Cuccaro Vetere	D195
065	 050 	065050	Eboli	D390
065	 051 	065051	Felitto	D527
065	 052 	065052	Fisciano	D615
065	 053 	065053	Furore	D826
065	 054 	065054	Futani	D832
065	 055 	065055	Giffoni Sei Casali	E026
065	 056 	065056	Giffoni Valle Piana	E027
065	 057 	065057	Gioi	E037
065	 058 	065058	Giungano	E060
065	 059 	065059	Ispani	E365
065	 060 	065060	Laureana Cilento	E480
065	 061 	065061	Laurino	E485
065	 062 	065062	Laurito	E486
065	 063 	065063	Laviano	E498
065	 064 	065064	Lustra	E767
065	 065 	065065	Magliano Vetere	E814
065	 066 	065066	Maiori	E839
065	 067 	065067	Mercato San Severino	F138
065	 068 	065068	Minori	F223
065	 069 	065069	Moio della Civitella	F278
065	 070 	065070	Montano Antilia	F426
065	 071 	065071	Montecorice	F479
065	 072 	065072	Montecorvino Pugliano	F480
065	 073 	065073	Montecorvino Rovella	F481
065	 074 	065074	Monteforte Cilento	F507
065	 075 	065075	Monte San Giacomo	F618
065	 076 	065076	Montesano sulla Marcellana	F625
065	 077 	065077	Morigerati	F731
065	 078 	065078	Nocera Inferiore	F912
065	 079 	065079	Nocera Superiore	F913
065	 080 	065080	Novi Velia	F967
065	 081 	065081	Ogliastro Cilento	G011
065	 082 	065082	Olevano sul Tusciano	G023
065	 083 	065083	Oliveto Citra	G039
065	 084 	065084	Omignano	G063
065	 085 	065085	Orria	G121
065	 086 	065086	Ottati	G192
065	 087 	065087	Padula	G226
065	 088 	065088	Pagani	G230
065	 089 	065089	Palomonte	G292
065	 090 	065090	Pellezzano	G426
065	 091 	065091	Perdifumo	G447
065	 092 	065092	Perito	G455
065	 093 	065093	Pertosa	G476
065	 094 	065094	Petina	G509
065	 095 	065095	Piaggine	G538
065	 096 	065096	Pisciotta	G707
065	 097 	065097	Polla	G793
065	 098 	065098	Pollica	G796
065	 099 	065099	Pontecagnano Faiano	G834
065	 100 	065100	Positano	G932
065	 101 	065101	Postiglione	G939
065	 102 	065102	Praiano	G976
065	 103 	065103	Prignano Cilento	H062
065	 104 	065104	Ravello	H198
065	 105 	065105	Ricigliano	H277
065	 106 	065106	Roccadaspide	H394
065	 107 	065107	Roccagloriosa	H412
065	 108 	065108	Roccapiemonte	H431
065	 109 	065109	Rofrano	H485
065	 110 	065110	Romagnano al Monte	H503
065	 111 	065111	Roscigno	H564
065	 112 	065112	Rutino	H644
065	 113 	065113	Sacco	H654
065	 114 	065114	Sala Consilina	H683
065	 115 	065115	Salento	H686
065	 116 	065116	Salerno	H703
065	 117 	065117	Salvitelle	H732
065	 118 	065118	San Cipriano Picentino	H800
065	 119 	065119	San Giovanni a Piro	H907
065	 120 	065120	San Gregorio Magno	H943
065	 121 	065121	San Mango Piemonte	H977
065	 122 	065122	San Marzano sul Sarno	I019
065	 123 	065123	San Mauro Cilento	I031
065	 124 	065124	San Mauro la Bruca	I032
065	 125 	065125	San Pietro al Tanagro	I089
065	 126 	065126	San Rufo	I143
065	 127 	065127	Santa Marina	I253
065	 128 	065128	SantAngelo a Fasanella	I278
065	 129 	065129	SantArsenio	I307
065	 130 	065130	SantEgidio del Monte Albino	I317
065	 131 	065131	Santomenna	I260
065	 132 	065132	San Valentino Torio	I377
065	 133 	065133	Sanza	I410
065	 134 	065134	Sapri	I422
065	 135 	065135	Sarno	I438
065	 136 	065136	Sassano	I451
065	 137 	065137	Scafati	I483
065	 138 	065138	Scala	I486
065	 139 	065139	Serramezzana	I648
065	 140 	065140	Serre	I666
065	 141 	065141	Sessa Cilento	I677
065	 142 	065142	Siano	I720
065	 143 	065143	Sicignano degli Alburni	M253
065	 144 	065144	Stella Cilento	G887
065	 145 	065145	Stio	I960
065	 146 	065146	Teggiano	D292
065	 147 	065147	Torchiara	L212
065	 148 	065148	Torraca	L233
065	 149 	065149	Torre Orsaia	L274
065	 150 	065150	Tortorella	L306
065	 151 	065151	Tramonti	L323
065	 152 	065152	Trentinara	L377
065	 153 	065153	Valle dellAngelo	G540
065	 154 	065154	Vallo della Lucania	L628
065	 155 	065155	Valva	L656
065	 156 	065156	Vibonati	L835
065	 157 	065157	Vietri sul Mare	L860
065	 158 	065158	Bellizzi	M294
066	 001 	066001	Acciano	A018
066	 002 	066002	Aielli	A100
066	 003 	066003	Alfedena	A187
066	 004 	066004	Anversa degli Abruzzi	A318
066	 005 	066005	Ateleta	A481
066	 006 	066006	Avezzano	A515
066	 007 	066007	Balsorano	A603
066	 008 	066008	Barete	A656
066	 009 	066009	Barisciano	A667
066	 010 	066010	Barrea	A678
066	 011 	066011	Bisegna	A884
066	 012 	066012	Bugnara	B256
066	 013 	066013	Cagnano Amiterno	B358
066	 014 	066014	Calascio	B382
066	 015 	066015	Campo di Giove	B526
066	 016 	066016	Campotosto	B569
066	 017 	066017	Canistro	B606
066	 018 	066018	Cansano	B624
066	 019 	066019	Capestrano	B651
066	 020 	066020	Capistrello	B656
066	 021 	066021	Capitignano	B658
066	 022 	066022	Caporciano	B672
066	 023 	066023	Cappadocia	B677
066	 024 	066024	Carapelle Calvisio	B725
066	 025 	066025	Carsoli	B842
066	 026 	066026	Castel del Monte	C083
066	 027 	066027	Castel di Ieri	C090
066	 028 	066028	Castel di Sangro	C096
066	 029 	066029	Castellafiume	C126
066	 030 	066030	Castelvecchio Calvisio	C278
066	 031 	066031	Castelvecchio Subequo	C279
066	 032 	066032	Celano	C426
066	 033 	066033	Cerchio	C492
066	 034 	066034	Civita dAntino	C766
066	 035 	066035	Civitella Alfedena	C778
066	 036 	066036	Civitella Roveto	C783
066	 037 	066037	Cocullo	C811
066	 038 	066038	Collarmele	C844
066	 039 	066039	Collelongo	C862
066	 040 	066040	Collepietro	C866
066	 041 	066041	Corfinio	C999
066	 042 	066042	Fagnano Alto	D465
066	 043 	066043	Fontecchio	D681
066	 044 	066044	Fossa	D736
066	 045 	066045	Gagliano Aterno	D850
066	 046 	066046	Gioia dei Marsi	E040
066	 047 	066047	Goriano Sicoli	E096
066	 048 	066048	Introdacqua	E307
066	 049 	066049	LAquila	A345
066	 050 	066050	Lecce nei Marsi	E505
066	 051 	066051	Luco dei Marsi	E723
066	 052 	066052	Lucoli	E724
066	 053 	066053	Magliano de Marsi	E811
066	 054 	066054	Massa dAlbe	F022
066	 055 	066055	Molina Aterno	M255
066	 056 	066056	Montereale	F595
066	 057 	066057	Morino	F732
066	 058 	066058	Navelli	F852
066	 059 	066059	Ocre	F996
066	 060 	066060	Ofena	G002
066	 061 	066061	Opi	G079
066	 062 	066062	Oricola	G102
066	 063 	066063	Ortona dei Marsi	G142
066	 064 	066064	Ortucchio	G145
066	 065 	066065	Ovindoli	G200
066	 066 	066066	Pacentro	G210
066	 067 	066067	Pereto	G449
066	 068 	066068	Pescasseroli	G484
066	 069 	066069	Pescina	G492
066	 070 	066070	Pescocostanzo	G493
066	 071 	066071	Pettorano sul Gizio	G524
066	 072 	066072	Pizzoli	G726
066	 073 	066073	Poggio Picenze	G766
066	 074 	066074	Prata dAnsidonia	G992
066	 075 	066075	Pratola Peligna	H007
066	 076 	066076	Prezza	H056
066	 077 	066077	Raiano	H166
066	 078 	066078	Rivisondoli	H353
066	 079 	066079	Roccacasale	H389
066	 080 	066080	Rocca di Botte	H399
066	 081 	066081	Rocca di Cambio	H400
066	 082 	066082	Rocca di Mezzo	H402
066	 083 	066083	Rocca Pia	H429
066	 084 	066084	Roccaraso	H434
066	 085 	066085	San Benedetto dei Marsi	H772
066	 086 	066086	San Benedetto in Perillis	H773
066	 087 	066087	San Demetrio ne Vestini	H819
066	 088 	066088	San Pio delle Camere	I121
066	 089 	066089	Sante Marie	I326
066	 090 	066090	SantEusanio Forconese	I336
066	 091 	066091	Santo Stefano di Sessanio	I360
066	 092 	066092	San Vincenzo Valle Roveto	I389
066	 093 	066093	Scanno	I501
066	 094 	066094	Scontrone	I543
066	 095 	066095	Scoppito	I546
066	 096 	066096	Scurcola Marsicana	I553
066	 097 	066097	Secinaro	I558
066	 098 	066098	Sulmona	I804
066	 099 	066099	Tagliacozzo	L025
066	 100 	066100	Tione degli Abruzzi	L173
066	 101 	066101	Tornimparte	L227
066	 102 	066102	Trasacco	L334
066	 103 	066103	Villalago	L958
066	 104 	066104	Villa Santa Lucia degli Abruzzi	M021
066	 105 	066105	Villa SantAngelo	M023
066	 106 	066106	Villavallelonga	M031
066	 107 	066107	Villetta Barrea	M041
066	 108 	066108	Vittorito	M090
067	 001 	067001	Alba Adriatica	A125
067	 002 	067002	Ancarano	A270
067	 003 	067003	Arsita	A445
067	 004 	067004	Atri	A488
067	 005 	067005	Basciano	A692
067	 006 	067006	Bellante	A746
067	 007 	067007	Bisenti	A885
067	 008 	067008	Campli	B515
067	 009 	067009	Canzano	B640
067	 010 	067010	Castel Castagna	C040
067	 011 	067011	Castellalto	C128
067	 012 	067012	Castelli	C169
067	 013 	067013	Castiglione Messer Raimondo	C316
067	 014 	067014	Castilenti	C322
067	 015 	067015	Cellino Attanasio	C449
067	 016 	067016	Cermignano	C517
067	 017 	067017	Civitella del Tronto	C781
067	 018 	067018	Colledara	C311
067	 019 	067019	Colonnella	C901
067	 020 	067020	Controguerra	C972
067	 021 	067021	Corropoli	D043
067	 022 	067022	Cortino	D076
067	 023 	067023	Crognaleto	D179
067	 024 	067024	Fano Adriano	D489
067	 025 	067025	Giulianova	E058
067	 026 	067026	Isola del Gran Sasso dItalia	E343
067	 027 	067027	Montefino	F500
067	 028 	067028	Montorio al Vomano	F690
067	 029 	067029	Morro dOro	F747
067	 030 	067030	Mosciano SantAngelo	F764
067	 031 	067031	Nereto	F870
067	 032 	067032	Notaresco	F942
067	 033 	067033	Penna SantAndrea	G437
067	 034 	067034	Pietracamela	G608
067	 035 	067035	Pineto	F831
067	 036 	067036	Rocca Santa Maria	H440
067	 037 	067037	Roseto degli Abruzzi	F585
067	 038 	067038	SantEgidio alla Vibrata	I318
067	 039 	067039	SantOmero	I348
067	 040 	067040	Silvi	I741
067	 041 	067041	Teramo	L103
067	 042 	067042	Torano Nuovo	L207
067	 043 	067043	Torricella Sicura	L295
067	 044 	067044	Tortoreto	L307
067	 045 	067045	Tossicia	L314
067	 046 	067046	Valle Castellana	L597
067	 047 	067047	Martinsicuro	E989
068	 001 	068001	Abbateggio	A008
068	 002 	068002	Alanno	A120
068	 003 	068003	Bolognano	A945
068	 004 	068004	Brittoli	B193
068	 005 	068005	Bussi sul Tirino	B294
068	 006 	068006	Cappelle sul Tavo	B681
068	 007 	068007	Caramanico Terme	B722
068	 008 	068008	Carpineto della Nora	B827
068	 009 	068009	Castiglione a Casauria	C308
068	 010 	068010	Catignano	C354
068	 011 	068011	Cepagatti	C474
068	 012 	068012	CittÔøΩ SantAngelo	C750
068	 013 	068013	Civitaquana	C771
068	 014 	068014	Civitella Casanova	C779
068	 015 	068015	Collecorvino	C853
068	 016 	068016	Corvara	D078
068	 017 	068017	Cugnoli	D201
068	 018 	068018	Elice	D394
068	 019 	068019	Farindola	D501
068	 020 	068020	Lettomanoppello	E558
068	 021 	068021	Loreto Aprutino	E691
068	 022 	068022	Manoppello	E892
068	 023 	068023	Montebello di Bertona	F441
068	 024 	068024	Montesilvano	F646
068	 025 	068025	Moscufo	F765
068	 026 	068026	Nocciano	F908
068	 027 	068027	Penne	G438
068	 028 	068028	Pescara	G482
068	 029 	068029	Pescosansonesco	G499
068	 030 	068030	Pianella	G555
068	 031 	068031	Picciano	G589
068	 032 	068032	Pietranico	G621
068	 033 	068033	Popoli	G878
068	 034 	068034	Roccamorice	H425
068	 035 	068035	Rosciano	H562
068	 036 	068036	Salle	H715
068	 037 	068037	SantEufemia a Maiella	I332
068	 038 	068038	San Valentino in Abruzzo Citeriore	I376
068	 039 	068039	Scafa	I482
068	 040 	068040	Serramonacesca	I649
068	 041 	068041	Spoltore	I922
068	 042 	068042	Tocco da Casauria	L186
068	 043 	068043	Torre de Passeri	L263
068	 044 	068044	Turrivalignani	L475
068	 045 	068045	Vicoli	L846
068	 046 	068046	Villa Celiera	L922
069	 001 	069001	Altino	A235
069	 002 	069002	Archi	A367
069	 003 	069003	Ari	A398
069	 004 	069004	Arielli	A402
069	 005 	069005	Atessa	A485
069	 006 	069006	Bomba	A956
069	 007 	069007	Borrello	B057
069	 008 	069008	Bucchianico	B238
069	 009 	069009	Montebello sul Sangro	B268
069	 010 	069010	Canosa Sannita	B620
069	 011 	069011	Carpineto Sinello	B826
069	 012 	069012	Carunchio	B853
069	 013 	069013	Casacanditella	B859
069	 014 	069014	Casalanguida	B861
069	 015 	069015	Casalbordino	B865
069	 016 	069016	Casalincontrada	B896
069	 017 	069017	Casoli	B985
069	 018 	069018	Castel Frentano	C114
069	 019 	069019	Castelguidone	C123
069	 020 	069020	Castiglione Messer Marino	C298
069	 021 	069021	Celenza sul Trigno	C428
069	 022 	069022	Chieti	C632
069	 023 	069023	Civitaluparella	C768
069	 024 	069024	Civitella Messer Raimondo	C776
069	 025 	069025	Colledimacine	C855
069	 026 	069026	Colledimezzo	C856
069	 027 	069027	Crecchio	D137
069	 028 	069028	Cupello	D209
069	 029 	069029	Dogliola	D315
069	 030 	069030	Fara Filiorum Petri	D494
069	 031 	069031	Fara San Martino	D495
069	 032 	069032	Filetto	D592
069	 033 	069033	Fossacesia	D738
069	 034 	069034	Fraine	D757
069	 035 	069035	Francavilla al Mare	D763
069	 036 	069036	Fresagrandinaria	D796
069	 037 	069037	Frisa	D803
069	 038 	069038	Furci	D823
069	 039 	069039	Gamberale	D898
069	 040 	069040	Gessopalena	D996
069	 041 	069041	Gissi	E052
069	 042 	069042	Giuliano Teatino	E056
069	 043 	069043	Guardiagrele	E243
069	 044 	069044	Guilmi	E266
069	 045 	069045	Lama dei Peligni	E424
069	 046 	069046	Lanciano	E435
069	 047 	069047	Lentella	E531
069	 048 	069048	Lettopalena	E559
069	 049 	069049	Liscia	E611
069	 050 	069050	Miglianico	F196
069	 051 	069051	Montazzoli	F433
069	 052 	069052	Monteferrante	F498
069	 053 	069053	Montelapiano	F535
069	 054 	069054	Montenerodomo	F578
069	 055 	069055	Monteodorisio	F582
069	 056 	069056	Mozzagrogna	F785
069	 057 	069057	Orsogna	G128
069	 058 	069058	Ortona	G141
069	 059 	069059	Paglieta	G237
069	 060 	069060	Palena	G271
069	 061 	069061	Palmoli	G290
069	 062 	069062	Palombaro	G294
069	 063 	069063	Pennadomo	G434
069	 064 	069064	Pennapiedimonte	G435
069	 065 	069065	Perano	G441
069	 066 	069066	Pizzoferrato	G724
069	 067 	069067	Poggiofiorito	G760
069	 068 	069068	Pollutri	G799
069	 069 	069069	Pretoro	H052
069	 070 	069070	Quadri	H098
069	 071 	069071	Rapino	H184
069	 072 	069072	Ripa Teatina	H320
069	 073 	069073	Roccamontepiano	H424
069	 074 	069074	Rocca San Giovanni	H439
069	 075 	069075	Roccascalegna	H442
069	 076 	069076	Roccaspinalveti	H448
069	 077 	069077	Roio del Sangro	H495
069	 078 	069078	Rosello	H566
069	 079 	069079	San Buono	H784
074	 007 	074007	Fasano	D508
069	 080 	069080	San Giovanni Lipioni	H923
069	 081 	069081	San Giovanni Teatino	D690
069	 082 	069082	San Martino sulla Marrucina	H991
069	 083 	069083	San Salvo	I148
069	 084 	069084	Santa Maria Imbaro	I244
069	 085 	069085	SantEusanio del Sangro	I335
069	 086 	069086	San Vito Chietino	I394
069	 087 	069087	Scerni	I520
069	 088 	069088	Schiavi di Abruzzo	I526
069	 089 	069089	Taranta Peligna	L047
069	 090 	069090	Tollo	L194
069	 091 	069091	Torino di Sangro	L218
069	 092 	069092	Tornareccio	L224
069	 093 	069093	Torrebruna	L253
069	 094 	069094	Torrevecchia Teatina	L284
069	 095 	069095	Torricella Peligna	L291
069	 096 	069096	Treglio	L363
069	 097 	069097	Tufillo	L459
069	 098 	069098	Vacri	L526
069	 099 	069099	Vasto	E372
069	 100 	069100	Villalfonsina	L961
069	 101 	069101	Villamagna	L964
069	 102 	069102	Villa Santa Maria	M022
069	 103 	069103	Pietraferrazzana	G613
069	 104 	069104	Fallo	D480
070	 001 	070001	Acquaviva Collecroce	A050
070	 002 	070002	Baranello	A616
070	 003 	070003	Bojano	A930
070	 004 	070004	Bonefro	A971
070	 005 	070005	Busso	B295
070	 006 	070006	Campobasso	B519
070	 007 	070007	Campochiaro	B522
070	 008 	070008	Campodipietra	B528
070	 009 	070009	Campolieto	B544
070	 010 	070010	Campomarino	B550
070	 011 	070011	Casacalenda	B858
070	 012 	070012	Casalciprano	B871
070	 013 	070013	Castelbottaccio	C066
070	 014 	070014	Castellino del Biferno	C175
070	 015 	070015	Castelmauro	C197
070	 016 	070016	Castropignano	C346
070	 017 	070017	Cercemaggiore	C486
070	 018 	070018	Cercepiccola	C488
070	 019 	070019	Civitacampomarano	C764
070	 020 	070020	Colle dAnchise	C854
070	 021 	070021	Colletorto	C875
070	 022 	070022	Duronia	C772
070	 023 	070023	Ferrazzano	D550
070	 024 	070024	Fossalto	D737
070	 025 	070025	Gambatesa	D896
070	 026 	070026	Gildone	E030
070	 027 	070027	Guardialfiera	E244
070	 028 	070028	Guardiaregia	E248
070	 029 	070029	Guglionesi	E259
070	 030 	070030	Jelsi	E381
070	 031 	070031	Larino	E456
070	 032 	070032	Limosano	E599
070	 033 	070033	Lucito	E722
070	 034 	070034	Lupara	E748
070	 035 	070035	Macchia Valfortore	E780
070	 036 	070036	Mafalda	E799
070	 037 	070037	Matrice	F055
070	 038 	070038	Mirabello Sannitico	F233
070	 039 	070039	Molise	F294
070	 040 	070040	Monacilioni	F322
070	 041 	070041	Montagano	F391
070	 042 	070042	Montecilfone	F475
070	 043 	070043	Montefalcone nel Sannio	F495
070	 044 	070044	Montelongo	F548
070	 045 	070045	Montemitro	F569
070	 046 	070046	Montenero di Bisaccia	F576
070	 047 	070047	Montorio nei Frentani	F689
070	 048 	070048	Morrone del Sannio	F748
070	 049 	070049	Oratino	G086
070	 050 	070050	Palata	G257
070	 051 	070051	Petacciato	G506
070	 052 	070052	Petrella Tifernina	G512
070	 053 	070053	Pietracatella	G609
070	 054 	070054	Pietracupa	G610
070	 055 	070055	Portocannone	G910
070	 056 	070056	Provvidenti	H083
070	 057 	070057	Riccia	H273
070	 058 	070058	Ripabottoni	H311
070	 059 	070059	Ripalimosani	H313
070	 060 	070060	Roccavivara	H454
070	 061 	070061	Rotello	H589
070	 062 	070062	Salcito	H693
070	 063 	070063	San Biase	H782
070	 064 	070064	San Felice del Molise	H833
070	 065 	070065	San Giacomo degli Schiavoni	H867
070	 066 	070066	San Giovanni in Galdo	H920
070	 067 	070067	San Giuliano del Sannio	H928
070	 068 	070068	San Giuliano di Puglia	H929
070	 069 	070069	San Martino in Pensilis	H990
070	 070 	070070	San Massimo	I023
070	 071 	070071	San Polo Matese	I122
070	 072 	070072	Santa Croce di Magliano	I181
070	 073 	070073	SantAngelo Limosano	I289
070	 074 	070074	SantElia a Pianisi	I320
070	 075 	070075	Sepino	I618
070	 076 	070076	Spinete	I910
070	 077 	070077	Tavenna	L069
070	 078 	070078	Termoli	L113
070	 079 	070079	Torella del Sannio	L215
070	 080 	070080	Toro	L230
070	 081 	070081	Trivento	L435
070	 082 	070082	Tufara	L458
070	 083 	070083	Ururi	L505
070	 084 	070084	Vinchiaturo	M057
071	 001 	071001	Accadia	A015
071	 002 	071002	Alberona	A150
071	 003 	071003	Anzano di Puglia	A320
071	 004 	071004	Apricena	A339
071	 005 	071005	Ascoli Satriano	A463
071	 006 	071006	Biccari	A854
071	 007 	071007	Bovino	B104
071	 008 	071008	Cagnano Varano	B357
071	 009 	071009	Candela	B584
071	 010 	071010	Carapelle	B724
071	 011 	071011	Carlantino	B784
071	 012 	071012	Carpino	B829
071	 013 	071013	Casalnuovo Monterotaro	B904
071	 014 	071014	Casalvecchio di Puglia	B917
071	 015 	071015	Castelluccio dei Sauri	C198
071	 016 	071016	Castelluccio Valmaggiore	C202
071	 017 	071017	Castelnuovo della Daunia	C222
071	 018 	071018	Celenza Valfortore	C429
071	 019 	071019	Celle di San Vito	C442
071	 020 	071020	Cerignola	C514
071	 021 	071021	Chieuti	C633
071	 022 	071022	Deliceto	D269
071	 023 	071023	Faeto	D459
071	 024 	071024	Foggia	D643
071	 025 	071025	Ischitella	E332
071	 026 	071026	Isole Tremiti	E363
071	 027 	071027	Lesina	E549
071	 028 	071028	Lucera	E716
071	 029 	071029	Manfredonia	E885
071	 031 	071031	Mattinata	F059
071	 032 	071032	Monteleone di Puglia	F538
071	 033 	071033	Monte SantAngelo	F631
071	 034 	071034	Motta Montecorvino	F777
071	 035 	071035	Orsara di Puglia	G125
071	 036 	071036	Orta Nova	G131
071	 037 	071037	Panni	G312
071	 038 	071038	Peschici	G487
071	 039 	071039	Pietramontecorvino	G604
071	 040 	071040	Poggio Imperiale	G761
071	 041 	071041	Rignano Garganico	H287
071	 042 	071042	Rocchetta SantAntonio	H467
071	 043 	071043	Rodi Garganico	H480
071	 044 	071044	Roseto Valfortore	H568
071	 046 	071046	San Giovanni Rotondo	H926
071	 047 	071047	San Marco in Lamis	H985
071	 048 	071048	San Marco la Catola	H986
071	 049 	071049	San Nicandro Garganico	I054
071	 050 	071050	San Paolo di Civitate	I072
071	 051 	071051	San Severo	I158
071	 052 	071052	SantAgata di Puglia	I193
071	 053 	071053	Serracapriola	I641
071	 054 	071054	Stornara	I962
071	 055 	071055	Stornarella	I963
071	 056 	071056	Torremaggiore	L273
071	 058 	071058	Troia	L447
071	 059 	071059	Vico del Gargano	L842
071	 060 	071060	Vieste	L858
071	 061 	071061	Volturara Appula	M131
071	 062 	071062	Volturino	M132
071	 063 	071063	Ordona	M266
071	 064 	071064	Zapponeta	M267
072	 001 	072001	Acquaviva delle Fonti	A048
072	 002 	072002	Adelfia	A055
072	 003 	072003	Alberobello	A149
072	 004 	072004	Altamura	A225
072	 006 	072006	Bari	A662
072	 008 	072008	Binetto	A874
072	 010 	072010	Bitetto	A892
072	 011 	072011	Bitonto	A893
072	 012 	072012	Bitritto	A894
072	 014 	072014	Capurso	B716
072	 015 	072015	Casamassima	B923
072	 016 	072016	Cassano delle Murge	B998
072	 017 	072017	Castellana Grotte	C134
072	 018 	072018	Cellamare	C436
072	 019 	072019	Conversano	C975
072	 020 	072020	Corato	C983
072	 021 	072021	Gioia del Colle	E038
072	 022 	072022	Giovinazzo	E047
072	 023 	072023	Gravina in Puglia	E155
072	 024 	072024	Grumo Appula	E223
072	 025 	072025	Locorotondo	E645
072	 027 	072027	Modugno	F262
072	 028 	072028	Mola di Bari	F280
072	 029 	072029	Molfetta	F284
072	 030 	072030	Monopoli	F376
072	 031 	072031	Noci	F915
072	 032 	072032	Noicattaro	F923
072	 033 	072033	Palo del Colle	G291
072	 034 	072034	Poggiorsini	G769
072	 035 	072035	Polignano a Mare	G787
072	 036 	072036	Putignano	H096
072	 037 	072037	Rutigliano	H643
072	 038 	072038	Ruvo di Puglia	H645
072	 039 	072039	Sammichele di Bari	H749
072	 040 	072040	Sannicandro di Bari	I053
072	 041 	072041	Santeramo in Colle	I330
072	 043 	072043	Terlizzi	L109
072	 044 	072044	Toritto	L220
072	 046 	072046	Triggiano	L425
072	 047 	072047	Turi	L472
072	 048 	072048	Valenzano	L571
073	 001 	073001	Avetrana	A514
073	 002 	073002	Carosino	B808
073	 003 	073003	Castellaneta	C136
073	 004 	073004	Crispiano	D171
073	 005 	073005	Faggiano	D463
073	 006 	073006	Fragagnano	D754
073	 007 	073007	Ginosa	E036
073	 008 	073008	Grottaglie	E205
073	 009 	073009	Laterza	E469
073	 010 	073010	Leporano	E537
073	 011 	073011	Lizzano	E630
073	 012 	073012	Manduria	E882
073	 013 	073013	Martina Franca	E986
073	 014 	073014	Maruggio	E995
073	 015 	073015	Massafra	F027
073	 016 	073016	Monteiasi	F531
073	 017 	073017	Montemesola	F563
073	 018 	073018	Monteparano	F587
073	 019 	073019	Mottola	F784
073	 020 	073020	Palagianello	G251
073	 021 	073021	Palagiano	G252
073	 022 	073022	Pulsano	H090
073	 023 	073023	Roccaforzata	H409
073	 024 	073024	San Giorgio Ionico	H882
073	 025 	073025	San Marzano di San Giuseppe	I018
073	 026 	073026	Sava	I467
073	 027 	073027	Taranto	L049
073	 028 	073028	Torricella	L294
073	 029 	073029	Statte	M298
074	 001 	074001	Brindisi	B180
074	 002 	074002	Carovigno	B809
074	 003 	074003	Ceglie Messapica	C424
074	 004 	074004	Cellino San Marco	C448
074	 005 	074005	Cisternino	C741
074	 006 	074006	Erchie	D422
074	 008 	074008	Francavilla Fontana	D761
074	 009 	074009	Latiano	E471
074	 010 	074010	Mesagne	F152
074	 011 	074011	Oria	G098
074	 012 	074012	Ostuni	G187
074	 013 	074013	San Donaci	H822
074	 014 	074014	San Michele Salentino	I045
074	 015 	074015	San Pancrazio Salentino	I066
074	 016 	074016	San Pietro Vernotico	I119
074	 017 	074017	San Vito dei Normanni	I396
074	 018 	074018	Torchiarolo	L213
074	 019 	074019	Torre Santa Susanna	L280
074	 020 	074020	Villa Castelli	L920
075	 001 	075001	Acquarica del Capo	A042
075	 002 	075002	Alessano	A184
075	 003 	075003	Alezio	A185
075	 004 	075004	Alliste	A208
075	 005 	075005	Andrano	A281
075	 006 	075006	Aradeo	A350
075	 007 	075007	Arnesano	A425
075	 008 	075008	Bagnolo del Salento	A572
075	 009 	075009	Botrugno	B086
075	 010 	075010	Calimera	B413
075	 011 	075011	Campi Salentina	B506
075	 012 	075012	Cannole	B616
075	 013 	075013	Caprarica di Lecce	B690
075	 014 	075014	Carmiano	B792
075	 015 	075015	Carpignano Salentino	B822
075	 016 	075016	Casarano	B936
075	 017 	075017	Castri di Lecce	C334
075	 018 	075018	Castrignano de Greci	C335
075	 019 	075019	Castrignano del Capo	C336
075	 020 	075020	Cavallino	C377
075	 021 	075021	Collepasso	C865
075	 022 	075022	Copertino	C978
075	 023 	075023	Corigliano dOtranto	D006
075	 024 	075024	Corsano	D044
075	 025 	075025	Cursi	D223
075	 026 	075026	Cutrofiano	D237
075	 027 	075027	Diso	D305
075	 028 	075028	Gagliano del Capo	D851
075	 029 	075029	Galatina	D862
075	 030 	075030	Galatone	D863
075	 031 	075031	Gallipoli	D883
075	 032 	075032	Giuggianello	E053
075	 033 	075033	Giurdignano	E061
075	 034 	075034	Guagnano	E227
075	 035 	075035	Lecce	E506
075	 036 	075036	Lequile	E538
075	 037 	075037	Leverano	E563
075	 038 	075038	Lizzanello	E629
075	 039 	075039	Maglie	E815
075	 040 	075040	Martano	E979
075	 041 	075041	Martignano	E984
075	 042 	075042	Matino	F054
075	 043 	075043	Melendugno	F101
075	 044 	075044	Melissano	F109
075	 045 	075045	Melpignano	F117
075	 046 	075046	Miggiano	F194
075	 047 	075047	Minervino di Lecce	F221
075	 048 	075048	Monteroni di Lecce	F604
075	 049 	075049	Montesano Salentino	F623
075	 050 	075050	Morciano di Leuca	F716
075	 051 	075051	Muro Leccese	F816
075	 052 	075052	NardÔøΩ	F842
075	 053 	075053	Neviano	F881
075	 054 	075054	Nociglia	F916
075	 055 	075055	Novoli	F970
075	 056 	075056	Ortelle	G136
075	 057 	075057	Otranto	G188
075	 058 	075058	Palmariggi	G285
075	 059 	075059	Parabita	G325
075	 060 	075060	PatÔøΩ	G378
075	 061 	075061	Poggiardo	G751
075	 062 	075062	Presicce	H047
075	 063 	075063	Racale	H147
075	 064 	075064	Ruffano	H632
075	 065 	075065	Salice Salentino	H708
075	 066 	075066	Salve	H729
075	 067 	075067	Sanarica	H757
075	 068 	075068	San Cesario di Lecce	H793
075	 069 	075069	San Donato di Lecce	H826
075	 070 	075070	Sannicola	I059
075	 071 	075071	San Pietro in Lama	I115
075	 072 	075072	Santa Cesarea Terme	I172
075	 073 	075073	Scorrano	I549
075	 074 	075074	SeclÔøΩ	I559
075	 075 	075075	Sogliano Cavour	I780
075	 076 	075076	Soleto	I800
075	 077 	075077	Specchia	I887
075	 078 	075078	Spongano	I923
075	 079 	075079	Squinzano	I930
075	 080 	075080	Sternatia	I950
075	 081 	075081	Supersano	L008
075	 082 	075082	Surano	L010
075	 083 	075083	Surbo	L011
075	 084 	075084	Taurisano	L064
075	 085 	075085	Taviano	L074
075	 086 	075086	Tiggiano	L166
075	 087 	075087	Trepuzzi	L383
075	 088 	075088	Tricase	L419
075	 089 	075089	Tuglie	L462
075	 090 	075090	Ugento	L484
075	 091 	075091	Uggiano la Chiesa	L485
075	 092 	075092	Veglie	L711
075	 093 	075093	Vernole	L776
075	 094 	075094	Zollino	M187
075	 095 	075095	San Cassiano	M264
075	 096 	075096	Castro	M261
075	 097 	075097	Porto Cesareo	M263
076	 001 	076001	Abriola	A013
076	 002 	076002	Acerenza	A020
076	 003 	076003	Albano di Lucania	A131
076	 004 	076004	Anzi	A321
076	 005 	076005	Armento	A415
076	 006 	076006	Atella	A482
076	 007 	076007	Avigliano	A519
076	 008 	076008	Balvano	A604
076	 009 	076009	Banzi	A612
076	 010 	076010	Baragiano	A615
076	 011 	076011	Barile	A666
076	 012 	076012	Bella	A743
076	 013 	076013	Brienza	B173
076	 014 	076014	Brindisi Montagna	B181
076	 015 	076015	Calvello	B440
076	 016 	076016	Calvera	B443
076	 017 	076017	Campomaggiore	B549
076	 018 	076018	Cancellara	B580
076	 019 	076019	Carbone	B743
076	 020 	076020	San Paolo Albanese	B906
076	 021 	076021	Castelgrande	C120
076	 022 	076022	Castelluccio Inferiore	C199
076	 023 	076023	Castelluccio Superiore	C201
076	 024 	076024	Castelmezzano	C209
076	 025 	076025	Castelsaraceno	C271
076	 026 	076026	Castronuovo di SantAndrea	C345
076	 027 	076027	Cersosimo	C539
076	 028 	076028	Chiaromonte	C619
076	 029 	076029	Corleto Perticara	D010
076	 030 	076030	Episcopia	D414
076	 031 	076031	Fardella	D497
076	 032 	076032	Filiano	D593
076	 033 	076033	Forenza	D696
076	 034 	076034	Francavilla in Sinni	D766
076	 035 	076035	Gallicchio	D876
076	 036 	076036	Genzano di Lucania	D971
076	 037 	076037	Grumento Nova	E221
076	 038 	076038	Guardia Perticara	E246
076	 039 	076039	Lagonegro	E409
076	 040 	076040	Latronico	E474
076	 041 	076041	Laurenzana	E482
076	 042 	076042	Lauria	E483
076	 043 	076043	Lavello	E493
076	 044 	076044	Maratea	E919
076	 045 	076045	Marsico Nuovo	E976
076	 046 	076046	Marsicovetere	E977
076	 047 	076047	Maschito	F006
076	 048 	076048	Melfi	F104
076	 049 	076049	Missanello	F249
076	 050 	076050	Moliterno	F295
076	 051 	076051	Montemilone	F568
076	 052 	076052	Montemurro	F573
076	 053 	076053	Muro Lucano	F817
076	 054 	076054	Nemoli	F866
076	 055 	076055	Noepoli	F917
076	 056 	076056	Oppido Lucano	G081
076	 057 	076057	Palazzo San Gervasio	G261
076	 058 	076058	Pescopagano	G496
076	 059 	076059	Picerno	G590
076	 060 	076060	Pietragalla	G616
076	 061 	076061	Pietrapertosa	G623
076	 062 	076062	Pignola	G663
076	 063 	076063	Potenza	G942
076	 064 	076064	Rapolla	H186
076	 065 	076065	Rapone	H187
076	 066 	076066	Rionero in Vulture	H307
076	 067 	076067	Ripacandida	H312
076	 068 	076068	Rivello	H348
076	 069 	076069	Roccanova	H426
076	 070 	076070	Rotonda	H590
076	 071 	076071	Ruoti	H641
076	 072 	076072	Ruvo del Monte	H646
076	 073 	076073	San Chirico Nuovo	H795
076	 074 	076074	San Chirico Raparo	H796
076	 075 	076075	San Costantino Albanese	H808
076	 076 	076076	San Fele	H831
076	 077 	076077	San Martino dAgri	H994
076	 078 	076078	San Severino Lucano	I157
076	 079 	076079	SantAngelo Le Fratte	I288
076	 080 	076080	SantArcangelo	I305
076	 081 	076081	Sarconi	I426
076	 082 	076082	Sasso di Castalda	I457
076	 083 	076083	Satriano di Lucania	G614
076	 084 	076084	Savoia di Lucania	H730
076	 085 	076085	Senise	I610
076	 086 	076086	Spinoso	I917
076	 087 	076087	Teana	L082
076	 088 	076088	Terranova di Pollino	L126
076	 089 	076089	Tito	L181
076	 090 	076090	Tolve	L197
076	 091 	076091	Tramutola	L326
076	 092 	076092	Trecchina	L357
076	 093 	076093	Trivigno	L439
076	 094 	076094	Vaglio Basilicata	L532
076	 095 	076095	Venosa	L738
076	 096 	076096	Vietri di Potenza	L859
076	 097 	076097	Viggianello	L873
076	 098 	076098	Viggiano	L874
076	 099 	076099	Ginestra	E033
076	 100 	076100	Paterno	M269
077	 001 	077001	Accettura	A017
077	 002 	077002	Aliano	A196
077	 003 	077003	Bernalda	A801
077	 004 	077004	Calciano	B391
077	 005 	077005	Cirigliano	C723
077	 006 	077006	Colobraro	C888
077	 007 	077007	Craco	D128
077	 008 	077008	Ferrandina	D547
077	 009 	077009	Garaguso	D909
077	 010 	077010	Gorgoglione	E093
077	 011 	077011	Grassano	E147
077	 012 	077012	Grottole	E213
077	 013 	077013	Irsina	E326
077	 014 	077014	Matera	F052
077	 015 	077015	Miglionico	F201
077	 016 	077016	Montalbano Jonico	F399
077	 017 	077017	Montescaglioso	F637
077	 018 	077018	Nova Siri	A942
077	 019 	077019	Oliveto Lucano	G037
077	 020 	077020	Pisticci	G712
077	 021 	077021	Policoro	G786
077	 022 	077022	Pomarico	G806
077	 023 	077023	Rotondella	H591
077	 024 	077024	Salandra	H687
077	 025 	077025	San Giorgio Lucano	H888
077	 026 	077026	San Mauro Forte	I029
077	 027 	077027	Stigliano	I954
077	 028 	077028	Tricarico	L418
077	 029 	077029	Tursi	L477
077	 030 	077030	Valsinni	D513
077	 031 	077031	Scanzano Jonico	M256
078	 001 	078001	Acquaformosa	A033
078	 002 	078002	Acquappesa	A041
078	 003 	078003	Acri	A053
078	 004 	078004	Aiello Calabro	A102
078	 005 	078005	Aieta	A105
078	 006 	078006	Albidona	A160
078	 007 	078007	Alessandria del Carretto	A183
078	 008 	078008	Altilia	A234
078	 009 	078009	Altomonte	A240
078	 010 	078010	Amantea	A253
078	 011 	078011	Amendolara	A263
078	 012 	078012	Aprigliano	A340
078	 013 	078013	Belmonte Calabro	A762
078	 014 	078014	Belsito	A768
078	 015 	078015	Belvedere Marittimo	A773
078	 016 	078016	Bianchi	A842
078	 017 	078017	Bisignano	A887
078	 018 	078018	Bocchigliero	A912
078	 019 	078019	Bonifati	A973
078	 020 	078020	Buonvicino	B270
078	 021 	078021	Calopezzati	B424
078	 022 	078022	Caloveto	B426
078	 023 	078023	Campana	B500
078	 024 	078024	Canna	B607
078	 025 	078025	Cariati	B774
078	 026 	078026	Carolei	B802
078	 027 	078027	Carpanzano	B813
078	 028 	078028	Casole Bruzio	B983
078	 029 	078029	Cassano allIonio	C002
078	 030 	078030	Castiglione Cosentino	C301
078	 031 	078031	Castrolibero	C108
078	 032 	078032	Castroregio	C348
078	 033 	078033	Castrovillari	C349
078	 034 	078034	Celico	C430
078	 035 	078035	Cellara	C437
078	 036 	078036	Cerchiara di Calabria	C489
078	 037 	078037	Cerisano	C515
078	 038 	078038	Cervicati	C554
078	 039 	078039	Cerzeto	C560
078	 040 	078040	Cetraro	C588
078	 041 	078041	Civita	C763
078	 042 	078042	Cleto	C795
078	 043 	078043	Colosimi	C905
078	 044 	078044	Corigliano Calabro	D005
078	 045 	078045	Cosenza	D086
078	 046 	078046	Cropalati	D180
078	 047 	078047	Crosia	D184
078	 048 	078048	Diamante	D289
078	 049 	078049	Dipignano	D304
078	 050 	078050	Domanico	D328
078	 051 	078051	Fagnano Castello	D464
078	 052 	078052	Falconara Albanese	D473
078	 053 	078053	Figline Vegliaturo	D582
078	 054 	078054	Firmo	D614
078	 055 	078055	Fiumefreddo Bruzio	D624
078	 056 	078056	Francavilla Marittima	D764
078	 057 	078057	Frascineto	D774
078	 058 	078058	Fuscaldo	D828
078	 059 	078059	Grimaldi	E180
078	 060 	078060	Grisolia	E185
078	 061 	078061	Guardia Piemontese	E242
078	 062 	078062	Lago	E407
078	 063 	078063	Laino Borgo	E417
078	 064 	078064	Laino Castello	E419
078	 065 	078065	Lappano	E450
078	 066 	078066	Lattarico	E475
078	 067 	078067	Longobardi	E677
078	 068 	078068	Longobucco	E678
078	 069 	078069	Lungro	E745
078	 070 	078070	Luzzi	E773
078	 071 	078071	MaierÔøΩ	E835
078	 072 	078072	Malito	E859
078	 073 	078073	Malvito	E872
078	 074 	078074	Mandatoriccio	E878
078	 075 	078075	Mangone	E888
078	 076 	078076	Marano Marchesato	E914
078	 077 	078077	Marano Principato	E915
078	 078 	078078	Marzi	F001
078	 079 	078079	Mendicino	F125
078	 080 	078080	Mongrassano	F370
078	 081 	078081	Montalto Uffugo	F416
078	 082 	078082	Montegiordano	F519
078	 083 	078083	Morano Calabro	F708
078	 084 	078084	Mormanno	F735
078	 085 	078085	Mottafollone	F775
078	 086 	078086	Nocara	F907
078	 087 	078087	Oriolo	G110
078	 088 	078088	Orsomarso	G129
078	 089 	078089	Paludi	G298
078	 090 	078090	Panettieri	G307
078	 091 	078091	Paola	G317
078	 092 	078092	Papasidero	G320
078	 093 	078093	Parenti	G331
078	 094 	078094	Paterno Calabro	G372
078	 095 	078095	Pedace	G400
078	 096 	078096	Pedivigliano	G411
078	 097 	078097	Piane Crati	G553
078	 098 	078098	Pietrafitta	G615
078	 099 	078099	Pietrapaola	G622
078	 100 	078100	Plataci	G733
078	 101 	078101	Praia a Mare	G975
078	 102 	078102	Rende	H235
078	 103 	078103	Rocca Imperiale	H416
078	 104 	078104	Roggiano Gravina	H488
078	 105 	078105	Rogliano	H490
078	 106 	078106	Rose	H565
078	 107 	078107	Roseto Capo Spulico	H572
078	 108 	078108	Rossano	H579
078	 109 	078109	Rota Greca	H585
078	 110 	078110	Rovito	H621
078	 111 	078111	San Basile	H765
078	 112 	078112	San Benedetto Ullano	H774
078	 113 	078113	San Cosmo Albanese	H806
078	 114 	078114	San Demetrio Corone	H818
078	 115 	078115	San Donato di Ninea	H825
078	 116 	078116	San Fili	H841
078	 117 	078117	Sangineto	H877
078	 118 	078118	San Giorgio Albanese	H881
078	 119 	078119	San Giovanni in Fiore	H919
078	 120 	078120	San Lorenzo Bellizzi	H961
078	 121 	078121	San Lorenzo del Vallo	H962
078	 122 	078122	San Lucido	H971
078	 123 	078123	San Marco Argentano	H981
078	 124 	078124	San Martino di Finita	H992
078	 125 	078125	San Nicola Arcella	I060
078	 126 	078126	San Pietro in Amantea	I108
078	 127 	078127	San Pietro in Guarano	I114
078	 128 	078128	San Sosti	I165
078	 129 	078129	Santa Caterina Albanese	I171
078	 130 	078130	Santa Domenica Talao	I183
078	 131 	078131	SantAgata di Esaro	I192
078	 132 	078132	Santa Maria del Cedro	C717
078	 133 	078133	Santa Sofia dEpiro	I309
078	 134 	078134	Santo Stefano di Rogliano	I359
078	 135 	078135	San Vincenzo La Costa	I388
078	 136 	078136	Saracena	I423
078	 137 	078137	Scala Coeli	I485
078	 138 	078138	Scalea	I489
078	 139 	078139	Scigliano	D290
078	 140 	078140	Serra dAiello	I642
078	 141 	078141	Serra Pedace	I650
078	 142 	078142	Spezzano Albanese	I895
078	 143 	078143	Spezzano della Sila	I896
078	 144 	078144	Spezzano Piccolo	I898
078	 145 	078145	Tarsia	L055
078	 146 	078146	Terranova da Sibari	L124
078	 147 	078147	Terravecchia	L134
078	 148 	078148	Torano Castello	L206
078	 149 	078149	Tortora	L305
078	 150 	078150	Trebisacce	L353
078	 151 	078151	Trenta	L375
078	 152 	078152	Vaccarizzo Albanese	L524
078	 153 	078153	Verbicaro	L747
078	 154 	078154	Villapiana	B903
078	 155 	078155	Zumpano	M202
079	 002 	079002	Albi	A155
079	 003 	079003	Amaroni	A255
079	 004 	079004	Amato	A257
079	 005 	079005	Andali	A272
079	 007 	079007	Argusto	A397
079	 008 	079008	Badolato	A542
079	 009 	079009	Belcastro	A736
079	 011 	079011	Borgia	B002
079	 012 	079012	Botricello	B085
079	 017 	079017	Caraffa di Catanzaro	B717
079	 018 	079018	Cardinale	B758
079	 020 	079020	Carlopoli	B790
079	 023 	079023	Catanzaro	C352
079	 024 	079024	Cenadi	C453
079	 025 	079025	Centrache	C472
079	 027 	079027	Cerva	C542
079	 029 	079029	Chiaravalle Centrale	C616
079	 030 	079030	Cicala	C674
079	 033 	079033	Conflenti	C960
079	 034 	079034	Cortale	D049
079	 036 	079036	Cropani	D181
079	 039 	079039	Curinga	D218
079	 042 	079042	Davoli	D257
079	 043 	079043	Decollatura	D261
079	 047 	079047	Falerna	D476
079	 048 	079048	Feroleto Antico	D544
079	 052 	079052	Fossato Serralta	D744
079	 055 	079055	Gagliato	D852
079	 056 	079056	Gasperina	D932
079	 058 	079058	Gimigliano	E031
079	 059 	079059	Girifalco	E050
079	 060 	079060	Gizzeria	E068
079	 061 	079061	Guardavalle	E239
079	 063 	079063	Isca sullo Ionio	E328
079	 065 	079065	Jacurso	E274
079	 068 	079068	Magisano	E806
079	 069 	079069	Maida	E834
079	 071 	079071	Marcedusa	E923
079	 072 	079072	Marcellinara	E925
079	 073 	079073	Martirano	E990
079	 074 	079074	Martirano Lombardo	E991
079	 077 	079077	Miglierina	F200
079	 080 	079080	Montauro	F432
079	 081 	079081	Montepaone	F586
079	 083 	079083	Motta Santa Lucia	F780
079	 087 	079087	Nocera Terinese	F910
079	 088 	079088	Olivadi	G034
079	 089 	079089	Palermiti	G272
079	 092 	079092	Pentone	G439
079	 094 	079094	Petrizzi	G517
079	 095 	079095	PetronÔøΩ	G518
079	 096 	079096	Pianopoli	D546
079	 099 	079099	Platania	G734
079	 108 	079108	San Floro	H846
079	 110 	079110	San Mango dAquino	H976
079	 114 	079114	San Pietro a Maida	I093
079	 115 	079115	San Pietro Apostolo	I095
079	 116 	079116	San Sostene	I164
079	 117 	079117	Santa Caterina dello Ionio	I170
079	 118 	079118	SantAndrea Apostolo dello Ionio	I266
079	 122 	079122	San Vito sullo Ionio	I393
079	 123 	079123	Satriano	I463
079	 126 	079126	Sellia	I589
079	 127 	079127	Sellia Marina	I590
079	 129 	079129	Serrastretta	I655
079	 130 	079130	Sersale	I671
079	 131 	079131	Settingiano	I704
079	 133 	079133	Simeri Crichi	I745
079	 134 	079134	Sorbo San Basile	I844
079	 137 	079137	Soverato	I872
079	 138 	079138	Soveria Mannelli	I874
079	 139 	079139	Soveria Simeri	I875
079	 142 	079142	Squillace	I929
079	 143 	079143	StalettÔøΩ	I937
079	 146 	079146	Taverna	L070
079	 147 	079147	Tiriolo	L177
079	 148 	079148	Torre di Ruggiero	L240
079	 151 	079151	Vallefiorita	I322
079	 157 	079157	Zagarise	M140
079	 160 	079160	Lamezia Terme	M208
080	 001 	080001	Africo	A065
080	 002 	080002	Agnana Calabra	A077
080	 003 	080003	Anoia	A303
080	 004 	080004	Antonimina	A314
080	 005 	080005	Ardore	A385
080	 006 	080006	Bagaladi	A544
080	 007 	080007	Bagnara Calabra	A552
080	 008 	080008	Benestare	A780
080	 009 	080009	Bianco	A843
080	 010 	080010	Bivongi	A897
080	 011 	080011	Bova	B097
080	 012 	080012	Bovalino	B098
080	 013 	080013	Bova Marina	B099
080	 014 	080014	Brancaleone	B118
080	 015 	080015	Bruzzano Zeffirio	B234
080	 016 	080016	Calanna	B379
080	 017 	080017	Camini	B481
080	 018 	080018	Campo Calabro	B516
080	 019 	080019	Candidoni	B591
080	 020 	080020	Canolo	B617
080	 021 	080021	Caraffa del Bianco	B718
080	 022 	080022	Cardeto	B756
080	 023 	080023	Careri	B766
080	 024 	080024	Casignana	B966
080	 025 	080025	Caulonia	C285
080	 026 	080026	CiminÔøΩ	C695
080	 027 	080027	Cinquefrondi	C710
080	 028 	080028	Cittanova	C747
080	 029 	080029	Condofuri	C954
080	 030 	080030	Cosoleto	D089
080	 031 	080031	Delianuova	D268
080	 032 	080032	Feroleto della Chiesa	D545
080	 033 	080033	Ferruzzano	D557
080	 034 	080034	Fiumara	D619
080	 035 	080035	Galatro	D864
080	 036 	080036	Gerace	D975
080	 037 	080037	Giffone	E025
080	 038 	080038	Gioia Tauro	E041
080	 039 	080039	Gioiosa Ionica	E044
080	 040 	080040	Grotteria	E212
080	 041 	080041	Laganadi	E402
080	 042 	080042	Laureana di Borrello	E479
080	 043 	080043	Locri	D976
080	 044 	080044	Mammola	E873
080	 045 	080045	Marina di Gioiosa Ionica	E956
080	 046 	080046	Maropati	E968
080	 047 	080047	Martone	E993
080	 048 	080048	MelicuccÔøΩ	F105
080	 049 	080049	Melicucco	F106
080	 050 	080050	Melito di Porto Salvo	F112
080	 051 	080051	Molochio	F301
080	 052 	080052	Monasterace	F324
080	 053 	080053	Montebello Ionico	D746
080	 054 	080054	Motta San Giovanni	F779
080	 055 	080055	Oppido Mamertina	G082
080	 056 	080056	Palizzi	G277
080	 057 	080057	Palmi	G288
080	 058 	080058	Pazzano	G394
080	 059 	080059	Placanica	G729
080	 060 	080060	PlatÔøΩ	G735
080	 061 	080061	Polistena	G791
080	 062 	080062	Portigliola	G905
080	 063 	080063	Reggio di Calabria	H224
080	 064 	080064	Riace	H265
080	 065 	080065	Rizziconi	H359
080	 066 	080066	Roccaforte del Greco	H408
080	 067 	080067	Roccella Ionica	H456
080	 068 	080068	Roghudi	H489
080	 069 	080069	Rosarno	H558
080	 070 	080070	Samo	H013
080	 071 	080071	San Giorgio Morgeto	H889
080	 072 	080072	San Giovanni di Gerace	H903
080	 073 	080073	San Lorenzo	H959
080	 074 	080074	San Luca	H970
080	 075 	080075	San Pietro di CaridÔøΩ	I102
080	 076 	080076	San Procopio	I132
080	 077 	080077	San Roberto	I139
080	 078 	080078	Santa Cristina dAspromonte	I176
080	 079 	080079	SantAgata del Bianco	I198
080	 080 	080080	SantAlessio in Aspromonte	I214
080	 081 	080081	SantEufemia dAspromonte	I333
080	 082 	080082	SantIlario dello Ionio	I341
080	 083 	080083	Santo Stefano in Aspromonte	I371
080	 084 	080084	Scido	I536
080	 085 	080085	Scilla	I537
080	 086 	080086	Seminara	I600
080	 087 	080087	Serrata	I656
080	 088 	080088	Siderno	I725
080	 089 	080089	Sinopoli	I753
080	 090 	080090	Staiti	I936
080	 091 	080091	Stignano	I955
080	 092 	080092	Stilo	I956
080	 093 	080093	Taurianova	L063
080	 094 	080094	Terranova Sappo Minulio	L127
080	 095 	080095	Varapodio	L673
080	 096 	080096	Villa San Giovanni	M018
080	 097 	080097	San Ferdinando	M277
081	 001 	081001	Alcamo	A176
081	 002 	081002	Buseto Palizzolo	B288
081	 003 	081003	Calatafimi-Segesta	B385
081	 004 	081004	Campobello di Mazara	B521
081	 005 	081005	Castellammare del Golfo	C130
081	 006 	081006	Castelvetrano	C286
081	 007 	081007	Custonaci	D234
081	 008 	081008	Erice	D423
081	 009 	081009	Favignana	D518
081	 010 	081010	Gibellina	E023
081	 011 	081011	Marsala	E974
081	 012 	081012	Mazara del Vallo	F061
081	 013 	081013	Paceco	G208
081	 014 	081014	Pantelleria	G315
081	 015 	081015	Partanna	G347
081	 016 	081016	Poggioreale	G767
081	 017 	081017	Salaparuta	H688
081	 018 	081018	Salemi	H700
081	 019 	081019	Santa Ninfa	I291
081	 020 	081020	San Vito Lo Capo	I407
081	 021 	081021	Trapani	L331
081	 022 	081022	Valderice	G319
081	 023 	081023	Vita	M081
081	 024 	081024	Petrosino	M281
082	 001 	082001	Alia	A195
082	 002 	082002	Alimena	A202
082	 003 	082003	Aliminusa	A203
082	 004 	082004	Altavilla Milicia	A229
082	 005 	082005	Altofonte	A239
082	 006 	082006	Bagheria	A546
082	 007 	082007	Balestrate	A592
082	 008 	082008	Baucina	A719
082	 009 	082009	Belmonte Mezzagno	A764
082	 010 	082010	Bisacquino	A882
082	 011 	082011	Bolognetta	A946
082	 012 	082012	Bompietro	A958
082	 013 	082013	Borgetto	A991
082	 014 	082014	Caccamo	B315
082	 015 	082015	Caltavuturo	B430
082	 016 	082016	Campofelice di Fitalia	B533
082	 017 	082017	Campofelice di Roccella	B532
082	 018 	082018	Campofiorito	B535
082	 019 	082019	Camporeale	B556
082	 020 	082020	Capaci	B645
082	 021 	082021	Carini	B780
082	 022 	082022	Castelbuono	C067
082	 023 	082023	Casteldaccia	C074
082	 024 	082024	Castellana Sicula	C135
082	 025 	082025	Castronovo di Sicilia	C344
082	 026 	082026	CefalÔøΩ Diana	C420
082	 027 	082027	CefalÔøΩ	C421
082	 028 	082028	Cerda	C496
082	 029 	082029	Chiusa Sclafani	C654
082	 030 	082030	Ciminna	C696
082	 031 	082031	Cinisi	C708
082	 032 	082032	Collesano	C871
082	 033 	082033	Contessa Entellina	C968
082	 034 	082034	Corleone	D009
082	 035 	082035	Ficarazzi	D567
082	 036 	082036	Gangi	D907
082	 037 	082037	Geraci Siculo	D977
082	 038 	082038	Giardinello	E013
082	 039 	082039	Giuliana	E055
082	 040 	082040	Godrano	E074
082	 041 	082041	Gratteri	E149
082	 042 	082042	Isnello	E337
082	 043 	082043	Isola delle Femmine	E350
082	 044 	082044	Lascari	E459
082	 045 	082045	Lercara Friddi	E541
082	 046 	082046	Marineo	E957
082	 047 	082047	Mezzojuso	F184
082	 048 	082048	Misilmeri	F246
082	 049 	082049	Monreale	F377
082	 050 	082050	Montelepre	F544
082	 051 	082051	Montemaggiore Belsito	F553
082	 052 	082052	Palazzo Adriano	G263
082	 053 	082053	Palermo	G273
082	 054 	082054	Partinico	G348
082	 055 	082055	Petralia Soprana	G510
082	 056 	082056	Petralia Sottana	G511
082	 057 	082057	Piana degli Albanesi	G543
082	 058 	082058	Polizzi Generosa	G792
082	 059 	082059	Pollina	G797
082	 060 	082060	Prizzi	H070
082	 061 	082061	Roccamena	H422
082	 062 	082062	Roccapalumba	H428
082	 063 	082063	San Cipirello	H797
082	 064 	082064	San Giuseppe Jato	H933
082	 065 	082065	San Mauro Castelverde	I028
082	 066 	082066	Santa Cristina Gela	I174
082	 067 	082067	Santa Flavia	I188
082	 068 	082068	Sciara	I534
082	 069 	082069	Sclafani Bagni	I541
082	 070 	082070	Termini Imerese	L112
082	 071 	082071	Terrasini	L131
082	 072 	082072	Torretta	L282
082	 073 	082073	Trabia	L317
082	 074 	082074	Trappeto	L332
082	 075 	082075	Ustica	L519
082	 076 	082076	Valledolmo	L603
082	 077 	082077	Ventimiglia di Sicilia	L740
082	 078 	082078	Vicari	L837
082	 079 	082079	Villabate	L916
082	 080 	082080	Villafrati	L951
082	 081 	082081	Scillato	I538
082	 082 	082082	Blufi	M268
083	 001 	083001	Alcara li Fusi	A177
083	 002 	083002	AlÔøΩ	A194
083	 003 	083003	AlÔøΩ Terme	A201
083	 004 	083004	Antillo	A313
083	 005 	083005	Barcellona Pozzo di Gotto	A638
083	 006 	083006	BasicÔøΩ	A698
083	 007 	083007	Brolo	B198
083	 008 	083008	Capizzi	B660
083	 009 	083009	Capo dOrlando	B666
083	 010 	083010	Capri Leone	B695
083	 011 	083011	Caronia	B804
083	 012 	083012	Casalvecchio Siculo	B918
083	 013 	083013	Castel di Lucio	C094
083	 014 	083014	CastellUmberto	C051
083	 015 	083015	Castelmola	C210
083	 016 	083016	Castroreale	C347
083	 017 	083017	CesarÔøΩ	C568
083	 018 	083018	CondrÔøΩ	C956
083	 019 	083019	Falcone	D474
083	 020 	083020	Ficarra	D569
083	 021 	083021	Fiumedinisi	D622
083	 022 	083022	Floresta	D635
083	 023 	083023	Fondachelli-Fantina	D661
083	 024 	083024	Forza dAgrÔøΩ	D733
083	 025 	083025	Francavilla di Sicilia	D765
083	 026 	083026	FrazzanÔøΩ	D793
083	 027 	083027	Furci Siculo	D824
083	 028 	083028	Furnari	D825
083	 029 	083029	Gaggi	D844
083	 030 	083030	Galati Mamertino	D861
083	 031 	083031	Gallodoro	D885
083	 032 	083032	Giardini-Naxos	E014
083	 033 	083033	Gioiosa Marea	E043
083	 034 	083034	Graniti	E142
083	 035 	083035	Gualtieri SicaminÔøΩ	E233
083	 036 	083036	Itala	E374
083	 037 	083037	Leni	E523
083	 038 	083038	Letojanni	E555
083	 039 	083039	Librizzi	E571
083	 040 	083040	Limina	E594
083	 041 	083041	Lipari	E606
083	 042 	083042	Longi	E674
083	 043 	083043	Malfa	E855
083	 044 	083044	Malvagna	E869
083	 045 	083045	Mandanici	E876
083	 046 	083046	MazzarrÔøΩ SantAndrea	F066
083	 047 	083047	MerÔøΩ	F147
083	 048 	083048	Messina	F158
083	 049 	083049	Milazzo	F206
083	 050 	083050	Militello Rosmarino	F210
083	 051 	083051	Mirto	F242
083	 052 	083052	Mistretta	F251
083	 053 	083053	Moio Alcantara	F277
083	 054 	083054	Monforte San Giorgio	F359
083	 055 	083055	Mongiuffi Melia	F368
083	 056 	083056	Montagnareale	F395
083	 057 	083057	Montalbano Elicona	F400
083	 058 	083058	Motta Camastra	F772
083	 059 	083059	Motta dAffermo	F773
083	 060 	083060	Naso	F848
083	 061 	083061	Nizza di Sicilia	F901
083	 062 	083062	Novara di Sicilia	F951
083	 063 	083063	Oliveri	G036
083	 064 	083064	Pace del Mela	G209
083	 065 	083065	Pagliara	G234
083	 066 	083066	Patti	G377
083	 067 	083067	Pettineo	G522
083	 068 	083068	Piraino	G699
083	 069 	083069	Raccuja	H151
083	 070 	083070	Reitano	H228
083	 071 	083071	Roccafiorita	H405
083	 072 	083072	Roccalumera	H418
083	 073 	083073	Roccavaldina	H380
083	 074 	083074	Roccella Valdemone	H455
083	 075 	083075	RodÔøΩ Milici	H479
083	 076 	083076	Rometta	H519
083	 077 	083077	San Filippo del Mela	H842
083	 078 	083078	San Fratello	H850
083	 079 	083079	San Marco dAlunzio	H982
083	 080 	083080	San Pier Niceto	I084
083	 081 	083081	San Piero Patti	I086
083	 082 	083082	San Salvatore di Fitalia	I147
083	 083 	083083	Santa Domenica Vittoria	I184
083	 084 	083084	SantAgata di Militello	I199
083	 085 	083085	SantAlessio Siculo	I215
083	 086 	083086	Santa Lucia del Mela	I220
083	 087 	083087	Santa Marina Salina	I254
083	 088 	083088	SantAngelo di Brolo	I283
083	 089 	083089	Santa Teresa di Riva	I311
083	 090 	083090	San Teodoro	I328
083	 091 	083091	Santo Stefano di Camastra	I370
083	 092 	083092	Saponara	I420
083	 093 	083093	Savoca	I477
083	 094 	083094	Scaletta Zanclea	I492
083	 095 	083095	Sinagra	I747
083	 096 	083096	Spadafora	I881
083	 097 	083097	Taormina	L042
083	 098 	083098	Torregrotta	L271
083	 099 	083099	Tortorici	L308
083	 100 	083100	Tripi	L431
083	 101 	083101	Tusa	L478
083	 102 	083102	Ucria	L482
083	 103 	083103	Valdina	L561
083	 104 	083104	Venetico	L735
083	 105 	083105	Villafranca Tirrena	L950
083	 106 	083106	Terme Vigliatore	M210
083	 107 	083107	Acquedolci	M211
083	 108 	083108	Torrenova	M286
084	 001 	084001	Agrigento	A089
084	 002 	084002	Alessandria della Rocca	A181
084	 003 	084003	Aragona	A351
084	 004 	084004	Bivona	A896
084	 005 	084005	Burgio	B275
084	 006 	084006	Calamonaci	B377
084	 007 	084007	Caltabellotta	B427
084	 008 	084008	Camastra	B460
084	 009 	084009	Cammarata	B486
084	 010 	084010	Campobello di Licata	B520
084	 011 	084011	CanicattÔøΩ	B602
084	 012 	084012	Casteltermini	C275
084	 013 	084013	Castrofilippo	C341
084	 014 	084014	Cattolica Eraclea	C356
084	 015 	084015	Cianciana	C668
084	 016 	084016	Comitini	C928
084	 017 	084017	Favara	D514
084	 018 	084018	Grotte	E209
084	 019 	084019	Joppolo Giancaxio	E390
084	 020 	084020	Lampedusa e Linosa	E431
084	 021 	084021	Licata	E573
084	 022 	084022	Lucca Sicula	E714
084	 023 	084023	Menfi	F126
084	 024 	084024	Montallegro	F414
084	 025 	084025	Montevago	F655
084	 026 	084026	Naro	F845
084	 027 	084027	Palma di Montechiaro	G282
084	 028 	084028	Porto Empedocle	F299
084	 029 	084029	Racalmuto	H148
084	 030 	084030	Raffadali	H159
084	 031 	084031	Ravanusa	H194
084	 032 	084032	Realmonte	H205
084	 033 	084033	Ribera	H269
084	 034 	084034	Sambuca di Sicilia	H743
084	 035 	084035	San Biagio Platani	H778
084	 036 	084036	San Giovanni Gemini	H914
084	 037 	084037	Santa Elisabetta	I185
084	 038 	084038	Santa Margherita di Belice	I224
084	 039 	084039	SantAngelo Muxaro	I290
084	 040 	084040	Santo Stefano Quisquina	I356
084	 041 	084041	Sciacca	I533
084	 042 	084042	Siculiana	I723
084	 043 	084043	Villafranca Sicula	L944
085	 001 	085001	Acquaviva Platani	A049
085	 002 	085002	Bompensiere	A957
085	 003 	085003	Butera	B302
085	 004 	085004	Caltanissetta	B429
085	 005 	085005	Campofranco	B537
085	 006 	085006	Delia	D267
085	 007 	085007	Gela	D960
085	 008 	085008	Marianopoli	E953
085	 009 	085009	Mazzarino	F065
085	 010 	085010	Milena	E618
085	 011 	085011	Montedoro	F489
085	 012 	085012	Mussomeli	F830
085	 013 	085013	Niscemi	F899
085	 014 	085014	Resuttano	H245
085	 015 	085015	Riesi	H281
085	 016 	085016	San Cataldo	H792
085	 017 	085017	Santa Caterina Villarmosa	I169
085	 018 	085018	Serradifalco	I644
085	 019 	085019	Sommatino	I824
085	 020 	085020	Sutera	L016
085	 021 	085021	Vallelunga Pratameno	L609
085	 022 	085022	Villalba	L959
086	 001 	086001	Agira	A070
086	 002 	086002	Aidone	A098
086	 003 	086003	Assoro	A478
086	 004 	086004	Barrafranca	A676
086	 005 	086005	Calascibetta	B381
086	 006 	086006	Catenanuova	C353
086	 007 	086007	Centuripe	C471
086	 008 	086008	Cerami	C480
086	 009 	086009	Enna	C342
086	 010 	086010	Gagliano Castelferrato	D849
086	 011 	086011	Leonforte	E536
086	 012 	086012	Nicosia	F892
086	 013 	086013	Nissoria	F900
086	 014 	086014	Piazza Armerina	G580
086	 015 	086015	Pietraperzia	G624
086	 016 	086016	Regalbuto	H221
086	 017 	086017	Sperlinga	I891
086	 018 	086018	Troina	L448
086	 019 	086019	Valguarnera Caropepe	L583
086	 020 	086020	Villarosa	M011
087	 001 	087001	Aci Bonaccorsi	A025
087	 002 	087002	Aci Castello	A026
087	 003 	087003	Aci Catena	A027
087	 004 	087004	Acireale	A028
087	 005 	087005	Aci SantAntonio	A029
087	 006 	087006	Adrano	A056
087	 007 	087007	Belpasso	A766
087	 008 	087008	Biancavilla	A841
087	 009 	087009	Bronte	B202
087	 010 	087010	Calatabiano	B384
087	 011 	087011	Caltagirone	B428
087	 012 	087012	Camporotondo Etneo	B561
087	 013 	087013	Castel di Iudica	C091
087	 014 	087014	Castiglione di Sicilia	C297
087	 015 	087015	Catania	C351
087	 016 	087016	Fiumefreddo di Sicilia	D623
087	 017 	087017	Giarre	E017
087	 018 	087018	Grammichele	E133
087	 019 	087019	Gravina di Catania	E156
087	 020 	087020	Licodia Eubea	E578
087	 021 	087021	Linguaglossa	E602
087	 022 	087022	Maletto	E854
087	 023 	087023	Mascali	F004
087	 024 	087024	Mascalucia	F005
087	 025 	087025	Militello in Val di Catania	F209
087	 026 	087026	Milo	F214
087	 027 	087027	Mineo	F217
087	 028 	087028	Mirabella Imbaccari	F231
087	 029 	087029	Misterbianco	F250
087	 030 	087030	Motta SantAnastasia	F781
087	 031 	087031	Nicolosi	F890
087	 032 	087032	Palagonia	G253
087	 033 	087033	PaternÔøΩ	G371
087	 034 	087034	Pedara	G402
087	 035 	087035	Piedimonte Etneo	G597
087	 036 	087036	Raddusa	H154
087	 037 	087037	Ramacca	H168
087	 038 	087038	Randazzo	H175
087	 039 	087039	Riposto	H325
087	 040 	087040	San Cono	H805
087	 041 	087041	San Giovanni la Punta	H922
087	 042 	087042	San Gregorio di Catania	H940
087	 043 	087043	San Michele di Ganzaria	I035
087	 044 	087044	San Pietro Clarenza	I098
087	 045 	087045	SantAgata li Battiati	I202
087	 046 	087046	SantAlfio	I216
087	 047 	087047	Santa Maria di Licodia	I240
087	 048 	087048	Santa Venerina	I314
087	 049 	087049	Scordia	I548
087	 050 	087050	Trecastagni	L355
087	 051 	087051	Tremestieri Etneo	L369
087	 052 	087052	Valverde	L658
087	 053 	087053	Viagrande	L828
087	 054 	087054	Vizzini	M100
087	 055 	087055	Zafferana Etnea	M139
087	 056 	087056	Mazzarrone	M271
087	 057 	087057	Maniace	M283
087	 058 	087058	Ragalna	M287
088	 001 	088001	Acate	A014
088	 002 	088002	Chiaramonte Gulfi	C612
088	 003 	088003	Comiso	C927
088	 004 	088004	Giarratana	E016
088	 005 	088005	Ispica	E366
088	 006 	088006	Modica	F258
088	 007 	088007	Monterosso Almo	F610
088	 008 	088008	Pozzallo	G953
088	 009 	088009	Ragusa	H163
088	 010 	088010	Santa Croce Camerina	I178
088	 011 	088011	Scicli	I535
088	 012 	088012	Vittoria	M088
089	 001 	089001	Augusta	A494
089	 002 	089002	Avola	A522
089	 003 	089003	Buccheri	B237
089	 004 	089004	Buscemi	B287
089	 005 	089005	Canicattini Bagni	B603
089	 006 	089006	Carlentini	B787
089	 007 	089007	Cassaro	C006
089	 008 	089008	Ferla	D540
089	 009 	089009	Floridia	D636
089	 010 	089010	Francofonte	D768
089	 011 	089011	Lentini	E532
089	 012 	089012	Melilli	F107
089	 013 	089013	Noto	F943
089	 014 	089014	Pachino	G211
089	 015 	089015	Palazzolo Acreide	G267
089	 016 	089016	Rosolini	H574
089	 017 	089017	Siracusa	I754
089	 018 	089018	Solarino	I785
089	 019 	089019	Sortino	I864
089	 020 	089020	Portopalo di Capo Passero	M257
089	 021 	089021	Priolo Gargallo	M279
090	 003 	090003	Alghero	A192
090	 004 	090004	Anela	A287
090	 005 	090005	Ardara	A379
090	 007 	090007	Banari	A606
090	 008 	090008	Benetutti	A781
090	 010 	090010	Bessude	A827
090	 011 	090011	Bonnanaro	A976
090	 012 	090012	Bono	A977
090	 013 	090013	Bonorva	A978
090	 015 	090015	Borutta	B064
090	 016 	090016	Bottidda	B094
090	 018 	090018	Bultei	B264
090	 019 	090019	Bulzi	B265
090	 020 	090020	Burgos	B276
090	 022 	090022	Cargeghe	B772
090	 023 	090023	Castelsardo	C272
090	 024 	090024	Cheremule	C600
090	 025 	090025	Chiaramonti	C613
090	 026 	090026	Codrongianos	C818
090	 027 	090027	Cossoine	D100
090	 028 	090028	Esporlatu	D441
090	 029 	090029	Florinas	D637
090	 030 	090030	Giave	E019
090	 031 	090031	Illorai	E285
090	 032 	090032	Ittireddu	E376
090	 033 	090033	Ittiri	E377
090	 034 	090034	Laerru	E401
090	 038 	090038	Mara	E902
090	 039 	090039	Martis	E992
090	 040 	090040	Monteleone Rocca Doria	F542
090	 042 	090042	Mores	F721
090	 043 	090043	Muros	F818
090	 044 	090044	Nughedu San NicolÔøΩ	F975
090	 045 	090045	Nule	F976
090	 046 	090046	Nulvi	F977
090	 048 	090048	Olmedo	G046
090	 050 	090050	Osilo	G156
090	 051 	090051	Ossi	G178
090	 052 	090052	Ozieri	G203
090	 053 	090053	Padria	G225
090	 055 	090055	Pattada	G376
090	 056 	090056	Perfugas	G450
090	 057 	090057	Ploaghe	G740
090	 058 	090058	Porto Torres	G924
090	 059 	090059	Pozzomaggiore	G962
090	 060 	090060	Putifigari	H095
090	 061 	090061	Romana	H507
090	 064 	090064	Sassari	I452
090	 065 	090065	Sedini	I565
090	 066 	090066	Semestene	I598
090	 067 	090067	Sennori	I614
090	 068 	090068	Siligo	I732
090	 069 	090069	Sorso	I863
090	 071 	090071	Thiesi	L158
090	 072 	090072	Tissi	L180
090	 073 	090073	Torralba	L235
090	 075 	090075	Tula	L464
090	 076 	090076	Uri	L503
090	 077 	090077	Usini	L509
090	 078 	090078	Villanova Monteleone	L989
090	 079 	090079	Valledoria	L604
090	 082 	090082	Viddalba	M259
090	 086 	090086	Tergu	M282
090	 087 	090087	Santa Maria Coghinas	M284
090	 088 	090088	Erula	M292
090	 089 	090089	Stintino	M290
091	 001 	091001	Aritzo	A407
091	 003 	091003	Atzara	A492
091	 004 	091004	Austis	A503
091	 007 	091007	BelvÔøΩ	A776
091	 008 	091008	Birori	A880
091	 009 	091009	Bitti	A895
091	 010 	091010	Bolotana	A948
091	 011 	091011	Borore	B056
091	 012 	091012	Bortigali	B062
091	 016 	091016	Desulo	D287
091	 017 	091017	Dorgali	D345
091	 018 	091018	Dualchi	D376
091	 024 	091024	Fonni	D665
091	 025 	091025	Gadoni	D842
091	 027 	091027	GaltellÔøΩ	D888
091	 028 	091028	Gavoi	D947
091	 033 	091033	Irgoli	E323
091	 038 	091038	Lei	E517
091	 040 	091040	Loculi	E646
091	 041 	091041	LodÔøΩ	E647
091	 043 	091043	Lula	E736
091	 044 	091044	Macomer	E788
091	 046 	091046	Mamoiada	E874
091	 047 	091047	Meana Sardo	F073
091	 050 	091050	Noragugume	F933
091	 051 	091051	Nuoro	F979
091	 055 	091055	Oliena	G031
091	 056 	091056	Ollolai	G044
091	 057 	091057	Olzai	G058
091	 058 	091058	OnanÔøΩ	G064
091	 059 	091059	Onifai	G070
091	 060 	091060	Oniferi	G071
091	 061 	091061	Orani	G084
091	 062 	091062	Orgosolo	G097
091	 063 	091063	Orosei	G119
091	 064 	091064	Orotelli	G120
091	 066 	091066	Ortueri	G146
091	 067 	091067	Orune	G147
091	 068 	091068	Osidda	G154
091	 070 	091070	Ottana	G191
091	 071 	091071	Ovodda	G201
091	 073 	091073	Posada	G929
091	 077 	091077	Sarule	I448
091	 083 	091083	Silanus	I730
091	 084 	091084	Sindia	I748
091	 085 	091085	Siniscola	I751
091	 086 	091086	Sorgono	I851
091	 090 	091090	Teti	L153
091	 091 	091091	Tiana	L160
091	 093 	091093	Tonara	L202
091	 094 	091094	TorpÔøΩ	L231
091	 104 	091104	Lodine	E649
092	 002 	092002	Armungia	A419
092	 003 	092003	Assemini	A474
092	 004 	092004	Ballao	A597
092	 005 	092005	Barrali	A677
092	 008 	092008	Burcei	B274
092	 009 	092009	Cagliari	B354
092	 011 	092011	Capoterra	B675
092	 015 	092015	Decimomannu	D259
092	 016 	092016	Decimoputzu	D260
092	 017 	092017	Dolianova	D323
092	 018 	092018	Domus de Maria	D333
092	 020 	092020	Donori	D344
092	 024 	092024	Gesico	D994
092	 027 	092027	Goni	E084
092	 030 	092030	Guamaggiore	E234
092	 031 	092031	Guasila	E252
092	 036 	092036	Mandas	E877
092	 037 	092037	Maracalagonis	E903
092	 038 	092038	Monastir	F333
092	 039 	092039	Muravera	F808
092	 042 	092042	Nuraminis	F983
092	 044 	092044	Ortacesus	G133
092	 048 	092048	Pimentel	G669
092	 050 	092050	Pula	H088
092	 051 	092051	Quartu SantElena	H118
092	 053 	092053	Samatzai	H739
092	 054 	092054	San Basilio	H766
092	 058 	092058	San NicolÔøΩ Gerrei	G383
092	 059 	092059	San Sperate	I166
092	 061 	092061	SantAndrea Frius	I271
092	 064 	092064	San Vito	I402
092	 066 	092066	Sarroch	I443
092	 068 	092068	Selargius	I580
092	 069 	092069	Selegas	I582
092	 070 	092070	SenorbÔøΩ	I615
092	 071 	092071	Serdiana	I624
092	 074 	092074	Sestu	I695
092	 075 	092075	Settimo San Pietro	I699
092	 078 	092078	Siliqua	I734
092	 079 	092079	Silius	I735
092	 080 	092080	Sinnai	I752
092	 081 	092081	Siurgus Donigala	I765
092	 082 	092082	Soleminis	I797
092	 083 	092083	Suelli	I995
092	 084 	092084	Teulada	L154
092	 088 	092088	Ussana	L512
092	 090 	092090	Uta	L521
092	 091 	092091	Vallermosa	L613
092	 097 	092097	Villaputzu	L998
092	 098 	092098	Villasalto	M016
092	 099 	092099	Villa San Pietro	I118
092	 100 	092100	Villasimius	B738
092	 101 	092101	Villasor	M025
092	 102 	092102	Villaspeciosa	M026
092	 105 	092105	Quartucciu	H119
092	 106 	092106	Castiadas	M288
092	 108 	092108	Elmas	D399
092	 109 	092109	Monserrato	F383
092	 110 	092110	Escalaplano	D430
092	 111 	092111	Escolca	D431
092	 112 	092112	Esterzili	D443
092	 113 	092113	Gergei	D982
092	 114 	092114	Isili	E336
092	 115 	092115	Nuragus	F981
092	 116 	092116	Nurallao	F982
092	 117 	092117	Nurri	F986
092	 118 	092118	Orroli	G122
092	 119 	092119	Sadali	H659
092	 120 	092120	Serri	I668
092	 121 	092121	Seulo	I707
092	 122 	092122	Villanova Tulo	L992
093	 001 	093001	Andreis	A283
093	 002 	093002	Arba	A354
093	 003 	093003	Arzene	A456
093	 004 	093004	Aviano	A516
093	 005 	093005	Azzano Decimo	A530
093	 006 	093006	Barcis	A640
093	 007 	093007	Brugnera	B215
093	 008 	093008	Budoia	B247
093	 009 	093009	Caneva	B598
093	 010 	093010	Casarsa della Delizia	B940
093	 011 	093011	Castelnovo del Friuli	C217
093	 012 	093012	Cavasso Nuovo	C385
093	 013 	093013	Chions	C640
093	 014 	093014	Cimolais	C699
093	 015 	093015	Claut	C790
093	 016 	093016	Clauzetto	C791
093	 017 	093017	Cordenons	C991
093	 018 	093018	Cordovado	C993
093	 019 	093019	Erto e Casso	D426
093	 020 	093020	Fanna	D487
093	 021 	093021	Fiume Veneto	D621
093	 022 	093022	Fontanafredda	D670
093	 024 	093024	Frisanco	D804
093	 025 	093025	Maniago	E889
093	 026 	093026	Meduno	F089
093	 027 	093027	Montereale Valcellina	F596
093	 028 	093028	Morsano al Tagliamento	F750
093	 029 	093029	Pasiano di Pordenone	G353
093	 030 	093030	Pinzano al Tagliamento	G680
093	 031 	093031	Polcenigo	G780
093	 032 	093032	Porcia	G886
093	 033 	093033	Pordenone	G888
093	 034 	093034	Prata di Pordenone	G994
093	 035 	093035	Pravisdomini	H010
093	 036 	093036	Roveredo in Piano	H609
093	 037 	093037	Sacile	H657
093	 038 	093038	San Giorgio della Richinvelda	H891
093	 039 	093039	San Martino al Tagliamento	H999
093	 040 	093040	San Quirino	I136
093	 041 	093041	San Vito al Tagliamento	I403
093	 042 	093042	Sequals	I621
093	 043 	093043	Sesto al Reghena	I686
093	 044 	093044	Spilimbergo	I904
093	 045 	093045	Tramonti di Sopra	L324
093	 046 	093046	Tramonti di Sotto	L325
093	 047 	093047	Travesio	L347
093	 048 	093048	Valvasone	L657
093	 049 	093049	Vito dAsio	M085
093	 050 	093050	Vivaro	M096
093	 051 	093051	Zoppola	M190
093	 052 	093052	Vajont	M265
094	 001 	094001	Acquaviva dIsernia	A051
094	 002 	094002	Agnone	A080
094	 003 	094003	Bagnoli del Trigno	A567
094	 004 	094004	Belmonte del Sannio	A761
094	 005 	094005	Cantalupo nel Sannio	B630
094	 006 	094006	Capracotta	B682
094	 007 	094007	Carovilli	B810
094	 008 	094008	Carpinone	B830
094	 009 	094009	Castel del Giudice	C082
094	 010 	094010	Castelpetroso	C246
094	 011 	094011	Castelpizzuto	C247
094	 012 	094012	Castel San Vincenzo	C270
094	 013 	094013	Castelverrino	C200
094	 014 	094014	Cerro al Volturno	C534
094	 015 	094015	Chiauci	C620
094	 016 	094016	Civitanova del Sannio	C769
094	 017 	094017	Colli a Volturno	C878
094	 018 	094018	Conca Casale	C941
094	 019 	094019	Filignano	D595
094	 020 	094020	ForlÔøΩ del Sannio	D703
094	 021 	094021	Fornelli	D715
094	 022 	094022	Frosolone	D811
094	 023 	094023	Isernia	E335
094	 024 	094024	Longano	E669
094	 025 	094025	Macchia dIsernia	E778
094	 026 	094026	Macchiagodena	E779
094	 027 	094027	Miranda	F239
094	 028 	094028	Montaquila	F429
094	 029 	094029	Montenero Val Cocchiara	F580
094	 030 	094030	Monteroduni	F601
094	 031 	094031	Pesche	G486
094	 032 	094032	Pescolanciano	G495
094	 033 	094033	Pescopennataro	G497
094	 034 	094034	Pettoranello del Molise	G523
094	 035 	094035	Pietrabbondante	G606
094	 036 	094036	Pizzone	G727
094	 037 	094037	Poggio Sannita	B317
094	 038 	094038	Pozzilli	G954
094	 039 	094039	Rionero Sannitico	H308
094	 040 	094040	Roccamandolfi	H420
094	 041 	094041	Roccasicura	H445
094	 042 	094042	Rocchetta a Volturno	H458
094	 043 	094043	San Pietro Avellana	I096
094	 044 	094044	SantAgapito	I189
094	 045 	094045	Santa Maria del Molise	I238
094	 046 	094046	SantAngelo del Pesco	I282
094	 047 	094047	SantElena Sannita	B466
094	 048 	094048	Scapoli	I507
094	 049 	094049	Sessano del Molise	I679
094	 050 	094050	Sesto Campano	I682
094	 051 	094051	Vastogirardi	L696
094	 052 	094052	Venafro	L725
095	 001 	095001	Abbasanta	A007
095	 002 	095002	Aidomaggiore	A097
095	 003 	095003	Albagiara	A126
095	 004 	095004	Ales	A180
095	 005 	095005	Allai	A204
095	 006 	095006	Arborea	A357
095	 007 	095007	Ardauli	A380
095	 008 	095008	Assolo	A477
095	 009 	095009	Asuni	A480
095	 010 	095010	Baradili	A614
095	 011 	095011	Baratili San Pietro	A621
095	 012 	095012	Baressa	A655
095	 013 	095013	Bauladu	A721
095	 014 	095014	BidonÔøΩ	A856
095	 015 	095015	Bonarcado	A960
095	 016 	095016	Boroneddu	B055
095	 017 	095017	Busachi	B281
095	 018 	095018	Cabras	B314
095	 019 	095019	Cuglieri	D200
095	 020 	095020	Fordongianus	D695
095	 021 	095021	Ghilarza	E004
095	 022 	095022	Gonnoscodina	E087
095	 023 	095023	GonnosnÔøΩ	D585
095	 024 	095024	Gonnostramatza	E088
095	 025 	095025	Marrubiu	E972
095	 026 	095026	Masullas	F050
095	 027 	095027	Milis	F208
095	 028 	095028	Mogorella	F270
095	 029 	095029	Mogoro	F272
095	 030 	095030	Morgongiori	F727
095	 031 	095031	Narbolia	F840
095	 032 	095032	Neoneli	F867
095	 033 	095033	Norbello	F934
095	 034 	095034	Nughedu Santa Vittoria	F974
095	 035 	095035	Nurachi	F980
095	 036 	095036	Nureci	F985
095	 037 	095037	Ollastra	G043
095	 038 	095038	Oristano	G113
095	 039 	095039	Palmas Arborea	G286
095	 040 	095040	Pau	G379
095	 041 	095041	Paulilatino	G384
095	 042 	095042	Pompu	G817
095	 043 	095043	Riola Sardo	H301
095	 044 	095044	Ruinas	F271
095	 045 	095045	Samugheo	H756
095	 046 	095046	San NicolÔøΩ dArcidano	A368
095	 047 	095047	Santa Giusta	I205
095	 048 	095048	Villa SantAntonio	I298
095	 049 	095049	Santu Lussurgiu	I374
095	 050 	095050	San Vero Milis	I384
095	 051 	095051	Scano di Montiferro	I503
095	 052 	095052	Sedilo	I564
095	 053 	095053	Seneghe	I605
095	 054 	095054	Senis	I609
095	 055 	095055	Sennariolo	I613
095	 056 	095056	Siamaggiore	I717
095	 057 	095057	Siamanna	I718
095	 058 	095058	Simala	I742
095	 059 	095059	Simaxis	I743
095	 060 	095060	Sini	I749
095	 061 	095061	Siris	I757
095	 062 	095062	Solarussa	I791
095	 063 	095063	Sorradile	I861
095	 064 	095064	Tadasuni	L023
095	 065 	095065	Terralba	L122
095	 066 	095066	Tramatza	L321
095	 067 	095067	Tresnuraghes	L393
095	 068 	095068	UlÔøΩ Tirso	L488
095	 069 	095069	Uras	L496
095	 070 	095070	Usellus	L508
095	 071 	095071	Villanova Truschedu	L991
095	 072 	095072	Villaurbana	M030
095	 073 	095073	Villa Verde	A609
095	 074 	095074	Zeddiani	M153
095	 075 	095075	Zerfaliu	M168
095	 076 	095076	Siapiccia	I721
095	 077 	095077	Curcuris	D214
095	 078 	095078	SoddÔøΩ	I778
095	 079 	095079	Bosa	B068
095	 080 	095080	Flussio	D640
095	 081 	095081	Genoni	D968
095	 082 	095082	Laconi	E400
095	 083 	095083	Magomadas	E825
095	 084 	095084	Modolo	F261
095	 085 	095085	Montresta	F698
095	 086 	095086	Sagama	H661
095	 087 	095087	Suni	L006
095	 088 	095088	Tinnura	L172
096	 001 	096001	Ailoche	A107
096	 002 	096002	Andorno Micca	A280
096	 003 	096003	Benna	A784
096	 004 	096004	Biella	A859
096	 005 	096005	Bioglio	A876
096	 006 	096006	Borriana	B058
096	 007 	096007	Brusnengo	B229
096	 008 	096008	Callabiana	B417
096	 009 	096009	Camandona	B457
096	 010 	096010	Camburzano	B465
096	 011 	096011	Campiglia Cervo	B508
096	 012 	096012	Candelo	B586
096	 013 	096013	Caprile	B708
096	 014 	096014	Casapinta	B933
096	 015 	096015	Castelletto Cervo	C155
096	 016 	096016	CavagliÔøΩ	C363
096	 017 	096017	Cerreto Castello	C526
096	 018 	096018	Cerrione	C532
096	 019 	096019	Coggiola	C819
096	 020 	096020	Cossato	D094
096	 021 	096021	Crevacuore	D165
096	 022 	096022	Crosa	D182
096	 023 	096023	Curino	D219
096	 024 	096024	Donato	D339
096	 025 	096025	Dorzano	D350
096	 026 	096026	Gaglianico	D848
096	 027 	096027	Gifflenga	E024
096	 028 	096028	Graglia	E130
096	 029 	096029	Lessona	E552
096	 030 	096030	Magnano	E821
096	 031 	096031	Massazza	F037
096	 032 	096032	Masserano	F042
096	 033 	096033	Mezzana Mortigliengo	F167
096	 034 	096034	Miagliano	F189
096	 035 	096035	Mongrando	F369
096	 037 	096037	Mottalciata	F776
096	 038 	096038	Muzzano	F833
096	 039 	096039	Netro	F878
096	 040 	096040	Occhieppo Inferiore	F992
096	 041 	096041	Occhieppo Superiore	F993
096	 042 	096042	Pettinengo	G521
096	 043 	096043	Piatto	G577
096	 044 	096044	Piedicavallo	G594
096	 046 	096046	Pollone	G798
096	 047 	096047	Ponderano	G820
096	 048 	096048	Portula	G927
096	 049 	096049	Pralungo	G980
096	 050 	096050	Pray	G974
096	 051 	096051	Quaregna	H103
096	 052 	096052	Quittengo	H145
096	 053 	096053	Ronco Biellese	H538
096	 054 	096054	Roppolo	H553
096	 055 	096055	Rosazza	H561
096	 056 	096056	Sagliano Micca	H662
096	 057 	096057	Sala Biellese	H681
096	 058 	096058	Salussola	H726
096	 059 	096059	Sandigliano	H821
096	 060 	096060	San Paolo Cervo	I074
096	 061 	096061	Selve Marcone	I596
096	 062 	096062	Soprana	I835
096	 063 	096063	Sordevolo	I847
096	 064 	096064	Sostegno	I868
096	 065 	096065	Strona	I980
096	 066 	096066	Tavigliano	L075
096	 067 	096067	Ternengo	L116
096	 068 	096068	Tollegno	L193
096	 069 	096069	Torrazzo	L239
096	 070 	096070	Trivero	L436
096	 071 	096071	Valdengo	L556
096	 072 	096072	Vallanzengo	L586
096	 073 	096073	Valle Mosso	L606
096	 074 	096074	Valle San Nicolao	L620
096	 075 	096075	Veglio	L712
096	 076 	096076	Verrone	L785
096	 077 	096077	Vigliano Biellese	L880
096	 078 	096078	Villa del Bosco	L933
096	 079 	096079	Villanova Biellese	L978
096	 080 	096080	Viverone	M098
096	 081 	096081	Zimone	M179
096	 082 	096082	Zubiena	M196
096	 083 	096083	Zumaglia	M201
096	 084 	096084	Mosso	M304
097	 001 	097001	Abbadia Lariana	A005
097	 002 	097002	Airuno	A112
097	 003 	097003	Annone di Brianza	A301
097	 004 	097004	Ballabio	A594
097	 005 	097005	Barzago	A683
097	 006 	097006	BarzanÔøΩ	A686
097	 007 	097007	Barzio	A687
097	 008 	097008	Bellano	A745
097	 009 	097009	Bosisio Parini	B081
097	 010 	097010	Brivio	B194
097	 011 	097011	Bulciago	B261
097	 012 	097012	Calco	B396
097	 013 	097013	Calolziocorte	B423
097	 014 	097014	Carenno	B763
097	 015 	097015	Casargo	B937
097	 016 	097016	Casatenovo	B943
097	 017 	097017	Cassago Brianza	B996
097	 018 	097018	Cassina Valsassina	C024
097	 019 	097019	Castello di Brianza	C187
097	 020 	097020	Cernusco Lombardone	C521
097	 021 	097021	Cesana Brianza	C563
097	 022 	097022	Civate	C752
097	 023 	097023	Colico	C839
097	 024 	097024	Colle Brianza	C851
097	 025 	097025	Cortenova	D065
097	 026 	097026	Costa Masnaga	D112
097	 027 	097027	Crandola Valsassina	D131
097	 028 	097028	Cremella	D143
097	 029 	097029	Cremeno	D145
097	 030 	097030	Dervio	D280
097	 031 	097031	Dolzago	D327
097	 032 	097032	Dorio	D346
097	 033 	097033	Ello	D398
097	 034 	097034	Erve	D428
097	 035 	097035	Esino Lario	D436
097	 036 	097036	Galbiate	D865
097	 037 	097037	Garbagnate Monastero	D913
097	 038 	097038	Garlate	D926
097	 039 	097039	Imbersago	E287
097	 040 	097040	Introbio	E305
097	 041 	097041	Introzzo	E308
097	 042 	097042	Lecco	E507
097	 043 	097043	Lierna	E581
097	 044 	097044	Lomagna	E656
097	 045 	097045	Malgrate	E858
097	 046 	097046	Mandello del Lario	E879
097	 047 	097047	Margno	E947
097	 048 	097048	Merate	F133
097	 049 	097049	Missaglia	F248
097	 050 	097050	Moggio	F265
097	 051 	097051	Molteno	F304
097	 052 	097052	Monte Marenzo	F561
097	 053 	097053	Montevecchia	F657
097	 054 	097054	Monticello Brianza	F674
097	 055 	097055	Morterone	F758
097	 056 	097056	Nibionno	F887
097	 057 	097057	Oggiono	G009
097	 058 	097058	Olgiate Molgora	G026
097	 059 	097059	Olginate	G030
097	 060 	097060	Oliveto Lario	G040
097	 061 	097061	Osnago	G161
097	 062 	097062	Paderno dAdda	G218
097	 063 	097063	Pagnona	G241
097	 064 	097064	Parlasco	G336
097	 065 	097065	Pasturo	G368
097	 066 	097066	Perego	G448
097	 067 	097067	Perledo	G456
097	 068 	097068	Pescate	G485
097	 069 	097069	Premana	H028
097	 070 	097070	Primaluna	H063
097	 071 	097071	Robbiate	G223
097	 072 	097072	Rogeno	H486
097	 073 	097073	Rovagnate	H596
097	 074 	097074	Santa Maria HoÔøΩ	I243
097	 075 	097075	Sirone	I759
097	 076 	097076	Sirtori	I761
097	 077 	097077	Sueglio	I994
097	 078 	097078	Suello	I996
097	 079 	097079	Taceno	L022
097	 080 	097080	Torre de Busi	L257
097	 081 	097081	Tremenico	L368
097	 082 	097082	Valgreghentino	L581
097	 083 	097083	Valmadrera	L634
097	 084 	097084	Varenna	L680
097	 085 	097085	Vendrogno	L731
097	 086 	097086	Vercurago	L751
097	 087 	097087	Verderio Inferiore	L755
097	 088 	097088	Verderio Superiore	L756
097	 089 	097089	Vestreno	L813
097	 090 	097090	ViganÔøΩ	L866
098	 001 	098001	Abbadia Cerreto	A004
098	 002 	098002	Bertonico	A811
098	 003 	098003	Boffalora dAdda	A919
098	 004 	098004	Borghetto Lodigiano	A995
098	 005 	098005	Borgo San Giovanni	B017
098	 006 	098006	Brembio	B141
098	 007 	098007	Camairago	B456
098	 008 	098008	Casaletto Lodigiano	B887
098	 009 	098009	Casalmaiocco	B899
098	 010 	098010	Casalpusterlengo	B910
098	 011 	098011	Caselle Landi	B961
098	 012 	098012	Caselle Lurani	B958
098	 013 	098013	Castelnuovo Bocca dAdda	C228
098	 014 	098014	Castiglione dAdda	C304
098	 015 	098015	Castiraga Vidardo	C329
098	 016 	098016	Cavacurta	C362
098	 017 	098017	Cavenago dAdda	C394
098	 018 	098018	Cervignano dAdda	C555
098	 019 	098019	Codogno	C816
098	 020 	098020	Comazzo	C917
098	 021 	098021	Cornegliano Laudense	D021
098	 022 	098022	Corno Giovine	D028
098	 023 	098023	Cornovecchio	D029
098	 024 	098024	Corte Palasio	D068
098	 025 	098025	Crespiatica	D159
098	 026 	098026	Fombio	D660
098	 027 	098027	Galgagnano	D868
098	 028 	098028	Graffignana	E127
098	 029 	098029	Guardamiglio	E238
098	 030 	098030	Livraga	E627
098	 031 	098031	Lodi	E648
098	 032 	098032	Lodi Vecchio	E651
098	 033 	098033	Maccastorna	E777
098	 034 	098034	Mairago	E840
098	 035 	098035	Maleo	E852
098	 036 	098036	Marudo	E994
098	 037 	098037	Massalengo	F028
098	 038 	098038	Meleti	F102
098	 039 	098039	Merlino	F149
098	 040 	098040	Montanaso Lombardo	F423
098	 041 	098041	Mulazzano	F801
098	 042 	098042	Orio Litta	G107
098	 043 	098043	Ospedaletto Lodigiano	G166
098	 044 	098044	Ossago Lodigiano	G171
098	 045 	098045	Pieve Fissiraga	G096
098	 046 	098046	Salerano sul Lambro	H701
098	 047 	098047	San Fiorano	H844
098	 048 	098048	San Martino in Strada	I012
098	 049 	098049	San Rocco al Porto	I140
098	 050 	098050	SantAngelo Lodigiano	I274
098	 051 	098051	Santo Stefano Lodigiano	I362
098	 052 	098052	Secugnago	I561
098	 053 	098053	Senna Lodigiana	I612
098	 054 	098054	Somaglia	I815
098	 055 	098055	Sordio	I848
098	 056 	098056	Tavazzano con Villavesco	F260
098	 057 	098057	Terranova dei Passerini	L125
098	 058 	098058	Turano Lodigiano	L469
098	 059 	098059	Valera Fratta	L572
098	 060 	098060	Villanova del Sillaro	L977
098	 061 	098061	Zelo Buon Persico	M158
099	 001 	099001	Bellaria-Igea Marina	A747
099	 002 	099002	Cattolica	C357
099	 003 	099003	Coriano	D004
099	 004 	099004	Gemmano	D961
099	 005 	099005	Misano Adriatico	F244
099	 006 	099006	Mondaino	F346
099	 007 	099007	Monte Colombo	F476
099	 008 	099008	Montefiore Conca	F502
099	 009 	099009	Montegridolfo	F523
099	 010 	099010	Montescudo	F641
099	 011 	099011	Morciano di Romagna	F715
099	 012 	099012	Poggio Berni	G755
099	 013 	099013	Riccione	H274
099	 014 	099014	Rimini	H294
099	 015 	099015	Saludecio	H724
099	 016 	099016	San Clemente	H801
099	 017 	099017	San Giovanni in Marignano	H921
099	 018 	099018	Santarcangelo di Romagna	I304
099	 019 	099019	Torriana	I550
099	 020 	099020	Verucchio	L797
099	 021 	099021	Casteldelci	C080
099	 022 	099022	Maiolo	E838
099	 023 	099023	Novafeltria	F137
099	 024 	099024	Pennabilli	G433
099	 025 	099025	San Leo	H949
099	 026 	099026	SantAgata Feltria	I201
099	 027 	099027	Talamello	L034
100	 001 	100001	Cantagallo	B626
100	 002 	100002	Carmignano	B794
100	 003 	100003	Montemurlo	F572
100	 004 	100004	Poggio a Caiano	G754
100	 005 	100005	Prato	G999
100	 006 	100006	Vaiano	L537
100	 007 	100007	Vernio	L775
101	 001 	101001	Belvedere di Spinello	A772
101	 002 	101002	Caccuri	B319
101	 003 	101003	Carfizzi	B771
101	 004 	101004	Casabona	B857
101	 005 	101005	Castelsilano	B968
101	 006 	101006	Cerenzia	C501
101	 007 	101007	CirÔøΩ	C725
101	 008 	101008	CirÔøΩ Marina	C726
101	 009 	101009	Cotronei	D123
101	 010 	101010	Crotone	D122
101	 011 	101011	Crucoli	D189
101	 012 	101012	Cutro	D236
101	 013 	101013	Isola di Capo Rizzuto	E339
101	 014 	101014	Melissa	F108
101	 015 	101015	Mesoraca	F157
101	 016 	101016	Pallagorio	G278
101	 017 	101017	Petilia Policastro	G508
101	 018 	101018	Roccabernarda	H383
101	 019 	101019	Rocca di Neto	H403
101	 020 	101020	San Mauro Marchesato	I026
101	 021 	101021	San Nicola dellAlto	I057
101	 022 	101022	Santa Severina	I308
101	 023 	101023	Savelli	I468
101	 024 	101024	Scandale	I494
101	 025 	101025	Strongoli	I982
101	 026 	101026	Umbriatico	L492
101	 027 	101027	Verzino	L802
102	 001 	102001	Acquaro	A043
102	 002 	102002	Arena	A386
102	 003 	102003	Briatico	B169
102	 004 	102004	Brognaturo	B197
102	 005 	102005	Capistrano	B655
102	 006 	102006	Cessaniti	C581
102	 007 	102007	DasÔøΩ	D253
102	 008 	102008	Dinami	D303
102	 009 	102009	Drapia	D364
102	 010 	102010	Fabrizia	D453
102	 011 	102011	Filadelfia	D587
102	 012 	102012	Filandari	D589
102	 013 	102013	Filogaso	D596
102	 014 	102014	Francavilla Angitola	D762
102	 015 	102015	Francica	D767
102	 016 	102016	Gerocarne	D988
102	 017 	102017	Ionadi	E321
102	 018 	102018	Joppolo	E389
102	 019 	102019	Limbadi	E590
102	 020 	102020	Maierato	E836
102	 021 	102021	Mileto	F207
102	 022 	102022	Mongiana	F364
102	 023 	102023	Monterosso Calabro	F607
102	 024 	102024	Nardodipace	F843
102	 025 	102025	Nicotera	F893
102	 026 	102026	Parghelia	G335
102	 027 	102027	Pizzo	G722
102	 028 	102028	Pizzoni	G728
102	 029 	102029	Polia	G785
102	 030 	102030	Ricadi	H271
102	 031 	102031	Rombiolo	H516
102	 032 	102032	San Calogero	H785
102	 033 	102033	San Costantino Calabro	H807
102	 034 	102034	San Gregorio dIppona	H941
102	 035 	102035	San Nicola da Crissa	I058
102	 036 	102036	SantOnofrio	I350
102	 037 	102037	Serra San Bruno	I639
102	 038 	102038	Simbario	I744
102	 039 	102039	Sorianello	I853
102	 040 	102040	Soriano Calabro	I854
102	 041 	102041	Spadola	I884
102	 042 	102042	Spilinga	I905
102	 043 	102043	Stefanaconi	I945
102	 044 	102044	Tropea	L452
102	 045 	102045	Vallelonga	L607
102	 046 	102046	Vazzano	L699
102	 047 	102047	Vibo Valentia	F537
102	 048 	102048	Zaccanopoli	M138
102	 049 	102049	Zambrone	M143
102	 050 	102050	Zungri	M204
103	 001 	103001	Antrona Schieranco	A317
103	 002 	103002	Anzola dOssola	A325
103	 003 	103003	Arizzano	A409
103	 004 	103004	Arola	A427
103	 005 	103005	Aurano	A497
103	 006 	103006	Baceno	A534
103	 007 	103007	Bannio Anzino	A610
103	 008 	103008	Baveno	A725
103	 009 	103009	Bee	A733
103	 010 	103010	Belgirate	A742
103	 011 	103011	Beura-Cardezza	A834
103	 012 	103012	Bognanco	A925
103	 013 	103013	Brovello-Carpugnino	B207
103	 014 	103014	Calasca-Castiglione	B380
103	 015 	103015	Cambiasca	B463
103	 016 	103016	Cannero Riviera	B610
103	 017 	103017	Cannobio	B615
103	 018 	103018	Caprezzo	B694
103	 019 	103019	Casale Corte Cerro	B876
103	 020 	103020	Cavaglio-Spoccia	C367
103	 021 	103021	Ceppo Morelli	C478
103	 022 	103022	Cesara	C567
103	 023 	103023	Cossogno	D099
103	 024 	103024	Craveggia	D134
103	 025 	103025	Crevoladossola	D168
103	 026 	103026	Crodo	D177
103	 027 	103027	Cursolo-Orasso	D225
103	 028 	103028	Domodossola	D332
103	 029 	103029	Druogno	D374
103	 030 	103030	Falmenta	D481
103	 031 	103031	Formazza	D706
103	 032 	103032	Germagno	D984
103	 033 	103033	Ghiffa	E003
103	 034 	103034	Gignese	E028
103	 035 	103035	Gravellona Toce	E153
103	 036 	103036	Gurro	E269
103	 037 	103037	Intragna	E304
103	 038 	103038	Loreglia	E685
103	 039 	103039	Macugnaga	E790
103	 040 	103040	Madonna del Sasso	E795
103	 041 	103041	Malesco	E853
103	 042 	103042	Masera	F010
103	 043 	103043	Massiola	F048
103	 044 	103044	Mergozzo	F146
103	 045 	103045	Miazzina	F192
103	 046 	103046	Montecrestese	F483
103	 047 	103047	Montescheno	F639
103	 048 	103048	Nonio	F932
103	 049 	103049	Oggebbio	G007
103	 050 	103050	Omegna	G062
103	 051 	103051	Ornavasso	G117
103	 052 	103052	Pallanzeno	G280
103	 053 	103053	Piedimulera	G600
103	 054 	103054	Pieve Vergonte	G658
103	 055 	103055	Premeno	H030
103	 056 	103056	Premia	H033
103	 057 	103057	Premosello-Chiovenda	H037
103	 058 	103058	Quarna Sopra	H106
103	 059 	103059	Quarna Sotto	H107
103	 060 	103060	Re	H203
103	 061 	103061	San Bernardino Verbano	H777
103	 062 	103062	Santa Maria Maggiore	I249
103	 063 	103063	Seppiana	I619
103	 064 	103064	Stresa	I976
103	 065 	103065	Toceno	L187
103	 066 	103066	Trarego Viggiona	L333
103	 067 	103067	Trasquera	L336
103	 068 	103068	Trontano	L450
103	 069 	103069	Valstrona	L651
103	 070 	103070	Vanzone con San Carlo	L666
103	 071 	103071	Varzo	L691
103	 072 	103072	Verbania	L746
103	 073 	103073	Viganella	L864
103	 074 	103074	Vignone	L889
103	 075 	103075	Villadossola	L906
103	 076 	103076	Villette	M042
103	 077 	103077	Vogogna	M111
104	 001 	104001	Aggius	A069
104	 002 	104002	Aglientu	H848
104	 003 	104003	AlÔøΩ dei Sardi	A115
104	 004 	104004	Arzachena	A453
104	 005 	104005	Badesi	M214
104	 006 	104006	Berchidda	A789
104	 007 	104007	Bortigiadas	B063
104	 008 	104008	BuddusÔøΩ	B246
104	 009 	104009	Budoni	B248
104	 010 	104010	Calangianus	B378
104	 011 	104011	Golfo Aranci	M274
104	 012 	104012	La Maddalena	E425
104	 013 	104013	Loiri Porto San Paolo	M275
104	 014 	104014	Luogosanto	E747
104	 015 	104015	Luras	E752
104	 016 	104016	Monti	F667
104	 017 	104017	Olbia	G015
104	 018 	104018	Oschiri	G153
104	 019 	104019	Padru	M301
104	 020 	104020	Palau	G258
104	 021 	104021	SantAntonio di Gallura	M276
104	 022 	104022	Santa Teresa Gallura	I312
104	 023 	104023	San Teodoro	I329
104	 024 	104024	Telti	L088
104	 025 	104025	Tempio Pausania	L093
104	 026 	104026	TrinitÔøΩ dAgultu e Vignola	L428
105	 001 	105001	Arzana	A454
105	 002 	105002	Bari Sardo	A663
105	 003 	105003	Baunei	A722
105	 004 	105004	Cardedu	M285
105	 005 	105005	Elini	D395
105	 006 	105006	Gairo	D859
105	 007 	105007	Girasole	E049
105	 008 	105008	Ilbono	E283
105	 009 	105009	Jerzu	E387
105	 010 	105010	Lanusei	E441
105	 011 	105011	Loceri	E644
105	 012 	105012	Lotzorai	E700
105	 013 	105013	Osini	G158
105	 014 	105014	Perdasdefogu	G445
105	 015 	105015	Seui	I706
105	 016 	105016	Talana	L036
105	 017 	105017	Tertenia	L140
105	 018 	105018	TortolÔøΩ	A355
105	 019 	105019	Triei	L423
105	 020 	105020	Ulassai	L489
105	 021 	105021	Urzulei	L506
105	 022 	105022	Ussassai	L514
105	 023 	105023	Villagrande Strisaili	L953
106	 001 	106001	Arbus	A359
106	 002 	106002	Barumini	A681
106	 003 	106003	Collinas	C882
106	 004 	106004	Furtei	D827
106	 005 	106005	Genuri	D970
106	 006 	106006	Gesturi	D997
106	 007 	106007	Gonnosfanadiga	E085
106	 008 	106008	Guspini	E270
106	 009 	106009	Las Plassas	E464
106	 010 	106010	Lunamatrona	E742
106	 011 	106011	Pabillonis	G207
106	 012 	106012	Pauli Arbarei	G382
106	 013 	106013	Samassi	H738
106	 014 	106014	San Gavino Monreale	H856
106	 015 	106015	Sanluri	H974
106	 016 	106016	Sardara	I428
106	 017 	106017	Segariu	I570
106	 018 	106018	Serramanna	I647
106	 019 	106019	Serrenti	I667
106	 020 	106020	Setzu	I705
106	 021 	106021	Siddi	I724
106	 022 	106022	Tuili	L463
106	 023 	106023	Turri	L473
106	 024 	106024	Ussaramanna	L513
106	 025 	106025	Villacidro	L924
106	 026 	106026	Villamar	L966
106	 027 	106027	Villanovaforru	L986
106	 028 	106028	Villanovafranca	L987
107	 001 	107001	Buggerru	B250
107	 002 	107002	Calasetta	B383
107	 003 	107003	Carbonia	B745
107	 004 	107004	Carloforte	B789
107	 005 	107005	Domusnovas	D334
107	 006 	107006	Fluminimaggiore	D639
107	 007 	107007	Giba	E022
107	 008 	107008	Gonnesa	E086
107	 009 	107009	Iglesias	E281
107	 010 	107010	Masainas	M270
107	 011 	107011	Musei	F822
107	 012 	107012	Narcao	F841
107	 013 	107013	Nuxis	F991
107	 014 	107014	Perdaxius	G446
107	 015 	107015	Piscinas	M291
107	 016 	107016	Portoscuso	G922
107	 017 	107017	San Giovanni Suergiu	G287
107	 018 	107018	Santadi	I182
107	 019 	107019	SantAnna Arresi	M209
107	 020 	107020	SantAntioco	I294
107	 021 	107021	Tratalias	L337
107	 022 	107022	Villamassargia	L968
107	 023 	107023	Villaperuccio	M278
108	 001 	108001	Agrate Brianza	A087
108	 002 	108002	Aicurzio	A096
108	 003 	108003	Albiate	A159
108	 004 	108004	Arcore	A376
108	 005 	108005	Barlassina	A668
108	 006 	108006	Bellusco	A759
108	 007 	108007	Bernareggio	A802
108	 008 	108008	Besana in Brianza	A818
108	 009 	108009	Biassono	A849
108	 010 	108010	Bovisio-Masciago	B105
108	 011 	108011	Briosco	B187
108	 012 	108012	Brugherio	B212
108	 013 	108013	Burago di Molgora	B272
108	 014 	108014	Camparada	B501
108	 015 	108015	Carate Brianza	B729
108	 016 	108016	Carnate	B798
108	 017 	108017	Cavenago di Brianza	C395
108	 018 	108018	Ceriano Laghetto	C512
108	 019 	108019	Cesano Maderno	C566
108	 020 	108020	Cogliate	C820
108	 021 	108021	Concorezzo	C952
108	 022 	108022	Correzzana	D038
108	 023 	108023	Desio	D286
108	 024 	108024	Giussano	E063
108	 025 	108025	Lazzate	E504
108	 026 	108026	Lesmo	E550
108	 027 	108027	Limbiate	E591
108	 028 	108028	Lissone	E617
108	 029 	108029	Macherio	E786
108	 030 	108030	Meda	F078
108	 031 	108031	Mezzago	F165
108	 032 	108032	Misinto	F247
108	 033 	108033	Monza	F704
108	 034 	108034	MuggiÔøΩ	F797
108	 035 	108035	Nova Milanese	F944
108	 036 	108036	Ornago	G116
108	 037 	108037	Renate	H233
108	 038 	108038	Ronco Briantino	H537
108	 039 	108039	Seregno	I625
108	 040 	108040	Seveso	I709
108	 041 	108041	Sovico	I878
108	 042 	108042	Sulbiate	I998
108	 043 	108043	Triuggio	L434
108	 044 	108044	Usmate Velate	L511
108	 045 	108045	Varedo	L677
108	 046 	108046	Vedano al Lambro	L704
108	 047 	108047	Veduggio con Colzano	L709
108	 048 	108048	Verano Brianza	L744
108	 049 	108049	Villasanta	M017
108	 050 	108050	Vimercate	M052
108	 051 	108051	Busnago	B289
108	 052 	108052	Caponago	B671
108	 053 	108053	Cornate dAdda	D019
108	 054 	108054	Lentate sul Seveso	E530
108	 055 	108055	Roncello	H529
109	 001 	109001	Altidona	A233
109	 002 	109002	Amandola	A252
109	 003 	109003	Belmonte Piceno	A760
109	 004 	109004	Campofilone	B534
109	 005 	109005	Falerone	D477
109	 006 	109006	Fermo	D542
109	 007 	109007	Francavilla dEte	D760
109	 008 	109008	Grottazzolina	E208
109	 009 	109009	Lapedona	E447
109	 010 	109010	Magliano di Tenna	E807
109	 011 	109011	Massa Fermana	F021
109	 012 	109012	Monsampietro Morico	F379
109	 013 	109013	Montappone	F428
109	 014 	109014	Montefalcone Appennino	F493
109	 015 	109015	Montefortino	F509
109	 016 	109016	Monte Giberto	F517
109	 017 	109017	Montegiorgio	F520
109	 018 	109018	Montegranaro	F522
109	 019 	109019	Monteleone di Fermo	F536
109	 020 	109020	Montelparo	F549
109	 021 	109021	Monte Rinaldo	F599
109	 022 	109022	Monterubbiano	F614
109	 023 	109023	Monte San Pietrangeli	F626
109	 024 	109024	Monte Urano	F653
109	 025 	109025	Monte Vidon Combatte	F664
109	 026 	109026	Monte Vidon Corrado	F665
109	 027 	109027	Montottone	F697
109	 028 	109028	Moresco	F722
109	 029 	109029	Ortezzano	G137
109	 030 	109030	Pedaso	G403
109	 031 	109031	Petritoli	G516
109	 032 	109032	Ponzano di Fermo	G873
109	 033 	109033	Porto San Giorgio	G920
109	 034 	109034	Porto SantElpidio	G921
109	 035 	109035	Rapagnano	H182
109	 036 	109036	Santa Vittoria in Matenano	I315
109	 037 	109037	SantElpidio a Mare	I324
109	 038 	109038	Servigliano	C070
109	 039 	109039	Smerillo	I774
109	 040 	109040	Torre San Patrizio	L279
110	 001 	110001	Andria	A285
110	 002 	110002	Barletta	A669
110	 003 	110003	Bisceglie	A883
110	 004 	110004	Canosa di Puglia	B619
110	 005 	110005	Margherita di Savoia	E946
110	 006 	110006	Minervino Murge	F220
110	 007 	110007	San Ferdinando di Puglia	H839
110	 008 	110008	Spinazzola	I907
110	 009 	110009	Trani	L328
110	 010 	110010	Trinitapoli	B915
001	 001 	001001	Agliè	A074
\.


--
-- Data for Name: _abi_istat_provincia; Type: TABLE DATA; Schema: public; Owner: jabi
--

COPY _abi_istat_provincia (cod_provincia, cod_nuts, cod_regione, denominazione, sigla) FROM stdin;
001	ITC11	001	Torino	TO
002	ITC12	001	Vercelli	VC
003	ITC15	001	Novara	NO
004	ITC16	001	Cuneo	CN
005	ITC17	001	Asti	AT
006	ITC18	001	Alessandria	AL
096	ITC13	001	Biella	BI
103	ITC14	001	Verbano-Cusio-Ossola	VB
007	ITC2	002	Valle d'Aosta/Vall√©e d'Aoste	AO
012	ITC41	003	Varese	VA
013	ITC42	003	Como	CO
014	ITC44	003	Sondrio	SO
015	ITC4C	003	Milano	MI
016	ITC46	003	Bergamo	BG
017	ITC47	003	Brescia	BS
018	ITC48	003	Pavia	PV
019	ITC4A	003	Cremona	CR
020	ITC4B	003	Mantova	MN
097	ITC43	003	Lecco	LC
098	ITC49	003	Lodi	LO
108	ITC4D	003	Monza e della Brianza	MB
021	ITH10	004	Bolzano/Bozen	BZ
022	ITH20	004	Trento	TN
023	ITH31	005	Verona	VR
024	ITH32	005	Vicenza	VI
025	ITH33	005	Belluno	BL
026	ITH34	005	Treviso	TV
027	ITH35	005	Venezia	VE
028	ITH36	005	Padova	PD
029	ITH37	005	Rovigo	RO
030	ITH42	006	Udine	UD
031	ITH43	006	Gorizia	GO
032	ITH44	006	Trieste	TS
093	ITH41	006	Pordenone	PN
008	ITC31	007	Imperia	IM
009	ITC32	007	Savona	SV
010	ITC33	007	Genova	GE
011	ITC34	007	La Spezia	SP
033	ITH51	008	Piacenza	PC
034	ITH52	008	Parma	PR
035	ITH53	008	Reggio nell'Emilia	RE
036	ITH54	008	Modena	MO
037	ITH55	008	Bologna	BO
038	ITH56	008	Ferrara	FE
039	ITH57	008	Ravenna	RA
040	ITH58	008	Forl√¨-Cesena	FC
099	ITH59	008	Rimini	RN
045	ITI11	009	Massa-Carrara	MS
046	ITI12	009	Lucca	LU
047	ITI13	009	Pistoia	PT
048	ITI14	009	Firenze	FI
049	ITI16	009	Livorno	LI
050	ITI17	009	Pisa	PI
051	ITI18	009	Arezzo	AR
052	ITI19	009	Siena	SI
053	ITI1A	009	Grosseto	GR
100	ITI15	009	Prato	PO
054	ITI21	010	Perugia	PG
055	ITI22	010	Terni	TR
041	ITI31	011	Pesaro e Urbino	PU
042	ITI32	011	Ancona	AN
043	ITI33	011	Macerata	MC
044	ITI34	011	Ascoli Piceno	AP
109	ITI35	011	Fermo	FM
056	ITI41	012	Viterbo	VT
057	ITI42	012	Rieti	RI
058	ITI43	012	Roma	RM
059	ITI44	012	Latina	LT
060	ITI45	012	Frosinone	FR
066	ITF11	013	L'Aquila	AQ
067	ITF12	013	Teramo	TE
068	ITF13	013	Pescara	PE
069	ITF14	013	Chieti	CH
070	ITF22	014	Campobasso	CB
094	ITF21	014	Isernia	IS
061	ITF31	015	Caserta	CE
062	ITF32	015	Benevento	BN
063	ITF33	015	Napoli	NA
064	ITF34	015	Avellino	AV
065	ITF35	015	Salerno	SA
071	ITF46	016	Foggia	FG
072	ITF47	016	Bari	BA
073	ITF43	016	Taranto	TA
074	ITF44	016	Brindisi	BR
075	ITF45	016	Lecce	LE
110	ITF48	016	Barletta-Andria-Trani	BT
076	ITF51	017	Potenza	PZ
077	ITF52	017	Matera	MT
078	ITF61	018	Cosenza	CS
079	ITF63	018	Catanzaro	CZ
080	ITF65	018	Reggio di Calabria	RC
101	ITF62	018	Crotone	KR
102	ITF64	018	Vibo Valentia	VV
081	ITG11	019	Trapani	TP
082	ITG12	019	Palermo	PA
083	ITG13	019	Messina	ME
084	ITG14	019	Agrigento	AG
085	ITG15	019	Caltanissetta	CL
086	ITG16	019	Enna	EN
087	ITG17	019	Catania	CT
088	ITG18	019	Ragusa	RG
089	ITG19	019	Siracusa	SR
090	ITG25	020	Sassari	SS
091	ITG26	020	Nuoro	NU
092	ITG27	020	Cagliari	CA
095	ITG28	020	Oristano	OR
104	ITG29	020	Olbia-Tempio	OT
105	ITG2A	020	Ogliastra	OG
106	ITG2B	020	Medio Campidano	VS
107	ITG2C	020	Carbonia-Iglesias	CI
\.


--
-- Data for Name: _abi_istat_regione; Type: TABLE DATA; Schema: public; Owner: jabi
--

COPY _abi_istat_regione (cod_regione, cod_nuts2, denominazione, ripartizione_geografica) FROM stdin;
001	ITC1	PIEMONTE	NORD-OVEST
002	ITC2	VALLE D'AOSTA/VALL√âE D'AOSTE	NORD-OVEST
003	ITC4	LOMBARDIA	NORD-OVEST
004	-	TRENTINO-ALTO ADIGE/S√úDTIROL	NORD-EST
005	ITH3	VENETO	NORD-EST
006	ITH4	FRIULI-VENEZIA GIULIA	NORD-EST
007	ITC3	LIGURIA	NORD-OVEST
008	ITH5	EMILIA-ROMAGNA	NORD-EST
009	ITI1	TOSCANA	CENTRO
010	ITI2	UMBRIA	CENTRO
011	ITI3	MARCHE	CENTRO
012	ITI4	LAZIO	CENTRO
013	ITF1	ABRUZZO	SUD
014	ITF2	MOLISE	SUD
015	ITF3	CAMPANIA	SUD
016	ITF4	PUGLIA	SUD
017	ITF5	BASILICATA	SUD
018	ITF6	CALABRIA	SUD
019	ITG1	SICILIA	ISOLE
020	ITG2	SARDEGNA	ISOLE
\.


--
-- Data for Name: _abi_main_account; Type: TABLE DATA; Schema: public; Owner: jabi
--

COPY _abi_main_account (id, descr) FROM stdin;
\.


--
-- Name: _abi_main_account_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jabi
--

SELECT pg_catalog.setval('_abi_main_account_id_seq', 1, false);


--
-- Data for Name: _abi_main_lk_users_class; Type: TABLE DATA; Schema: public; Owner: jabi
--

COPY _abi_main_lk_users_class (descrizione, id) FROM stdin;
\.


--
-- Data for Name: _abi_main_lk_users_status; Type: TABLE DATA; Schema: public; Owner: jabi
--

COPY _abi_main_lk_users_status (descrizione, id) FROM stdin;
\.


--
-- Data for Name: _abi_main_users; Type: TABLE DATA; Schema: public; Owner: jabi
--

COPY _abi_main_users (id, login, pwd, name, surname, email, insert_date, update_date, last_access, statusid, classid, audit, account_id, codice_attivazione, codice_fiscale, numero_iscrizione_albo, data_nascita) FROM stdin;
\.


--
-- Name: _abi_main_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jabi
--

SELECT pg_catalog.setval('_abi_main_users_id_seq', 27, false);


--
-- Data for Name: _abi_main_users_session; Type: TABLE DATA; Schema: public; Owner: jabi
--

COPY _abi_main_users_session (userid, prev_access, sessionid, id, ip, fl_attiva, data_fine_sessione) FROM stdin;
\.


--
-- Name: _abi_main_users_session_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jabi
--

SELECT pg_catalog.setval('_abi_main_users_session_id_seq', 274, false);


--
-- Name: _abi_conf_token_pkey; Type: CONSTRAINT; Schema: public; Owner: jabi
--

ALTER TABLE ONLY _abi_conf_token
    ADD CONSTRAINT _abi_conf_token_pkey PRIMARY KEY (id);


--
-- Name: _abi_contab_conf_pkey; Type: CONSTRAINT; Schema: public; Owner: jabi
--

ALTER TABLE ONLY _abi_conf_parametro
    ADD CONSTRAINT _abi_contab_conf_pkey PRIMARY KEY (chiave);


--
-- Name: _abi_istat_comuni_pkey; Type: CONSTRAINT; Schema: public; Owner: jabi
--

ALTER TABLE ONLY _abi_istat_comune
    ADD CONSTRAINT _abi_istat_comuni_pkey PRIMARY KEY (istat_comune);


--
-- Name: _abi_istat_province_pkey; Type: CONSTRAINT; Schema: public; Owner: jabi
--

ALTER TABLE ONLY _abi_istat_provincia
    ADD CONSTRAINT _abi_istat_province_pkey PRIMARY KEY (cod_provincia);


--
-- Name: _abi_istat_regioni_pkey; Type: CONSTRAINT; Schema: public; Owner: jabi
--

ALTER TABLE ONLY _abi_istat_regione
    ADD CONSTRAINT _abi_istat_regioni_pkey PRIMARY KEY (cod_regione);


--
-- Name: _abi_main_account_pkey; Type: CONSTRAINT; Schema: public; Owner: jabi
--

ALTER TABLE ONLY _abi_main_account
    ADD CONSTRAINT _abi_main_account_pkey PRIMARY KEY (id);


--
-- Name: _abi_main_lk_users_class_pkey; Type: CONSTRAINT; Schema: public; Owner: jabi
--

ALTER TABLE ONLY _abi_main_lk_users_class
    ADD CONSTRAINT _abi_main_lk_users_class_pkey PRIMARY KEY (id);


--
-- Name: _abi_main_lk_users_status_pkey; Type: CONSTRAINT; Schema: public; Owner: jabi
--

ALTER TABLE ONLY _abi_main_lk_users_status
    ADD CONSTRAINT _abi_main_lk_users_status_pkey PRIMARY KEY (id);


--
-- Name: _abi_main_user_session_pkey; Type: CONSTRAINT; Schema: public; Owner: jabi
--

ALTER TABLE ONLY _abi_main_users_session
    ADD CONSTRAINT _abi_main_user_session_pkey PRIMARY KEY (id);


--
-- Name: _abi_main_users_email_key; Type: CONSTRAINT; Schema: public; Owner: jabi
--

ALTER TABLE ONLY _abi_main_users
    ADD CONSTRAINT _abi_main_users_email_key UNIQUE (email);


--
-- Name: _abi_main_users_login_key; Type: CONSTRAINT; Schema: public; Owner: jabi
--

ALTER TABLE ONLY _abi_main_users
    ADD CONSTRAINT _abi_main_users_login_key UNIQUE (login);


--
-- Name: _abi_main_users_pkey; Type: CONSTRAINT; Schema: public; Owner: jabi
--

ALTER TABLE ONLY _abi_main_users
    ADD CONSTRAINT _abi_main_users_pkey PRIMARY KEY (id);


--
-- Name: fki_; Type: INDEX; Schema: public; Owner: jabi
--

CREATE INDEX fki_ ON _abi_main_users USING btree (classid);


--
-- Name: _abi_istat_comuni_cod_provincia_fkey; Type: FK CONSTRAINT; Schema: public; Owner: jabi
--

ALTER TABLE ONLY _abi_istat_comune
    ADD CONSTRAINT _abi_istat_comuni_cod_provincia_fkey FOREIGN KEY (cod_provincia) REFERENCES _abi_istat_provincia(cod_provincia);


--
-- Name: _abi_istat_province_cod_regione_fkey; Type: FK CONSTRAINT; Schema: public; Owner: jabi
--

ALTER TABLE ONLY _abi_istat_provincia
    ADD CONSTRAINT _abi_istat_province_cod_regione_fkey FOREIGN KEY (cod_regione) REFERENCES _abi_istat_regione(cod_regione);


--
-- Name: _abi_main_users_account_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: jabi
--

ALTER TABLE ONLY _abi_main_users
    ADD CONSTRAINT _abi_main_users_account_id_fkey FOREIGN KEY (account_id) REFERENCES _abi_main_account(id);


--
-- Name: _abi_main_users_classid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: jabi
--

ALTER TABLE ONLY _abi_main_users
    ADD CONSTRAINT _abi_main_users_classid_fkey FOREIGN KEY (classid) REFERENCES _abi_main_lk_users_class(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: _abi_main_users_session_fkey; Type: FK CONSTRAINT; Schema: public; Owner: jabi
--

ALTER TABLE ONLY _abi_main_users_session
    ADD CONSTRAINT _abi_main_users_session_fkey FOREIGN KEY (userid) REFERENCES _abi_main_users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: _abi_main_users_statusid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: jabi
--

ALTER TABLE ONLY _abi_main_users
    ADD CONSTRAINT _abi_main_users_statusid_fkey FOREIGN KEY (statusid) REFERENCES _abi_main_lk_users_status(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

